//
//  UIScrollView+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 9/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
   }
}
