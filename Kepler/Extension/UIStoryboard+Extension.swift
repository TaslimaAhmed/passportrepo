//
//  UIStoryboard+Extension.swift
//  BDKepler
//
//  Created by Mahbubur Rashid on 20/12/19.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    /// The uniform place where we state all the storyboard we have in our application
    enum Storyboard: String {
        case common                 = "Common"
        case main                   = "Main"
        case ekyc                   = "EKYC"
        case home                   = "Home"
        case my_account             = "MyAccount"
        case cashOut                = "CashOut"
        case send_money             = "SendMoney"
        case mobileRecharge         = "MobileRecharge"
        case bill_pay               = "BillPay"
        case merchant               = "Merchant"
        case addMoney               = "AddMoney"
        case transactionHistory     = "TransactionHistory"
        
        case doMore                 = "DoMore"
        case electricity            = "Electricity"
        case desco_prepaid          = "DescoPrepaid"
        case desco_postpaid         = "DescoPostpaid"
        case tuition_fee            = "TuitionFee"
        case insurance              = "Insurance"
        case passport               = "PassportFees"
        case nidFee                 = "NIDFee"
        case nid                    = "NIDFees"
        case dpdc                   = "DPDC"
        case fund_transfer          = "FundTransferFees"
    }
    
    // MARK: - Convenience Initializers
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    // MARK: - Class Functions
    class func storyboard(_ storyboard: Storyboard, bundle: Bundle? = nil) -> UIStoryboard {
        return UIStoryboard(name: storyboard.rawValue, bundle: bundle)
    }
}
