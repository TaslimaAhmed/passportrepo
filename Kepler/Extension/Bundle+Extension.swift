//
//  Bundle+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 12/1/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation

extension Bundle {
    // Name of the app - title under the icon.
    var displayName: String? {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ??
            object(forInfoDictionaryKey: "CFBundleName") as? String
    }
    
    var appVersion:String? {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    
    var appIdentifier:String? {
        return Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String ?? ""
    }
}
