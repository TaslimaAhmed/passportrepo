//
//  Dictionary+Extension.swift
//  BDKepler
//
//  Created by Mahbubur Rashid on 26/11/19.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
