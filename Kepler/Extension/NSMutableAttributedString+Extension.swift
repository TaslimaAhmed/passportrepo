//
//  NSMutableAttributedString+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 23/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {

    func bold(_ value:String, _ fontSize: CGFloat) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font : UIFont(name: FontList.Bold.rawValue, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func semiBold(_ value:String, _ fontSize: CGFloat) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font : UIFont(name: FontList.SemiBold.rawValue, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func extraBold(_ value:String, _ fontSize: CGFloat) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font : UIFont(name: FontList.ExtraBold.rawValue, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }

    func normal(_ value:String, _ fontSize: CGFloat) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font : UIFont(name: FontList.Regular.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    /* Other styling methods */
    func orangeHighlight(_ value:String, _ fontSize: CGFloat) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font :  UIFont(name: FontList.Regular.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize),
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }

    func blackHighlight(_ value:String, _ fontSize: CGFloat) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font :  UIFont(name: FontList.Regular.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize),
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black

        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }

    func underlined(_ value:String, _ fontSize: CGFloat) -> NSMutableAttributedString {

        let attributes:[NSAttributedString.Key : Any] = [
            .font :  UIFont(name: FontList.Regular.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize),
            .underlineStyle : NSUnderlineStyle.single.rawValue

        ]

        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}
