//
//  UIViewController+Extension.swift
//  BDKepler
//
//  Created by Mahbubur Rashid on 25/1/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import Toast_Swift

extension UIViewController {
    /** View Identifier
     */
    static var identifier: String {
        return String(describing: self)
    }
    
    func showToast(message: String) {
        self.view.makeToast(message)
    }
    
    func showToast(message: DefaultMessages){
        self.view.makeToast(message.rawValue.localized)
    }
}
