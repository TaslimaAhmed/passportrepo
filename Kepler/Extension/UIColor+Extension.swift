//
//  UIColor+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var keplerBlack: UIColor {
        return #colorLiteral(red: 0.137254902, green: 0.1215686275, blue: 0.1254901961, alpha: 1)
    }
    static var keplerLightGray: UIColor {
        return #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1)
    }
    static var keplerGray: UIColor {
        return #colorLiteral(red: 0.5411764706, green: 0.5411764706, blue: 0.5490196078, alpha: 1)
    }
    static var keplerDarkGray: UIColor {
        return #colorLiteral(red: 0.3450980392, green: 0.3490196078, blue: 0.3568627451, alpha: 1)
    }
    static var keplerRed: UIColor {
        return #colorLiteral(red: 0.9411764706, green: 0.2392156863, blue: 0.2392156863, alpha: 1)
    }
    static var keplerDarkRed: UIColor {
        return #colorLiteral(red: 0.737254902, green: 0.1960784314, blue: 0.1921568627, alpha: 1)
    }
    static var keplerLightRed: UIColor {
        return #colorLiteral(red: 0.9882352941, green: 0.8823529412, blue: 0.8823529412, alpha: 1)
    }
    static var keplerGreen: UIColor {
        return #colorLiteral(red: 0.08235294118, green: 0.568627451, blue: 0.568627451, alpha: 1)
    }
    static var keplerDarkGreen: UIColor {
        return #colorLiteral(red: 0.01176470588, green: 0.4705882353, blue: 0.4705882353, alpha: 1)
    }
    static var keplerLightGreen: UIColor {
        return #colorLiteral(red: 0.8039215686, green: 0.9254901961, blue: 0.937254902, alpha: 1)
    }
    static var keplerLightBlue: UIColor {
        return #colorLiteral(red: 0.8588235294, green: 0.9450980392, blue: 0.968627451, alpha: 1)
    }
    static var keplerWhite: UIColor {
        return #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    static var keplerLightBackground: UIColor {
        return #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
    }
    static var keplerTextUnderLine: UIColor {
        return #colorLiteral(red: 0.9411764706, green: 0.2392156863, blue: 0.2392156863, alpha: 1)
    }
}

enum ColorPalette: String {
    case Red                = "red"
    case DarkRed            = "darkred"
    case LightRed           = "lightred"
    case Green              = "green"
    case DarkGreen          = "darkgreen"
    case LightGreen         = "lightgreen"
    case keplerLightBlue    = "lightblue"
    case Black              = "black"
    case Gray               = "gray"
    case LightGray          = "lightgray"
    case DarkGray           = "darkgray"
    case White              = "white"
    case TextUnderline      = "text_underline"

    case Clear              = "clear"
}

let namedColors = [
    "red"           : UIColor.keplerRed,
    "darkred"       : UIColor.keplerDarkRed,
    "lightred"      : UIColor.keplerLightRed,
    "green"         : UIColor.keplerGreen,
    "darkgreen"     : UIColor.keplerDarkGreen,
    "lightgreen"    : UIColor.keplerLightGreen,
    "lightblue"     : UIColor.keplerLightBlue,
    "black"         : UIColor.keplerBlack,
    "gray"          : UIColor.keplerGray,
    "lightgray"     : UIColor.keplerLightGray,
    "darkgray"      : UIColor.keplerDarkGray,
    "white"         : UIColor.keplerWhite,
    "text_underline": UIColor.keplerTextUnderLine
]
