//
//  ContactHelper.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 3/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import Contacts

class ContactHelper {
    private static var contanctInstance: ContactHelper!
    
    public static var contactData: [ContactsItem] = []
    
    private init() {
        
    }
    
    public static func getInstance() {
        if self.contanctInstance == nil {
            self.contanctInstance = ContactHelper()
            self.getContancts()
        }
    }
    
    private static func getContancts() {
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { (granted, err) in
            if let err = err {
                DLog("Failed to fetch contacts \(err)")
            }
            
            if granted {
                var contacts = [CNContact]()
                let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .phoneticFullName), CNContactPhoneNumbersKey as CNKeyDescriptor, CNContactImageDataKey as CNKeyDescriptor]
                let request = CNContactFetchRequest(keysToFetch: keys)
                request.sortOrder = .givenName
                do {
                    try store.enumerateContacts(with: request) { (contact, stop) in
                        contacts.append(contact)
                    }
                } catch {
                    DLog(error.localizedDescription)
                }
                
                contacts.forEach { (contact) in
                    if contact.phoneNumbers.count > 1 {
                        contact.phoneNumbers.forEach { (phoneNumber) in
                            //Filtering out shortcodes
                            if phoneNumber.value.stringValue.count > 10 {
                                let data = ContactsItem(name: contact.givenName + " " + contact.familyName, number: phoneNumber.value.stringValue)
                                self.contactData.append(data)
                            }
                        }
                    } else {
                        //Filtering out shortcodes
                        if contact.phoneNumbers.first?.value.stringValue.count ?? 11 > 10 {
                            let data = ContactsItem(name: contact.givenName + " " + contact.familyName, number: contact.phoneNumbers.first?.value.stringValue ?? "")
                            self.contactData.append(data)
                        }
                    }
                }
            }
        }
    }
}
