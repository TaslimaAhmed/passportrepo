//
//  CommonMethods.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 9/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SDWebImage
import Contacts
import SwiftyJSON
import Network

func emptyMessageInTableView(_ tableView: UITableView,_ title: String){
    let noDataLabel: UILabel        = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
    noDataLabel.textColor           = UIColor.black
    noDataLabel.font                = UIFont(name: "Raleway-Medium", size: 20)
    noDataLabel.textAlignment       = .center
    noDataLabel.lineBreakMode       = .byWordWrapping
    noDataLabel.numberOfLines       = 0
    tableView.backgroundView        = noDataLabel
    tableView.separatorStyle        = .none
    noDataLabel.text = title
}

func emptyMessageInCollectionView(_ collectionView: UICollectionView,_ title: String){
    let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height))
    noDataLabel.textColor        = UIColor.black
    noDataLabel.font             = UIFont(name: "Raleway-Medium", size: 20)
    noDataLabel.textAlignment    = .center
    noDataLabel.lineBreakMode       = .byWordWrapping
    noDataLabel.numberOfLines       = 0
    collectionView.backgroundView = noDataLabel
    //        collectionView.separatorStyle = .none
    noDataLabel.text = title
}

// Random number generation
private var randNumber: String = ""
private var uuid: String = ""

func getRandomString() -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    var rand: String = String((0..<32).map { _ in letters.randomElement()! })
    if rand.elementsEqual(randNumber) {
        rand = String((0..<32).map { _ in letters.randomElement()! })
    }
    randNumber = rand
    return rand
}

func getUUID() -> String{
    var uuID = UUID().uuidString
    if uuID.elementsEqual(uuid) {
        uuID = UUID().uuidString
    }
    uuid = uuID
    return uuID
}
// Random number generation

func loadImage(imageURL: String, imageView: UIImageView, placeHolderImage: String? = nil) {
    if placeHolderImage != nil {
        imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: placeHolderImage ?? ""))
    } else {
        imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "ic_placeholder"))
    }
}

func validatePhoneNumber(_ msisdn: String?) -> (Bool, String) {
    guard let phnNum = msisdn else {return (false,"")}
    
    if phnNum.hasPrefix("01") && phnNum.count == 11 {
        return (true, phnNum)
    } else {
        return (false, DefaultMessages.INVALID_PHONE_NUMBER.rawValue.localized)
    }
}

func validateOTP(_ otp: String?) -> (Bool, String) {
    guard let otpFinal = otp else {return (false,"")}
    
    if otpFinal.count == 6 {
        return (true, otpFinal)
    } else {
        return (false, DefaultMessages.VALID_OTP.rawValue.localized)
    }
}

func getCurrentMillis()->String {
    return  String(Int64(NSDate().timeIntervalSince1970 * 1000))
}

func showSettingsAlert(_ viewController: UIViewController, _ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
    let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
    if let settings = URL(string: UIApplication.openSettingsURLString),
       UIApplication.shared.canOpenURL(settings) {
        alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
            completionHandler(false)
            UIApplication.shared.open(settings)
        })
    }
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
        completionHandler(false)
    })
    viewController.present(alert, animated: true)
}

func getMobileOperator(phoneNumber: String) -> String {
    var operatorName: String = ""
    
    if(phoneNumber.hasPrefix("017") || phoneNumber.hasPrefix("013")) {
        operatorName = "GrameenPhone"
    } else if(phoneNumber.hasPrefix("018")) {
        operatorName = "Robi"
    } else if(phoneNumber.hasPrefix("019") || phoneNumber.hasPrefix("014")) {
        operatorName = "Banglalink"
    } else if(phoneNumber.hasPrefix("015")) {
        operatorName = "Teletalk"
    } else if(phoneNumber.hasPrefix("016")) {
        operatorName = "Airtel"
    }
    
    return operatorName
}

func initHomeView() {
    if let vc = UIStoryboard(storyboard: .home).instantiateViewController(withIdentifier: MainVC.identifier) as? MainVC {
        let navVC = UINavigationController(rootViewController: vc)
        let window = UIApplication.shared.keyWindow!
        window.rootViewController = navVC
        
        UIView.transition(with: window, duration: 0.3, options: UIView.AnimationOptions.transitionCrossDissolve) {
            window.makeKeyAndVisible()
        }
    }
}

func initOnboardingView() {
    let vc = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: SigninVC.identifier) as! SigninVC
    let navVC = UINavigationController(rootViewController: vc)
    let window = UIApplication.shared.keyWindow!
    window.rootViewController = navVC
    
    UIView.transition(with: window, duration: 0.3, options: UIView.AnimationOptions.transitionCrossDissolve) {
        window.makeKeyAndVisible()
    }
}

func showNoInternet() {
    DispatchQueue.main.async {
        let window = UIApplication.shared.keyWindow!
        let noInterNetView = NoInternetView().setupFromNib()
        ShowCustomPopup().showPopup(parentView: window, popupView: noInterNetView)
    }
}

func showSessionExpired() {
    initOnboardingView()
    NetworkManager.isReachable { reachable in
        if reachable {
            DispatchQueue.main.async {
                let window = UIApplication.shared.keyWindow!
                let noInterNetView = SessionExpiredView().setupFromNib()
                ShowCustomPopup().showPopup(parentView: window, popupView: noInterNetView)
            }
        } else {
            showNoInternet()
        }
    }
}

func enableDisableNextButton(button: UIButton, _ shouldEnable: Bool = false) {
    if shouldEnable {
        button.isEnabled = true
        button.alpha = 1
    } else {
        button.isEnabled = false
        button.alpha = 0.5
    }
}

func showLoadingView(navigationController: UINavigationController, loadingText: String) -> LoadingVC {
    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: LoadingVC.identifier) as! LoadingVC
    vc.loadingText = loadingText
    navigationController.present(vc, animated: true, completion: nil)
    return vc
}
