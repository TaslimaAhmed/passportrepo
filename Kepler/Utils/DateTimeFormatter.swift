//
//  DateTimeFormatter.swift
//  BDKepler
//
//  Created by Mahbubur Rashid on 25/11/19.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

class DateTimeFormatter {
    class func getFormatedDate(inputDate: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let newFormate = DateFormatter()
        newFormate.dateFormat = "dd MMM, yyyy"
        
        if let date = dateFormatter.date(from: inputDate) {
            return newFormate.string(from: date)
        } else {
            return nil
        }
    }
}
