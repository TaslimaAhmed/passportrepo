//
//  Protocols.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 19/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import UIKit

protocol MobileOperatorSelectionDelegeate {
    func onOperatorSelect(operatorName: String, operatorImage: UIImage?)
}

protocol OnConfirmClickDelegate {
    func onConfirmClicked(pin: String)
}

protocol OnPopupDismissDelegate {
    func onPopupdismissClicked()
}

protocol OnQRCodeScannedDelegate {
    func onQRCodeScanned(code: String)
}

protocol OnReloadViewForLanguageDelegate {
    func onReloadViewForLanguage()
}
