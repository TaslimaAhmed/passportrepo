//
//  SessionManager.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 9/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

enum SessionManagerKey: String, CaseIterable {
    case DEFAULT_DATA = "default_data"
    case DEFAULT_LANGUAGE = "default_language"
    
    case USER_NAME = "user_name"
    case USER_MSISDN = "user_msisdn"
    case USER_ID = "user_id"
    case USER_NID = "user_nid"
    case IS_LOGGED_IN = "isLoggedIn"
    case IS_FIRST_LAUNCH = "is_first_launch"
    case GENDER_TYPE = "gender_type"
    case VERIFICATION_TYPE = "verfication_type"
    case MOBILE_OPERATOR_TYPE = "mobile_operator_type"
    case AVAILABLE_BALANCE = " available_balance"
    case USER_ADDRESS = "user_address"
    case USER_DOB = "user_dob"
    case USER_GENDER = "user_gender"
    
    case EKYC_STATUS = "ekyc_status"
    case EKYC_USER_TYPE = "ekyc_user_type"
    
    case ACCESS_TOKEN = "access_token"
    
    case RECENT_CASH_OUT = "recent_cash_out"
    case RECENT_SEND_MONEY = "recent_send_money"
    case RECENT_MOBILE_RECHARGE = "recent_mobile_recharge"
    case RECENT_INSTITUTE = "recent_institute"
    case RECENT_INSURANCE = "recent_insurance"
    case RECENT_MERCHANT = "recent_merchant"
    
    case RECENT_ORGANIZATION = "recent_organization"
    
    case IS_FOR_LANGUAGE_RELOAD = "is_for_language_reload"
}

class SessionManager {
    class func hasValue(forKey keyName: SessionManagerKey) -> Bool {
        return UserDefaults.standard.object(forKey: keyName.rawValue) != nil
    }
    
    class func setValue(_ value: Any?, forKey keyName: SessionManagerKey) {
        UserDefaults.standard.setValue(value, forKey: keyName.rawValue)
    }
    
    class func getString(forKey keyName: SessionManagerKey) -> String? {
        return UserDefaults.standard.string(forKey: keyName.rawValue)
    }
    
    class func getInteger(forKey keyName: SessionManagerKey) -> Int {
        return UserDefaults.standard.integer(forKey: keyName.rawValue)
    }
    
    class func getBool(forKey keyName: SessionManagerKey) -> Bool? {
        return UserDefaults.standard.bool(forKey: keyName.rawValue)
    }
    
    class func getModelData<T: Mappable>(forKey keyName: SessionManagerKey, model: T.Type) -> Any? {
        return model.init(JSONString: UserDefaults.standard.string(forKey: keyName.rawValue)!)
    }
    
    class func removeData(forKey keyName: SessionManagerKey) {
        UserDefaults.standard.removeObject(forKey: keyName.rawValue)
    }
    
    class func destroySession() {
        SessionManagerKey.allCases.forEach{UserDefaults.standard.removeObject(forKey: $0.rawValue)}
    }
}
