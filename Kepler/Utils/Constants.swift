//
//  Constants.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 9/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    public static let GOOGLE_MAP_API_KEY = "AIzaSyCUnslCfaezPS1sZ8sOuzgUVxKVVks660E"
    
    
}

enum MenuConstants: String {
    case SEND_MONEY             = "Send Money"
    case MOBILE_RECHARGE        = "Mobile Recharge"
    case MERCHANT_PAY           = "Make Payment\n(Merchant)"
    case CASH_OUT               = "Cash Out"
    case ADD_MONEY              = "Add Money"
}

enum EkycUserStatus: String {
    case INCOMPLETE     = "incomplete"
    case PENDING        = "pending"
    case FAILED         = "failed"
    case SUCCESS        = "success"
}

enum EkycUserType: String {
    case OLD_USER       = "olduser"
    case NEW_USER       = "newuser"
}

enum DefaultMessages: String {
    case INVALID_PHONE_NUMBER           = "Invalid phone number"
    
    case EMPTY_AMOUNT_FIELD             = "Please enter amount to continue."
    
    case MINIMUM_SENDING_AMOUNT         = "Amount should be more then 10 taka."
    case INSUFFICIENT_BALANCE           = "Insufficient balance.\nPlease add balance into your account."
    
    case PIN_ERROR                      = "Pin does not match."
    case ENTER_PIN                      = "Please enter your pin"
    case PIN_REQUEST                    = "Please enter a 4 digit pin."
    case FILL_ALL_FIELDS                = "Please fill up all the fields."
    case ENTER_MOBILE_NUMBER            = "Please enter valid mobile number."
    
    case VALID_OTP                      = "Please enter a valid OTP"
    case SUCCESS_OTP                    = "OTP matched."
    case ERROR_OTP                      = "Please Try with correct OTP."
    case OTP_SENT                       = "OTP sent successfully."

    case SUCCESS_ADD_MONEY              = "Amount added successfully into your account."
    
    case ERROR_MESSAGE                  = "Network problem. Please try again later."
    case ERROR_TRANSACTION              = "Transaction failed. Please try again."
    case FEATURE_NOT_READY_ERROR        = "Feature is not ready to use"
}

enum Images: String {
    //Mobile Operators
    case ROBI                   = "ic_robi"
    case AIRTEL                 = "ic_airtel"
    case GP                     = "ic_gp"
    case TELETALK               = "ic_teletalk"
    case BANGLALINK             = "ic_banglalink"
    
    case ADD_MONEY              = "ic_add_money"
    case SEND_MONEY             = "ic_send_money"
    case MOBILE                 = "ic_mobile"
    case MARCHENT_PAY           = "ic_marchant_pay"
    case CASH_OUT               = "ic_cashout"
    
    case HOME_TAB               = "ic_home_tab"
    case DO_MORE_TAB            = "ic_do_more_tab"
    case TRANSACTIONS_TAB       = "ic_transactions_tab"
    case MY_ACCOUNT_TAB         = "ic_profile_tab"
    
    case INSURANCE              = "ic_insurance"
    case Fund                   = "ic_fund"
    case BILL_PAY               = "ic_bill_pay"
    case TUITION_FEES           = "ic_tuition_fees"
    case OTHERS                 = "ic_others"
    
    case PIN                    = "ic_pin"
    case LIMIT                  = "ic_limit"
    case BANK                   = "ic_bank"
    case ABOUT                  = "ic_about"
    case SIGNOUT                = "ic_signout"
}

enum DoMoreMenuConstants: String {
    case INSURANCE              = "Insurance"
    case BILL_PAYMENT           = "Bills Payment"
    case TUITION_FEES           = "Fees Payment"
    case OTHERS                 = "Others"
    case FUND                  = "Fund Transfer"
}

enum TransectionType: String {
    case ADD_MONEY                  = "Add Money"
    case SEND_MONEY                 = "Send Money"
    case MOBILE_RECHARGE            = "Mobile Recharge"
    case MARCHENT_PAY               = "Marchent Pay"
    case BILL_PAYMENT               = "Bill Payment"
    case EDUCATION_BILL_PAYMENT     = "Education Bill Payment"
}

enum TabMenuItem: String {
    case HOME           = "Home"
    case DO_MORE        = "Do More"
    case TRANSACTIONS   = "Transactions"
    case MY_ACCOUNT     = "My Account"
}

enum RecentTransectionType: String {
    case RECENT_CASH_OUT        = "recent_cash_out"
    case RECENT_SEND_MONEY      = "recent_send_money"
    case RECENT_MOBILE_RECHARGE = "recent_mobile_recharge"
    case RECENT_MERCHANT        = "recent_merchant"
}
