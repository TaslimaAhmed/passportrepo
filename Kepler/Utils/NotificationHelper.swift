
//
//  NotificationHelper.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 6/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationHelper: NSObject {
    class func showNotification(message: String) {
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        content.title = "Success"
        content.body = message
        content.sound = .default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        
        let request = UNNotificationRequest(identifier: "success_notification", content: content, trigger: trigger)
        
        center.add(request) { (error) in
            if error != nil {
                DLog(error?.localizedDescription)
            }
        }
    }

}
