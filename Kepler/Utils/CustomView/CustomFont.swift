//
//  CustomFont.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 21/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

enum FontList: String, CustomStringConvertible {
    case Bold               = "Raleway-Bold"
    case BoldItalic         = "Raleway-BoldItalic"
    case ExtraBold          = "Raleway-ExtraBold"
    case ExtraBoldItalic    = "Raleway-ExtraBoldItalic"
    case ExtraLight         = "Raleway-ExtraLight"
    case ExtraLightItalic   = "Raleway-ExtraLightItalic"
    case Italic             = "Raleway-Italic"
    case Light              = "Raleway-Light"
    case LightItalic        = "Raleway-LightItalic"
    case Medium             = "Raleway-Medium"
    case MediumItalic       = "Raleway-MediumItalic"
    case Regular            = "Raleway-Regular"
    case SemiBold           = "Raleway-SemiBold"
    case SemiBoldItalic     = "Raleway-SemiBoldItalic"
    case Thin               = "Raleway-Thin"
    case ThinItalic         = "Raleway-ThinItalic"
    
    var name: String {
        get {
            switch self {
            case .Bold:
                return "Bold"
            case .BoldItalic:
                return "BoldItalic"
            case .ExtraBold:
                return "ExtraBold"
            case .ExtraBoldItalic:
                return "ExtraBoldItalic"
            case .ExtraLight:
                return "ExtraLight"
            case .ExtraLightItalic:
                return "ExtraLightItalic"
            case .Italic:
                return "Italic"
            case .Light:
                return "Light"
            case .LightItalic:
                return "LightItalic"
            case .Medium:
                return "Medium"
            case .MediumItalic:
                return "MediumItalic"
            case .Regular:
                return "Regular"
            case .SemiBold:
                return "SemiBold"
            case .SemiBoldItalic:
                return "SemiBoldItalic"
            case .Thin:
                return "Thin"
            case .ThinItalic:
                return "ThinItalic"
            }
        }
    }
    
    var description: String {
        get {
            return self.rawValue
        }
    }
}
