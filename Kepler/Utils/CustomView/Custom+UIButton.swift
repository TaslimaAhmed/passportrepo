//
//  Custom+UIButton.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 21/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

@IBDesignable
class CustomUIButton: UIButton {
    
    @IBInspectable var translationKey: String? {
        didSet {
            self.setUpButton()
        }
    }
    
    @IBInspectable var newBackgroundColor: String = "red" {
        didSet {
            self.setUpButton()
        }
    }
    @IBInspectable var newTextColor: String = "white" {
        didSet {
            self.setUpButton()
        }
    }
    @IBInspectable var newFontSize: CGFloat = 17.0 {
        didSet {
            self.setUpButton()
        }
    }
    
    var fontName: String = "Raleway-Regular"
    @IBInspectable var newFontType: String = "Regular" {
        didSet {
            switch newFontType {
            case FontList.Bold.name:
                fontName = FontList.Bold.rawValue
            case FontList.BoldItalic.name:
                fontName = FontList.BoldItalic.rawValue
            case FontList.ExtraBold.name:
                fontName = FontList.ExtraBold.rawValue
            case FontList.ExtraBoldItalic.name:
                fontName = FontList.ExtraBoldItalic.rawValue
            case FontList.ExtraLight.name:
                fontName = FontList.ExtraLight.rawValue
            case FontList.ExtraLightItalic.name:
                fontName = FontList.ExtraLightItalic.rawValue
            case FontList.Italic.name:
                fontName = FontList.Italic.rawValue
            case FontList.Light.name:
                fontName = FontList.Light.rawValue
            case FontList.LightItalic.name:
                fontName = FontList.LightItalic.rawValue
            case FontList.Medium.name:
                fontName = FontList.Medium.rawValue
            case FontList.MediumItalic.name:
                fontName = FontList.MediumItalic.rawValue
            case FontList.Regular.name:
                fontName = FontList.Regular.rawValue
            case FontList.SemiBold.name:
                fontName = FontList.SemiBold.rawValue
            case FontList.SemiBoldItalic.name:
                fontName = FontList.SemiBoldItalic.rawValue
            case FontList.Thin.name:
                fontName = FontList.Thin.rawValue
            case FontList.ThinItalic.name:
                fontName = FontList.ThinItalic.rawValue
            default:
                break
            }
            self.setUpButton()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUpButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUpButton()
    }
    
    func setUpButton() {
        if let color = ColorPalette(rawValue: newTextColor.lowercased()) {
            self.setTitleColor(namedColors[color.rawValue], for: .normal)
        }
        if let color = ColorPalette(rawValue: newBackgroundColor.lowercased()) {
            self.backgroundColor = namedColors[color.rawValue]
        }
        self.titleLabel?.font = UIFont(name: self.fontName, size: self.newFontSize)
        
        if let key = self.translationKey {
            self.text(key.localized)
        }
//        else {
//            assertionFailure("Translation not set for \(self.titleLabel?.text ?? "")")
//        }
    }
}
