//
//  TransactionHistoryVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 1/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD

class TransactionHistoryVC: UIViewController {
    
    @IBOutlet weak var transactionHistoryTableView: UITableView!
    @IBOutlet weak var noTransactionView: UIView!
    
    var refreshControl = UIRefreshControl()
    
    var itemInfo: IndicatorInfo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.transactionData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.transactionData()
    }
    
    private func setupView() {
        self.transactionHistoryTableView.dataSource = self
        self.transactionHistoryTableView.delegate = self
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refreshing Transaction History".localized)
        self.refreshControl.addTarget(self, action: #selector(self.transactionData), for: .valueChanged)
        self.transactionHistoryTableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc private func transactionData() {
        self.refreshControl.endRefreshing()
        TransactionHistoryVM.getTransactionList { (response, message) in
            if response {
                self.transactionHistoryTableView.reloadData()
            } else {
                DLog(message)
            }
        }
    }
}

extension TransactionHistoryVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if TransactionHistoryVM.transactionList.count == 0 {
            self.noTransactionView.isHidden = false
        } else {
            self.noTransactionView.isHidden = true
        }
        return TransactionHistoryVM.transactionList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionHistoryTVC", for: indexPath) as! TransactionHistoryTVC
        cell.transactionData = TransactionHistoryVM.transactionList[indexPath.row]
        return cell
    }
}

extension TransactionHistoryVC: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
