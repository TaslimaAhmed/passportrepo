//
//  TransactionHistoryVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 1/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

class TransactionHistoryVM {
    private static var transactionHistoryInstance: TransactionHistoryVM!
    
    public static var transactionList: [Transaction] = []
    public static var transactionTypeList: [FeaturesConfigList] = []
    
    private init() {
        
    }
    
    public static func getInstance() {
        if self.transactionHistoryInstance == nil {
            self.transactionHistoryInstance = TransactionHistoryVM()
            self.getTransactionType()
            self.getTransactionList()
        }
    }
    
    public static func getTransactionType() {
        AlamofireWrapper.requestGETURL(HttpURLs.transactionType, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            self.transactionTypeList = TransactionTypeResponse (JSONString: responseData)!.featuresConfigList!
        }) { (error) in
            DLog(error)
        }
    }
    
    public static func getTransactionList(response: @escaping(_ success: Bool, _ message: String)->Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.transactionHistory, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            self.transactionList = TransactionHistoryResponse (JSONString: responseData)!.statement!
            response(true, "Success")
        }) { (error) in
            DLog(error)
            response(false, DefaultMessages.ERROR_MESSAGE.rawValue.localized)
        }
    }
    
    public static func getTransactionList() {
        AlamofireWrapper.requestPOSTURL(HttpURLs.transactionHistory, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            self.transactionList = TransactionHistoryResponse (JSONString: responseData)!.statement!
        }) { (error) in
            DLog(error)
        }
    }
}
