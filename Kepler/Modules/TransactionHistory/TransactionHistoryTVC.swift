//
//  TransactionHistoryTVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 1/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class TransactionHistoryTVC: UITableViewCell {
    
    @IBOutlet weak var transactionTypeLabel: CustomLabel!
    @IBOutlet weak var recipientDetailsLabel: CustomLabel!
    @IBOutlet weak var transactionIDLabel: CustomLabel!
    
    @IBOutlet weak var transactionDateLabel: CustomLabel!
    @IBOutlet weak var amountLabel: CustomLabel!
    @IBOutlet weak var repeatTransactionLabel: CustomLabel!
    
    @IBOutlet weak var transactionCopyView: UIView!
    var popUpTimer: Timer!
    
    var transactionData: Transaction! {
        didSet {
            self.loadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    private func loadData() {
        self.transactionTypeLabel.text = self.transactionData.transactionType
        self.transactionIDLabel.text = "Trans ID: ".localized + "" + self.transactionData.txCode!
        self.transactionIDLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showCopyPopUp)))
        
        let fromDateFormat = DateFormatter()
        fromDateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let toDateFormat = DateFormatter()
        toDateFormat.dateFormat = "dd MMM, hh:mm a"
        
        if let fromDate = self.transactionData.transactionDate {
            if let toDate = fromDateFormat.date(from: fromDate) {
                self.transactionDateLabel.text = toDateFormat.string(from: toDate)
            } else {
                self.transactionDateLabel.text = ""
            }
        }
        
        let amount: Double = self.transactionData.amount!
        if transactionData.actionType!.elementsEqual("CREDIT") {
            if transactionData.transactionType!.elementsEqual("Pay") {
                self.transactionTypeLabel.text = "Received Money".localized
            }
            
            let name = ContactHelper.contactData.filter ({
                $0.number.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ".", with: "") == (String(format: "%.0f", self.transactionData.senderNumber!)).replacingOccurrences(of: "88", with: "")
            }).first?.name
            
            if name != nil && name != "" {
                self.recipientDetailsLabel.text = "\(name ?? "") (\(String(self.transactionData.senderNumber!).replacingOccurrences(of: "88", with: "").replacingOccurrences(of: ".0", with: "")))"
            } else {
                self.recipientDetailsLabel.text = "\(self.transactionData.senderName!) (\(String(self.transactionData.senderNumber!).replacingOccurrences(of: "88", with: "").replacingOccurrences(of: ".0", with: "")))"
            }
            
            if transactionData.transactionType!.elementsEqual("TransferFloraToTBMM") {
                self.recipientDetailsLabel.text = "\(self.transactionData.senderName!)"
            }
            self.amountLabel.text = "+ ৳ " + String(format: "%.2f", amount)
            self.amountLabel.textColor = UIColor.keplerGreen
        } else {
            if transactionData.transactionType!.elementsEqual("Pay") {
                self.transactionTypeLabel.text = "Send Money".localized
            } else if transactionData.transactionType!.elementsEqual("TopUp") {
                self.transactionTypeLabel.text = "Mobile Recharge".localized
            }
            let name = ContactHelper.contactData.filter ({
                $0.number.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ".", with: "") == (String(format: "%.0f", self.transactionData.receiverNumber!)).replacingOccurrences(of: "88", with: "")
            }).first?.name
            
            if name != nil && name != "" {
                self.recipientDetailsLabel.text = "\(name ?? "") (\(String(self.transactionData.receiverNumber!).replacingOccurrences(of: "88", with: "").replacingOccurrences(of: ".0", with: "")))"
            } else {
                self.recipientDetailsLabel.text = "\(self.transactionData.receiverName!) (\(String(self.transactionData.receiverNumber!).replacingOccurrences(of: "88", with: "").replacingOccurrences(of: ".0", with: "")))"
            }
            self.amountLabel.text = "- ৳ " + String(format: "%.2f", amount)
            self.amountLabel.textColor = UIColor.keplerRed
        }
        
        self.repeatTransactionLabel.isHidden = true
        self.repeatTransactionLabel.textColor = UIColor.keplerRed
        self.repeatTransactionLabel.attributedText = NSMutableAttributedString().underlined("Repeat Transction".localized, 10)
        self.repeatTransactionLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.repeatTransaction)))
    }
    
    @objc private func dismissPopup() {
        self.popUpTimer.invalidate()
        UIView.animate(withDuration: 0.5, delay: 0.2, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.transactionCopyView.alpha = 0
        }, completion: { finished in
            self.transactionCopyView.isHidden = true
        })
    }
    
    @objc private func showCopyPopUp() {
        UIPasteboard.general.string = self.transactionIDLabel.text?.replacingOccurrences(of: "Transaction ID: ", with: "")
        self.transactionCopyView.isHidden = false
        self.transactionCopyView.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0.2, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.transactionCopyView.alpha = 1
        }, completion: { finished in
            self.transactionCopyView.isHidden = false
        })
        self.popUpTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.dismissPopup), userInfo: nil, repeats: false)
    }
    
    @objc private func repeatTransaction() {
        
    }
}
