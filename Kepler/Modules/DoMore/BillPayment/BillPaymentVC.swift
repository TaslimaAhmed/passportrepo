//
//  BillPaymentVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class BillPaymentVC: UIViewController {
    
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var billPaymentCollectionView: UICollectionView!
    
    var isForUtility = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !isForUtility {
            self.headerLabel.text = "Others".localized
        }
        
        self.billPaymentCollectionView.dataSource = self
        self.billPaymentCollectionView.delegate = self
    }
}
