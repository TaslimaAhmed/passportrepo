//
//  BillPaymentVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension BillPaymentVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isForUtility {
            return BillingVM.utilityList.count
        } else {
            return BillingVM.otherList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BillPaymentCVC", for: indexPath) as! BillPaymentCVC
        if isForUtility {
            cell.setupCell(BillingVM.utilityList[indexPath.row])
        } else {
            cell.setupCell(BillingVM.otherList[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isForUtility {
            if indexPath.row == 0 {
                let vc = UIStoryboard(storyboard: .electricity).instantiateViewController(withIdentifier: ElectricityVC.identifier) as! ElectricityVC
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.showToast(message: .FEATURE_NOT_READY_ERROR)
            }
        } else {
            if indexPath.row == 0 {
                let vc = UIStoryboard(storyboard: .passport).instantiateViewController(withIdentifier: PassportVC.identifier) as! PassportVC
                
                vc.passportData = BillingVM.otherList[0]
                
                self.showToast(message: .FEATURE_NOT_READY_ERROR)
                self.navigationController?.pushViewController(vc, animated: true)
              
                
            } else if indexPath.row == 1 {
                let vc = UIStoryboard(storyboard: .nid).instantiateViewController(withIdentifier: NIDFeesVC.identifier) as! NIDFeesVC
                vc.nidData = BillingVM.otherList[1]
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.bounds.width / 3) - 10
        return CGSize(width: size, height: size + 20)
    }
}


