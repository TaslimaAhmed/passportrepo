//
//  NIDPaymentConfirmationVC.swift
//  Kepler
//
//  Created by Taslima Ahmed on 3/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class NIDPaymentConfirmationVC: UIViewController {

    @IBOutlet weak var nidNumberLabel: CustomLabel!
    @IBOutlet weak var correctionTypeLabel: CustomLabel!
    @IBOutlet weak var notitficationNumberLabel: CustomLabel!
    
    @IBOutlet weak var confirmLabel: CustomLabel!
    @IBOutlet weak var confirmButton: CustomUIButton!
    @IBOutlet weak var closeButton: CustomUIButton!
    
    @IBOutlet weak var pinTexField: UITextField!
    @IBOutlet weak var pinView: UIView!
    @IBOutlet weak var pinViewHeight: NSLayoutConstraint!
    
    var nidNumber, correctionTypeString, notificationNumber: String!
    var delegate: OnConfirmClickDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    private func setupView() {
        self.nidNumberLabel.text = self.nidNumber
        self.correctionTypeLabel.text = self.correctionTypeString
        self.notitficationNumberLabel.text = self.notificationNumber
        
        self.confirmButton.cornerRadius = self.confirmButton.bounds.width / 2
        self.closeButton.cornerRadius = self.closeButton.bounds.width / 2
        
        self.pinTexField.delegate = self
        self.pinTexField.defaultTextAttributes.updateValue(24.0, forKey: NSAttributedString.Key.kern)
        self.pinView.isHidden = true
        self.pinViewHeight.constant = 0
    }
    
    @IBAction func confirmClicked(_ sender: Any) {
        if self.pinTexField.text!.isEmpty {
            self.pinView.isHidden = false
            self.pinViewHeight.constant = 80.5
            self.pinTexField.becomeFirstResponder()
            self.confirmLabel.isHidden = true
            enableDisableNextButton(button: self.confirmButton)
        } else {
            self.dismiss(animated: true, completion: nil)
            self.delegate.onConfirmClicked(pin: self.pinTexField.text!.toBase64())
        }
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension NIDPaymentConfirmationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var pinLength = self.pinTexField.text!.count
        if textField == self.pinTexField {
            if string != "" {
                pinLength = self.pinTexField.text!.count + 1
            } else {
                pinLength = self.pinTexField.text!.count - 1
            }
        }
        
        if !pinTexField.text!.isEmpty {
            if pinLength >= 4 {
                enableDisableNextButton(button: self.confirmButton, true)
            } else {
                enableDisableNextButton(button: self.confirmButton)
            }
        }
        
        return true
    }
}
