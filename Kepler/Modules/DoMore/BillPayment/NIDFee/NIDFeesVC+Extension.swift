//
//  NIDFeesVC+Extension.swift
//  Kepler
//
//  Created by Taslima Ahmed on 3/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

extension NIDFeesVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.nidNumberTextField {
            self.nidNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerGray
        } else if textField == self.notificationNumberTextField {
            self.nidNumberUnderLine.backgroundColor = UIColor.keplerGray
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
        }
    }
}

extension NIDFeesVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Paying NID fees...".localized)
        
        let params = [
            "nidNo": self.nidNumberTextField.text?.replacingOccurrences(of: " ", with: ""),
            "serviceType": "1",
            "correctionType": selectedCorrectionType,
            "notificationNumber": self.notificationNumber,
            "pin": pin
        ] as [String : AnyObject]
        
        NIDFeesVM.payNIDFees(params: params) { (success, message) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if success {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as! SuccessSendVC
                    vc.additionalData = "NID Fee has been paid to Bangladesh Election Commission".localized
                    vc.totalAmountString = ""
                    vc.successDetailsMessage = message
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
