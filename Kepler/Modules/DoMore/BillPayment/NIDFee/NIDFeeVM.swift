//
//  NIDFeeVM.swift
//  Kepler
//
//  Created by Taslima Ahmed on 3/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation

class NIDFeesVM {
    private static var nidFeesInstance: NIDFeesVM!
    
    public static var nidServiceList: [NIDServicesData]!
    
    public static func getInstance() {
        if self.nidFeesInstance == nil {
            self.nidFeesInstance = NIDFeesVM()
            self.getNIDServiceList()
        }
    }
        
    class func getNIDServiceList() {
        AlamofireWrapper.requestGETURL(HttpURLs.nidServices, params: nil, headers: AlamofireWrapper().defaultHeader) { (response) in
            nidServiceList = [NIDServicesData] (JSONString: response)
        } failure: { (error) in
            DLog(error)
        }
    }
    
    class func payNIDFees(params: [String : AnyObject], complition: @escaping(_ isSuccess: Bool, _ message: String) -> Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.nidBillPayment, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            let message = NIDPaymentResponse (JSONString: response)
            complition(true, message!.message!)
        } failure: { (error) in
            DLog(error)
            complition(false, error)
        }
    }
}
