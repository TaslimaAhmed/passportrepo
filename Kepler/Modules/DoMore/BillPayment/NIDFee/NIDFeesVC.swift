//
//  NIDFeesVC.swift
//  Kepler
//
//  Created by Taslima Ahmed on 3/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class NIDFeesVC: UIViewController {
    @IBOutlet weak var nidImage: UIImageView!
    @IBOutlet weak var nidNumberTextField: CustomUITextField!
    @IBOutlet weak var nidNumberUnderLine: UIView!
    @IBOutlet weak var correctionTypeView: UIView!
    @IBOutlet weak var correctionTypeLabel: CustomLabel!
    @IBOutlet weak var notificationNumberView: UIView!
    @IBOutlet weak var notificationNumberTextField: CustomUITextField!
    @IBOutlet weak var notificationNumberUnderLine: UIView!
    @IBOutlet weak var nextButton: CustomUIButton!
    
    @IBOutlet weak var selfRadioView: UIStackView!
    @IBOutlet weak var selfRadio: CheckBox!
    @IBOutlet weak var otherRadioView: UIStackView!
    @IBOutlet weak var otherRadio: CheckBox!
    
    var selectedCorrectionType, selectedCorrectionTypeString, notificationNumber: String!
    var nidData: BillingItemList!
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.checkTextFields), userInfo: nil, repeats: true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.timer.invalidate()
    }
    
    private func setupView() {
        loadImage(imageURL: nidData.iconSource!, imageView: self.nidImage)
        
        self.nidNumberTextField.becomeFirstResponder()
        self.nidNumberTextField.delegate = self
        self.notificationNumberTextField.delegate = self
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        
        self.selfRadio.style = .circle
        self.selfRadio.tintColor = UIColor.keplerDarkGray
        self.selfRadio.borderStyle = .rounded
        self.selfRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkmarkColor = UIColor.keplerDarkGray
        self.selfRadio.useHapticFeedback = true
        
        self.otherRadio.style = .circle
        self.otherRadio.tintColor = UIColor.keplerDarkGray
        self.otherRadio.borderStyle = .rounded
        self.otherRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkmarkColor = UIColor.keplerDarkGray
        self.otherRadio.useHapticFeedback = true
        
        self.selfRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.selfRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.otherRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        self.otherRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        
        self.selectedCorrectionType = String(NIDFeesVM.nidServiceList[0].id!)
        self.selectedCorrectionTypeString = NIDFeesVM.nidServiceList[0].description
        self.correctionTypeLabel.text = NIDFeesVM.nidServiceList[0].description
        self.correctionTypeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showCorrectionType)))
        
        passportVM.loadAvailableBalance()
    }
    
    @objc private func onSelfRadioClick() {
        self.selfRadio.isChecked = true
        self.otherRadio.isChecked = false
        self.notificationNumberView.isHidden = true
        if !nidNumberTextField.text!.isEmpty {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    @objc private func onOtherRadioClick() {
        self.selfRadio.isChecked = false
        self.otherRadio.isChecked = true
        self.notificationNumberView.isHidden = false
        if !nidNumberTextField.text!.isEmpty && !notificationNumberTextField.text!.isEmpty {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    
    @objc private func checkTextFields() {
        if self.selfRadio.isChecked {
            if !nidNumberTextField.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        } else {
            if !nidNumberTextField.text!.isEmpty && !notificationNumberTextField.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        }
    }
    
    @objc private func showCorrectionType() {
        let correctionDialogVC = UIAlertController(title: "Select correction type".localized, message: "", preferredStyle: .actionSheet)
        NIDFeesVM.nidServiceList.forEach { (data) in
            let correctionAction = UIAlertAction(title: data.description, style: .default) { (action) in
                self.selectedCorrectionType = String(data.id!)
                self.selectedCorrectionTypeString = data.description
                self.correctionTypeLabel.text = data.description
            }
            correctionDialogVC.addAction(correctionAction)
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel) { (action) in
            correctionDialogVC.dismiss(animated: true, completion: nil)
        }
        correctionDialogVC.addAction(cancelAction)
        self.present(correctionDialogVC, animated: true, completion: nil)
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let vc = UIStoryboard(storyboard: .nid).instantiateViewController(withIdentifier: NIDPaymentConfirmationVC.identifier) as! NIDPaymentConfirmationVC
        vc.nidNumber = self.nidNumberTextField.text
        vc.correctionTypeString = self.selectedCorrectionTypeString
        vc.delegate = self
        if self.otherRadio.isChecked {
            vc.notificationNumber = self.notificationNumberTextField.text
            self.notificationNumber = self.notificationNumberTextField.text
        } else {
            vc.notificationNumber = SessionManager.getString(forKey: .USER_MSISDN)
            self.notificationNumber = SessionManager.getString(forKey: .USER_MSISDN)
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}
