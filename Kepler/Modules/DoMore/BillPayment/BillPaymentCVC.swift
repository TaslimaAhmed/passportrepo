//
//  BillPaymentCVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class BillPaymentCVC: UICollectionViewCell {
    @IBOutlet weak var iv_menu_icon: UIImageView!
    @IBOutlet weak var tv_menu_name: UILabel!
    
    override class func awakeFromNib() {
        
    }
    
    func setupCell(_ item: BillingListResponse){
        loadImage(imageURL: item.categoryIconSource!, imageView: iv_menu_icon)
        tv_menu_name.text = item.categoryTitle
    }
    
    func setupCell(_ item: BillingItemList){
        loadImage(imageURL: item.iconSource!, imageView: iv_menu_icon)
        tv_menu_name.text = item.description
    }
}
