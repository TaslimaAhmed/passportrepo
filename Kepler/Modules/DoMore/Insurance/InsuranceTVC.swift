//
//  InsuranceTVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 11/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class InsuranceTVC: UITableViewCell {

    @IBOutlet weak var insuranceImage: UIImageView!
    @IBOutlet weak var insuranceName: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            contentView.backgroundColor = UIColor.keplerLightBackground
        } else {
            contentView.backgroundColor = UIColor.keplerWhite
        }
    }
    
    func loadData(_ instituteData: BillingItemList) {
        loadImage(imageURL: instituteData.iconSource!, imageView: self.insuranceImage)
        self.insuranceName.text = instituteData.description
    }
}
