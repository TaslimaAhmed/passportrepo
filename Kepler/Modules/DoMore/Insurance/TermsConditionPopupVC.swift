//
//  TermsConditionPopupVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class TermsConditionPopupVC: UIViewController {
    
    @IBOutlet weak var nextButton: CustomUIButton!
    @IBOutlet weak var termsLabel: CustomLabel!
    
    var navController: UINavigationController!
    var insuranceData: BillingItemList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        
        termsLabel.attributedText = NSMutableAttributedString().normal("By proceeding, you agree to the ".localized, 15).underlined("Terms & Conditions".localized, 15)
        //            "By proceeding, you agree to the ".localized
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        let vc = UIStoryboard(storyboard: .insurance).instantiateViewController(withIdentifier: InsuranceTermsConditionVC.identifier) as! InsuranceTermsConditionVC
        vc.insuranceData = self.insuranceData
        vc.navController = self.navController
        self.present(vc, animated: true, completion: nil)
    }
}
