//
//  InsuranceVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 4/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class InsuranceVM {
    class func payInsurance(param: [String : AnyObject], complition: @escaping(_ success: Bool, _ message: String) -> Void) {
        SVProgressHUD.show()
        DLog(param)
        AlamofireWrapper.requestPOSTURL(HttpURLs.insurancePayment, params: param, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            complition(true, "Success")
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(false, error)
        }
    }
}
