//
//  InsuranceVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 6/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension InsuranceVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.recentInsuranceList != nil {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.recentInsuranceList != nil {
            if section == 0 {
                return recentInsuranceList.count
            } else {
                return insuranceList.count
            }
        } else {
            return insuranceList.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.recentInsuranceList != nil {
            if section == 0 {
                return "Recent Organizations".localized
            } else {
                return "All Organizations".localized
            }
        } else {
            return "All Organizations".localized
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40)
        let myLabel = UILabel()
        headerView.addSubview(myLabel)
        myLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            myLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 25),
            myLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor)
        ])
        myLabel.font = UIFont(name: FontList.ExtraBold.rawValue, size: 16)
        myLabel.attributedText = NSMutableAttributedString().extraBold(self.tableView(tableView, titleForHeaderInSection: section)!, 16)
        headerView.backgroundColor = UIColor.keplerWhite
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InsuranceTVC", for: indexPath) as! InsuranceTVC
        cell.loadData(self.insuranceList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.insuranceNameField.text = self.insuranceList[indexPath.row].description
        self.insuranceData = self.insuranceList[indexPath.row]
        if let termscondition = self.insuranceList[indexPath.row].termsAndConditions, !termscondition.elementsEqual("") {
            let vc =  UIStoryboard(storyboard: .insurance).instantiateViewController(withIdentifier: TermsConditionPopupVC.identifier) as! TermsConditionPopupVC
            vc.insuranceData = self.insuranceData
            vc.navController = self.navigationController
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = UIStoryboard(storyboard: .insurance).instantiateViewController(withIdentifier: InsurancePaymentVC.identifier) as! InsurancePaymentVC
            vc.insuranceData = self.insuranceData
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension InsuranceVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            return false
        }
        if textField == self.insuranceNameField {
            var searchString = self.insuranceNameField.text! + "" + string
            if string == "" {
                searchString.removeLast()
            }
            
            if string.isEmpty && range.location == 0 {
                self.insuranceList = self.searchInsuranceList
                self.recentInsuranceList = self.recentTempInsuranceList
                self.insuranceTableView.reloadData()
            } else {
                self.recentInsuranceList = nil
                self.insuranceList = self.searchInsuranceList.filter{$0.description!.lowercased().contains(searchString.lowercased())}
                self.insuranceTableView.reloadData()
            }
        }
        
        return true
    }
}
