//
//  InsuranceVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 4/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class InsuranceVC: UIViewController {
    
    @IBOutlet weak var insuranceTableView: UITableView!
//    @IBOutlet weak var insuranceTableHeight: NSLayoutConstraint!
    @IBOutlet weak var insuranceNameField: CustomUITextField!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var insuranceList: [BillingItemList] = []
    var searchInsuranceList: [BillingItemList] = []
    var recentInsuranceList: [BillingItemList]!
    var recentTempInsuranceList: [BillingItemList]!
    var insuranceData: BillingItemList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadRecentInstitute()
        self.setupView()
        self.loadData()
    }
    
    func setupView() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        self.insuranceNameField.delegate = self
        
        self.insuranceTableView.dataSource = self
        self.insuranceTableView.delegate = self
    }
    
    private func loadRecentInstitute() {
        if SessionManager.hasValue(forKey: .RECENT_INSURANCE) {
            self.recentInsuranceList = [BillingItemList] (JSONString: SessionManager.getString(forKey: .RECENT_INSURANCE)!)!
            self.recentTempInsuranceList = self.recentInsuranceList
        }
    }
    
    private func loadData() {
        self.insuranceList = BillingVM.insuranceList
        self.searchInsuranceList = self.insuranceList
        self.insuranceTableView.reloadData()
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        //        if !self.insuranceNameField.text!.isEmpty {
        //            let vc = UIStoryboard(storyboard: .tuition_fee).instantiateViewController(withIdentifier: TuitionFeePaymentVC.identifier) as! TuitionFeePaymentVC
        //            vc.institueData = self.insuranceData
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        } else {
        //            self.showToast(message: .FILL_ALL_FIELDS)
        //        }
    }
}
