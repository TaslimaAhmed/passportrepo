//
//  InsurancePaymentVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 18/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class InsurancePaymentVC: UIViewController {
    
    @IBOutlet weak var insuranceImage: UIImageView!
    @IBOutlet weak var insuranceNameLabel: CustomLabel!
    @IBOutlet weak var policyNumberTextField: CustomUITextField!
    @IBOutlet weak var policyNumberUnderLine: UIView!
    @IBOutlet weak var amountTextField: CustomUITextField!
    @IBOutlet weak var amountUnderLine: UIView!
    @IBOutlet weak var notificationNumberView: UIView!
    @IBOutlet weak var notificationNumberTextField: CustomUITextField!
    @IBOutlet weak var notificationNumberUnderLine: UIView!
    @IBOutlet weak var availableBalanceLabel: CustomLabel!
    
    @IBOutlet weak var selfRadioView: UIStackView!
    @IBOutlet weak var selfRadio: CheckBox!
    @IBOutlet weak var otherRadioView: UIStackView!
    @IBOutlet weak var otherRadio: CheckBox!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var notificationNumber: String = SessionManager.getString(forKey: .USER_MSISDN)!
    var timer: Timer!
    var insuranceData: BillingItemList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.enableDisableNextButton()
        self.setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.checkTextFields), userInfo: nil, repeats: true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.timer.invalidate()
    }
    
    private func setupView() {
        loadImage(imageURL: insuranceData.iconSource!, imageView: self.insuranceImage)
        self.insuranceNameLabel.text = self.insuranceData.description
        self.policyNumberTextField.becomeFirstResponder()
        self.policyNumberTextField.delegate = self
        self.amountTextField.delegate = self
        self.notificationNumberTextField.delegate = self
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        
        self.selfRadio.style = .circle
        self.selfRadio.tintColor = UIColor.keplerDarkGray
        self.selfRadio.borderStyle = .rounded
        self.selfRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkmarkColor = UIColor.keplerDarkGray
        self.selfRadio.useHapticFeedback = true
        
        self.otherRadio.style = .circle
        self.otherRadio.tintColor = UIColor.keplerDarkGray
        self.otherRadio.borderStyle = .rounded
        self.otherRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkmarkColor = UIColor.keplerDarkGray
        self.otherRadio.useHapticFeedback = true
        
        self.selfRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.selfRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.otherRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        self.otherRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        
        self.loadExistingBalance()
    }
    
    private func loadExistingBalance() {
        HomeVM.checkBalance() { (response, message) in
            if response {
                self.availableBalanceLabel.text = String(format: "৳ %.2f", (message as NSString).doubleValue)
                SessionManager.setValue(message, forKey: .AVAILABLE_BALANCE)
            } else {
                self.showToast(message: message)
            }
        }
    }
    
    @objc private func checkTextFields() {
        if self.selfRadio.isChecked {
            if !self.policyNumberTextField.text!.isEmpty && !self.amountTextField.text!.isEmpty {
                self.enableDisableNextButton(true)
            } else {
                self.enableDisableNextButton()
            }
        } else {
            if !self.policyNumberTextField.text!.isEmpty && !self.amountTextField.text!.isEmpty && !self.notificationNumberTextField.text!.isEmpty {
                self.enableDisableNextButton(true)
            } else {
                self.enableDisableNextButton()
            }
        }
    }
    
    func enableDisableNextButton(_ shouldEnable: Bool = false) {
        if shouldEnable {
            self.nextButton.isEnabled = true
            self.nextButton.alpha = 1
        } else {
            self.nextButton.isEnabled = false
            self.nextButton.alpha = 0.5
        }
    }
    
    @objc private func onSelfRadioClick() {
        self.selfRadio.isChecked = true
        self.otherRadio.isChecked = false
        self.notificationNumberView.isHidden = true
    }
    @objc private func onOtherRadioClick() {
        self.selfRadio.isChecked = false
        self.otherRadio.isChecked = true
        self.notificationNumberView.isHidden = false
    }
    
    @IBAction func nexButtonClicked(_ sender: Any) {
        let availableBalance = (self.availableBalanceLabel.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
        let transactionType = TransactionHistoryVM.transactionTypeList.filter { $0.featureName == "INSURANCE_PAYMENT" }.first
        let payamount = (self.amountTextField.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
        
        if notificationNumberTextField.text!.isEmpty {
            self.notificationNumber = SessionManager.getString(forKey: .USER_MSISDN)!
        } else {
            self.notificationNumber = self.notificationNumberTextField.text!
        }
        
        if payamount <= availableBalance {
            let amount = (self.amountTextField.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
            if amount >= Double(transactionType!.minAmount!) {
                if amount <= Double(transactionType!.maxAmount!) || transactionType!.maxAmount! == -1 {
                    ChargeVM.getCharge(fromUserNumber: self.notificationNumber, toUserNumber: nil, transactionType: self.insuranceData.code!, amount: String(amount)) { (isSuccessful, message) in
                        if isSuccessful {
                            let totalAmount = (Double(message) ?? 0.0) + amount
                            if totalAmount <= availableBalance {
                                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ConfirmationVC.identifier) as! ConfirmationVC
                                vc.isForOthers = true
                                vc.recipientName = self.insuranceData.description
                                vc.additionalMessage = "Policy number: \(self.policyNumberTextField.text!)"
                                vc.sendingLabel = "Paying".localized
                                vc.totalAmount = String(amount)
                                vc.chargeAmount = message
                                vc.onConfirmClickDelegate = self
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                                vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                                self.present(vc, animated: true, completion: nil)
                            }
                        } else {
                            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                            vc.errorMessage = message
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = "Amount should be less than ".localized + "\(transactionType!.maxAmount!)" + " taka".localized
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = "Amount should be more than ".localized + "\(transactionType!.minAmount!)" + " taka".localized
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension InsurancePaymentVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.policyNumberTextField {
            self.policyNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
            self.amountUnderLine.backgroundColor = UIColor.keplerGray
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerGray
        } else if textField == self.amountTextField {
            self.policyNumberUnderLine.backgroundColor = UIColor.keplerGray
            self.amountUnderLine.backgroundColor = UIColor.keplerTextUnderLine
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerGray
        } else {
            self.policyNumberUnderLine.backgroundColor = UIColor.keplerGray
            self.amountUnderLine.backgroundColor = UIColor.keplerGray
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.amountTextField {
            if !textField.text!.isEmpty && textField.text!.count >= 2 {
                if !self.amountTextField.text!.isEmpty {
                    self.amountTextField.text = "৳ " + self.amountTextField.text!.replacingOccurrences(of: "৳ ", with: "")
                }
            } else {
                self.amountTextField.text = "৳ "
            }
        }
        return true
    }
}

extension InsurancePaymentVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Loading...".localized)
        
        let params = [
            "code" : insuranceData.code!,
            "amount" : self.amountTextField.text!.replacingOccurrences(of: "৳ ", with: ""),
            "notificationNumber" : notificationNumber,
            "policyNo" : self.policyNumberTextField.text!,
            "pin" : pin
        ] as [String : AnyObject]
        
        InsuranceVM.payInsurance(param: params) { (isSuccess, message) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if isSuccess {
                    //For adding recent desco prepaid into local
                    if SessionManager.hasValue(forKey: .RECENT_INSURANCE) {
                        var recentInsurance: [BillingItemList] = [BillingItemList](JSONString: SessionManager.getString(forKey: .RECENT_INSURANCE)!)!
                        if recentInsurance.filter({ $0.code == self.insuranceData.code }).count == 0 {
                            recentInsurance.append(self.insuranceData)
                            if recentInsurance.count > 3 {
                                recentInsurance.removeFirst()
                            }
                            SessionManager.setValue(recentInsurance.toJSONString(), forKey: .RECENT_INSURANCE)
                        }
                    } else {
                        var recentInsurance: [BillingItemList] = []
                        recentInsurance.append(self.insuranceData)
                        SessionManager.setValue(recentInsurance.toJSONString(), forKey: .RECENT_INSURANCE)
                    }
                    //For adding recent mobile recharge into local
                    
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as! SuccessSendVC
                    vc.additionalData = self.insuranceData.description!
                    vc.totalAmountString = self.amountTextField.text! + " has been paid to".localized
                    vc.successDetailsMessage = message
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
