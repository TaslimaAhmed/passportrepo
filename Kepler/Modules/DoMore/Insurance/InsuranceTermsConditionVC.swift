//
//  InsuranceTermsConditionVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class InsuranceTermsConditionVC: UIViewController {

    @IBOutlet weak var companyNameLabel: CustomLabel!
    @IBOutlet weak var termsWebView: UIWebView!
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var insuranceData: BillingItemList!
    var navController: UINavigationController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let vc = UIStoryboard(storyboard: .insurance).instantiateViewController(withIdentifier: InsurancePaymentVC.identifier) as! InsurancePaymentVC
        vc.insuranceData = self.insuranceData
        self.navController?.pushViewController(vc, animated: true)
        dismiss(animated: true, completion: nil)
    }
}
