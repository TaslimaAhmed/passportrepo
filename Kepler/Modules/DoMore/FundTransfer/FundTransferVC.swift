//
//  FundTransferVC.swift
//  Kepler
//
//  Created by Taslima Ahmed on 23/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class FundTransferVC: UIViewController {
   
    
   
    @IBOutlet var userMobileNumber: CustomLabel!
    @IBOutlet weak var amountField: CustomUITextField!
    @IBOutlet weak var availableAmountFiled: CustomLabel!
    @IBOutlet var fundAccountNumberTextField: CustomUITextField!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var fundData: BillingItemList?
    
   // var accountNumber: String!
    var timer: Timer!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.setupView()
       // self.loadExistingBalance()

    }
  
    
    
    private func setupView() {
       
        //loadImage(imageURL: (fundData?.iconSource!)!, imageView: self.fundImage)
        enableDisableNextButton(button: self.nextButton)
        
      self.userMobileNumber.text = SessionManager.getString(forKey: .USER_MSISDN)
      
        self.amountField.delegate = self
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.loadExistingBalance()
        FundTransferVM.getAccounts()
        self.timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(self.checkInputFiled), userInfo: nil, repeats: true)
    
    }
    
    @objc private func checkInputFiled() {
        if !self.fundAccountNumberTextField.text!.isEmpty && !self.amountField.text!.isEmpty && self.amountField.text!.count > 2 {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    
    
    @IBAction func bankListDropDownClicked(_ sender: Any) {
        if FundTransferVM.existingAccounts.count > 0 {
            let bankListAlert = UIAlertController(title: "Linked Account".localized, message: "", preferredStyle: .actionSheet)
            FundTransferVM.existingAccounts.forEach { (accountInfo) in
                let bankListDataAction = UIAlertAction(title: accountInfo.maskedAccountNo, style: .default) { (action) in
                    self.fundAccountNumberTextField.text = accountInfo.maskedAccountNo
                }
                bankListAlert.addAction(bankListDataAction)
            }
            let cancleAlert = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                bankListAlert.dismiss(animated: true, completion: nil)
            }
            bankListAlert.addAction(cancleAlert)
            self.present(bankListAlert, animated: true, completion: nil)
        }
    }
 
    private func loadExistingBalance() {
        HomeVM.checkBalance() { (response, message) in
            if response {
                self.availableAmountFiled.text = String(format: "৳ %.2f", (message as NSString).doubleValue)
                SessionManager.setValue(message, forKey: .AVAILABLE_BALANCE)
            } else {
                self.showToast(message: message)
            }
        }
    }
    
    
    @IBAction func nextPaymentClicked(_ sender: UIButton) {
        if FundTransferVM.existingAccounts.count > 0 {
            let bankListAlert = UIAlertController(title: "Linked Account".localized, message: "", preferredStyle: .actionSheet)
            FundTransferVM.existingAccounts.forEach { (accountInfo) in
                let bankListDataAction = UIAlertAction(title: accountInfo.maskedAccountNo, style: .default) { (action) in
                    self.fundAccountNumberTextField.text = accountInfo.maskedAccountNo
                }
                bankListAlert.addAction(bankListDataAction)
            }
            let cancleAlert = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                bankListAlert.dismiss(animated: true, completion: nil)
            }
            bankListAlert.addAction(cancleAlert)
            self.present(bankListAlert, animated: true, completion: nil)
        }
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let availableBalance = (self.availableAmountFiled.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
        let transactionType = TransactionHistoryVM.transactionTypeList.filter { $0.featureName == "TBL_ACCOUNT_DEPOSIT" }.first
        let amount = (self.amountField.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
        
        if amount <= availableBalance {
            if amount >= Double(transactionType!.minAmount!) {
                if amount <= Double(transactionType!.maxAmount!) || transactionType!.maxAmount! == -1 {
                    ChargeVM.getCharge(fromUserNumber: SessionManager.getString(forKey: .USER_MSISDN)!, toUserNumber: nil, transactionType: transactionType!.code!, amount: String(amount)) { (isSuccessful, message) in
                        if isSuccessful {
                            let totalAmount = (Double(message) ?? 0.0) + amount
                            if totalAmount <= availableBalance {
                                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ConfirmationVC.identifier) as! ConfirmationVC
                                vc.isForOthers = true
                                vc.recipientName = "\("Bank A/C".localized) (\(self.fundAccountNumberTextField.text!))"
                                vc.sendingLabel = "Transferring".localized
                                vc.totalAmount = String(format: "%.2f", amount)
                                vc.chargeAmount = message
                                vc.onConfirmClickDelegate = self
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                                vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                                self.present(vc, animated: true, completion: nil)
                            }
                        } else {
                            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                            vc.errorMessage = message
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = "Amount should be maximum ".localized + "\(transactionType!.maxAmount!)" + " taka".localized
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = "Amount should be minimum ".localized + "\(transactionType!.minAmount!)" + " taka".localized
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension FundTransferVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            return false
        }
        if textField == self.amountField {
            if !textField.text!.isEmpty && textField.text!.count >= 2 {
                if !self.amountField.text!.isEmpty {
                    self.amountField.text = "৳ " + self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")
                }
            } else {
                self.amountField.text = "৳ "
            }
        }
        return true
    }
}
extension FundTransferVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Transferring...".localized)
        
        let parmas = [
            "accountNumber" : self.fundAccountNumberTextField.text!,
            "amount" : self.amountField.text!.replacingOccurrences(of: "৳ ", with: ""),
            "pin" : pin
        ] as [String : AnyObject]
        
        FundTransferVM.payFundBill(params: parmas) { (message, isSuccess) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if isSuccess {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as! SuccessSendVC
                    vc.additionalData = "Bank Account".localized
                    vc.totalAmountString = self.amountField.text! + " has been transferred to".localized
                    vc.successDetailsMessage = message
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}



















