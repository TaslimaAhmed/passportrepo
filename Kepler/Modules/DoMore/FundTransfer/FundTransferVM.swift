//
//  FundTransferVM.swift
//  Kepler
//
//  Created by Taslima Ahmed on 23/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation

class FundTransferVM {
    static var existingAccounts : [AccountInfo] = []
    
    static func getAccounts() {
        AlamofireWrapper.requestPOSTURL(HttpURLs.linkedAccounts, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            let connectedAccountsResponse = FundTransferServiceData (JSONString: responseData)
            self.existingAccounts = connectedAccountsResponse!.accountInfo!
        }) { (error) in
            DLog(error)
        }
    }

  
    

    static func payFundBill(params: [String : AnyObject], complition: @escaping(_ message: String, _ isSuccess: Bool) -> Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.fundTransfer, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            let message = FundPaymentResponse (JSONString: response)
            complition(message!.message!, true)
        } failure: { (error) in
            DLog(error)
            complition(error, false)
            
            
            
        
            
            
        }
    }
}
