//
//  DoMoreVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 5/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class DoMoreVC: UIViewController {
    
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var doMoreCollectionView: UICollectionView!
    
    var itemInfo: IndicatorInfo!
    var doMoreMenu: [HomeMenu] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    func setupView() {
        loadImage(imageURL: HttpURLs.bannerImage, imageView: self.bannerImage)
        doMoreMenu = gatherData()
        self.doMoreCollectionView.delegate = self
        self.doMoreCollectionView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        doMoreMenu = gatherData()
        self.doMoreCollectionView.reloadData()
    }
    
    private func gatherData() -> [HomeMenu] {
        var temp:[HomeMenu] = []
        
        let featureLists = LoginRegistrationVM.loginResponse!.userLoginInfo!.featureAccesibilityMatrix!.featureList!
        let menu1 = HomeMenu(name: .INSURANCE, image: UIImage(named: .INSURANCE), featureName: "INSURANCE_PAYMENT", isActive: true)
        let menu2 = HomeMenu(name: .BILL_PAYMENT, image: UIImage(named: .BILL_PAY), featureName: "BILL_PAYMENT", isActive: true)
        let menu3 = HomeMenu(name: .TUITION_FEES, image: UIImage(named: .TUITION_FEES), featureName: "TUTION_FEE_PAYMENT", isActive: true)
        let menu4 = HomeMenu(name: .OTHERS, image: UIImage(named: .OTHERS), featureName: "OTHERS_PAYMENT", isActive: true)
        let menu5 = HomeMenu(name: .FUND, image: UIImage(named: .Fund), featureName: "FUND_PAY", isActive: true)
        
        if !SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.SUCCESS.rawValue) {
            featureLists.forEach { feature in
                if feature.featureName == "INSURANCE_PAYMENT" {
                    menu1.isActive = feature.featureAccess!
                }
                if feature.featureName == "BILL_PAYMENT" {
                    menu2.isActive = feature.featureAccess!
                }
                if feature.featureName == "TUTION_FEE_PAYMENT" {
                    menu3.isActive = feature.featureAccess!
                }
                if feature.featureName == "OTHERS_PAYMENT" {
                    menu4.isActive = feature.featureAccess!
                if feature.featureName == "FUND_PAY" {
                    menu5.isActive = feature.featureAccess!
                }
                    
        
//        let menu1 = HomeMenu(name: .BILL_PAYMENT, image: UIImage(named: .BILL_PAY), featureName: "BILL_PAYMENT", isActive: true)
//        let menu2 = HomeMenu(name: .TUITION_FEES, image: UIImage(named: .TUITION_FEES), featureName: "TUTION_FEE_PAYMENT", isActive: true)
//        let menu3 = HomeMenu(name: .FUND, image: UIImage(named: .Fund), featureName: "FUND_PAY", isActive: true)
//        let menu4 = HomeMenu(name: .INSURANCE, image: UIImage(named: .INSURANCE), featureName: "INSURANCE_PAYMENT", isActive: true)
//
//        let menu5 = HomeMenu(name: .OTHERS, image: UIImage(named: .OTHERS), featureName: "OTHERS_PAYMENT", isActive: true)
//
//
//
//
//        if !SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.SUCCESS.rawValue) {
//            featureLists.forEach { feature in
//                if feature.featureName == "BILL_PAYMENT" {
//                    menu1.isActive = feature.featureAccess!
//                }
//                if feature.featureName == "TUTION_FEE_PAYMENT" {
//                    menu2.isActive = feature.featureAccess!
//                }
//                if feature.featureName == "FUND_PAY" {
//                    menu3.isActive = feature.featureAccess!
//                }
//                if feature.featureName == "INSURANCE_PAYMENT" {
//                    menu4.isActive = feature.featureAccess!
//                }
//                if feature.featureName == "OTHERS_PAYMENT" {
//                    menu5.isActive = feature.featureAccess!
                }
            }
        }
        
        temp.append(menu1)
        temp.append(menu2)
        temp.append(menu3)
        temp.append(menu4)
        temp.append(menu5)
        return temp
    }
}

extension DoMoreVC: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
