//
//  passportVM.swift
//  Kepler
//
//  Created by Taslima Ahmed on 28/1/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class passportVM {
    public static var currentBalance: String!
    
    class func loadAvailableBalance() {
        AlamofireWrapper.requestPOSTURL(HttpURLs.balanceCheck,params: nil, headers: AlamofireWrapper().defaultHeader) { (response) in
            let ballanceResposen = CheckBlanceResponse(JSONString: response)
            DpdcVM.currentBalance = String(format: "%.2f", (ballanceResposen!.currentBalance! as NSString).doubleValue)
        } failure: { (error) in
            DLog(error)
        }
    }
    
    class func payPassportBill(params: [String : AnyObject], complition: @escaping(_ success: Bool, _ message: String) -> Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.passportBillPayment, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            let message = PassportPaymentResponse (JSONString: response)
            complition(true, message!.message!)
        } failure: { (error) in
            DLog(error)
            complition(false, error)
        }
    }
}
