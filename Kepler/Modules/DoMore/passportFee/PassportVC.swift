//
//  PassportVC.swift
//  Kepler
//
//  Created by Taslima Ahmed on 23/2/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class PassportVC: UIViewController {
    
    @IBOutlet weak var passportImage: UIImageView!
    
    @IBOutlet weak var passportLabel: UILabel!
    @IBOutlet weak var underlineFirst: UIView!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var firstNameUnderline: UIView!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var lastNameUnderline: UIView!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var emailUnderline: UIView!
    
    @IBOutlet weak var reEmailTextField: UITextField!
    
    
    @IBOutlet weak var reEmailUnderline: UIView!
    
    
    @IBOutlet weak var selfRadioView: UIStackView!
    @IBOutlet weak var selfRadio: CheckBox!
    @IBOutlet weak var otherRadioView: UIStackView!
    @IBOutlet weak var otherRadio: CheckBox!
    @IBOutlet weak var notificationNumberView: UIView!
    @IBOutlet weak var notificationNumberTextField: CustomUITextField!
    @IBOutlet weak var notificationNumberUnderLine: UIView!
    @IBOutlet weak var nextButton: CustomUIButton!
    @IBOutlet weak var emailDoesNotMatchLabel: CustomLabel!
    var passportData: BillingItemList!
    var timer: Timer!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.checkTextFields), userInfo: nil, repeats: true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.timer.invalidate()
    }
    
    private func setupView() {
        
        loadImage(imageURL: passportData.iconSource!, imageView: self.passportImage)

    
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.selfRadio.style = .circle
        self.selfRadio.tintColor = UIColor.keplerDarkGray
        self.selfRadio.borderStyle = .rounded
        self.selfRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkmarkColor = UIColor.keplerDarkGray
        self.selfRadio.useHapticFeedback = true
        
        self.otherRadio.style = .circle
        self.otherRadio.tintColor = UIColor.keplerDarkGray
        self.otherRadio.borderStyle = .rounded
        self.otherRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkmarkColor = UIColor.keplerDarkGray
        self.otherRadio.useHapticFeedback = true
        
        self.selfRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.selfRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.otherRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        self.otherRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        passportVM.loadAvailableBalance()
    }
    
//    func enableDisableNextButton(_ shouldEnable: Bool = false) {
//        if shouldEnable {
//            self.nextButton.isEnabled = true
//            self.nextButton.alpha = 1
//        } else {
//            self.nextButton.isEnabled = false
//            self.nextButton.alpha = 0.5
//        }
//    }
    
    @objc private func onSelfRadioClick() {
        self.selfRadio.isChecked = true
        self.otherRadio.isChecked = false
        self.notificationNumberView.isHidden = true
    }
    @objc private func onOtherRadioClick() {
        self.selfRadio.isChecked = false
        self.otherRadio.isChecked = true
        self.notificationNumberView.isHidden = false
    }
    @objc private func checkTextFields() {
        if self.selfRadio.isChecked {
            if !firstNameTextField.text!.isEmpty && !lastNameTextField.text!.isEmpty && !emailTextfield.text!.isEmpty && !reEmailTextField.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        } else {
            if !firstNameTextField.text!.isEmpty && !lastNameTextField.text!.isEmpty && !emailTextfield.text!.isEmpty && !reEmailTextField.text!.isEmpty && !notificationNumberTextField.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        }
    }
    
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        if self.emailTextfield.text!.replacingOccurrences(of: " ", with: "").isValidEmail &&
            self.reEmailTextField.text!.replacingOccurrences(of: " ", with: "").isValidEmail {
            if self.emailTextfield.text!.replacingOccurrences(of: " ", with: "").elementsEqual(self.reEmailTextField.text!.replacingOccurrences(of: " ", with: "")) {
                self.emailDoesNotMatchLabel.isHidden = true
                
                let vc = UIStoryboard(storyboard: .passport).instantiateViewController(withIdentifier: passportPayment.identifier) as! passportPayment
                vc.passportData = self.passportData
                vc.firstName = self.firstNameTextField.text
                vc.lastName = self.lastNameTextField.text
                vc.emailAddress = self.emailTextfield.text!.replacingOccurrences(of: " ", with: "")
                if self.otherRadio.isChecked {
                    vc.notificationNumber = self.notificationNumberTextField.text
                } else {
                    vc.notificationNumber = SessionManager.getString(forKey: .USER_MSISDN)
                }
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.emailDoesNotMatchLabel.text = "Email does not match. Please try again.".localized
                self.emailDoesNotMatchLabel.isHidden = false
            }
        } else {
            self.emailDoesNotMatchLabel.text = "Please enter a valid email address".localized
            self.emailDoesNotMatchLabel.isHidden = false
        }
    }
}

    
    
    

   


