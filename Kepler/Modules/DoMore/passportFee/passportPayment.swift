//
//  passportPayment.swift
//  Kepler
//
//  Created by Taslima Ahmed on 28/1/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class passportPayment: UIViewController {
    @IBOutlet weak var passportImage: UIImageView!
    
    @IBOutlet weak var amountField: CustomUITextField!
    @IBOutlet weak var availableAmountFiled: CustomLabel!
    
   
    
    
   
    
    var firstName, lastName, emailAddress, notificationNumber: String?
    var passportData: BillingItemList?
    
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.loadExistingBalance()
        
        

        
    }
   
 
   
       private func  setupView() {
        
        loadImage(imageURL: (passportData?.iconSource!)!, imageView: self.passportImage)
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        
        self.amountField.delegate = self
        self.amountField.becomeFirstResponder()
        
        
    }
    
 
    
    private func loadExistingBalance() {
        HomeVM.checkBalance() { (response, message) in
            if response {
                self.availableAmountFiled.text = String(format: "৳ %.2f", (message as NSString).doubleValue)
                SessionManager.setValue(message, forKey: .AVAILABLE_BALANCE)
            } else {
                self.showToast(message: message)
            }
        }
    }
    



    
   
        
    @IBAction func nextPaymentClicked(_ sender: UIButton) {
        if !amountField.text!.isEmpty {
            let amount = (self.amountField.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
            let availableBalance = (self.availableAmountFiled.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
            //            let transactionType = TransactionHistoryVM.transactionTypeList.filter { $0.featureName == "CASH_OUT" }.first
            //
            if amount >= 1.0 {
                //                if amount <= Double(transactionType!.maxAmount!) {
                if amount <= availableBalance {
                    //                        ChargeVM.getCharge(fromUserNumber: self.msisdn, toUserNumber: SessionManager.getString(forKey: .USER_MSISDN)!, transactionType: transactionType!.code!, amount: String(amount)) { (isSuccessful, message) in
                    //                            if isSuccessful {
                    //                                let totalAmount = (Double(message) ?? 0.0) + amount
                    //                                if totalAmount <= availableBalance {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ConfirmationVC.identifier) as! ConfirmationVC
                    vc.isForOthers = true
                    vc.recipientName = self.passportData?.description!
                    vc.additionalMessage = "\("Email address".localized): \(emailAddress!)"
                    vc.sendingLabel = "Paying".localized
                    vc.totalAmount = String(format: "%.2f", amount)
                    vc.chargeAmount = "0.00"
                    //vc.onConfirmClickDelegate = self
                    vc.onConfirmClickDelegate = self
                    self.present(vc, animated: true, completion: nil)
                    //                                } else {
                    //                                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    //                                    vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                    //                                    self.present(vc, animated: true, completion: nil)
                    //                                }
                    //                            } else {
                    //                                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    //                                vc.errorMessage = message
                    //                                self.present(vc, animated: true, completion: nil)
                    //                            }
                    //                        }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                    self.present(vc, animated: true, completion: nil)
                }
                //                } else {
                //                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                //                    vc.errorMessage = "Amount should be maximum ".localized + "\(transactionType!.maxAmount!)" + " taka".localized
                //                    self.present(vc, animated: true, completion: nil)
                //                }
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = "Amount should be minimum ".localized + "1" + " taka".localized
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = DefaultMessages.EMPTY_AMOUNT_FIELD.rawValue.localized
            self.present(vc, animated: true, completion: nil)
        }

    }
}
    
    extension passportPayment: UITextFieldDelegate {
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if !textField.text!.isEmpty && textField.text!.count >= 2 {
                if textField == self.amountField && !self.amountField.text!.isEmpty {
                    self.amountField.text = "৳ " + self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")
                }
            } else {
                self.amountField.text = "৳ "
            }
            return true
        }
    }

    extension passportPayment: OnConfirmClickDelegate {
        func onConfirmClicked(pin: String) {
            let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Paying passport bill...".localized)
            let totalAmount = self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")
            
            let params = [
                "firstName" : self.firstName,
                "lastName" : self.lastName,
                "email" : self.emailAddress,
                "amount" : totalAmount,
                "notificationNumber" : self.notificationNumber,
                "pin" : pin
            ] as [String : AnyObject]
            
            passportVM.payPassportBill(params: params) { (response, message) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    loadinVC.dismisViewController()
                    if response {
                        if let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as? SuccessSendVC {
                            vc.additionalData = (self.passportData!.description!)
                            vc.totalAmountString = self.amountField.text! + " has been paid to".localized
                            vc.successDetailsMessage = message
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    } else {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                        vc.errorMessage = message
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }

    
    








