//
//  BillingVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 16/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

class BillingVM {
    
    private static var billingInstance: BillingVM!
    
    public static var insuranceList: [BillingItemList]!
    public static var institutionList: [BillingItemList]!
    public static var utilityList: [BillingListResponse]!
    public static var electricityList: [BillingItemList]!
    public static var gasList: [BillingItemList]!
    public static var waterList: [BillingItemList]!
    public static var otherList: [BillingItemList]!
    
    enum UtilityCategory: String, CaseIterable {
        case ELECTRICITY = "Electricity"
        case GAS = "Gas"
        case WATER = "Water"
    }
    
    private init() {
        
    }
    
    public static func getInstance(){
        if self.billingInstance == nil {
            self.billingInstance = BillingVM()
            self.getInstitutionList()
            self.getInsuranceList()
            self.getUtilityList()
            self.getOtherList()
        }
    }
    
    private static func getInstitutionList() {
        AlamofireWrapper.requestGETURL(HttpURLs.getInstitution, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (response) in
            self.institutionList = Mapper<BillingItemList>().mapArray(JSONString: response) ?? nil
            DLog(self.institutionList)
        }) { (error) in
            DLog(error)
        }
    }
    
    private static func getInsuranceList() {
        AlamofireWrapper.requestGETURL(HttpURLs.getInsurance, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (response) in
            self.insuranceList = Mapper<BillingItemList>().mapArray(JSONString: response) ?? nil
            DLog(self.insuranceList)
        }) { (error) in
            DLog(error)
        }
    }
    
    private static func getUtilityList() {
        AlamofireWrapper.requestGETURL(HttpURLs.getUtility, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (response) in
            self.utilityList = Mapper<BillingListResponse>().mapArray(JSONString: response) ?? nil
            self.utilityList?.forEach {
                if ($0.categoryTitle?.contains(UtilityCategory.ELECTRICITY.rawValue))! {
                    self.electricityList = $0.itemList
                } else if ($0.categoryTitle?.contains(UtilityCategory.GAS.rawValue))! {
                    self.gasList = $0.itemList
                } else if ($0.categoryTitle?.contains(UtilityCategory.WATER.rawValue))! {
                    self.waterList = $0.itemList
                }
            }
            DLog(self.utilityList)
        }) { (error) in
            DLog(error)
        }
    }
    
    private static func getOtherList() {
        AlamofireWrapper.requestGETURL(HttpURLs.getOhers, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (response) in
            self.otherList = Mapper<BillingItemList>().mapArray(JSONString: response) ?? nil
            DLog(self.insuranceList)
        }) { (error) in
            DLog(error)
        }
    }
}
