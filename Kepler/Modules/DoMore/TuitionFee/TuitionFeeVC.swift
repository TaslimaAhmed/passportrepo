//
//  TuitionFeeVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 24/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class TuitionFeeVC: UIViewController {
    
    @IBOutlet weak var instituteTableView: UITableView!
    @IBOutlet weak var instituteNameField: CustomUITextField!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var instituteList: [BillingItemList] = []
    var searchInstituteList: [BillingItemList] = []
    var recentInstituteList: [BillingItemList]!
    var recentTempInstituteList: [BillingItemList]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadRecentInstitute()
        self.setupView()
        self.loadData()
    }
    
    func setupView() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        self.instituteNameField.delegate = self
        
        self.instituteTableView.dataSource = self
        self.instituteTableView.delegate = self
    }
    
    private func loadRecentInstitute() {
        if SessionManager.hasValue(forKey: .RECENT_INSTITUTE) {
            self.recentInstituteList = [BillingItemList] (JSONString: SessionManager.getString(forKey: .RECENT_INSTITUTE)!)!
            self.recentTempInstituteList = self.recentInstituteList
        }
    }
    
    private func loadData() {
        self.instituteList = BillingVM.institutionList
        self.searchInstituteList = self.instituteList
        self.instituteTableView.reloadData()
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        //        if !self.instituteNameField.text!.isEmpty {
        //            let vc = UIStoryboard(storyboard: .tuition_fee).instantiateViewController(withIdentifier: TuitionFeePaymentVC.identifier) as! TuitionFeePaymentVC
        //            vc.institueData = self.institueData
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        } else {
        //            self.showToast(message: .FILL_ALL_FIELDS)
        //        }
    }
}
