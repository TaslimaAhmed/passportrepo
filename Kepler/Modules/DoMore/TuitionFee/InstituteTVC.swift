//
//  InstituteTVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 24/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class InstituteTVC: UITableViewCell {

    @IBOutlet weak var instituteImage: UIImageView!
    @IBOutlet weak var instituteName: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            contentView.backgroundColor = UIColor.keplerLightBackground
        } else {
            contentView.backgroundColor = UIColor.keplerWhite
        }
    }
    
    func loadData(_ instituteData: BillingItemList) {
        if let iconURL = instituteData.iconSource {
            loadImage(imageURL: iconURL, imageView: self.instituteImage)
        } else {
            self.instituteImage.image = UIImage(named: .TUITION_FEES)
        }
        self.instituteName.text = "\(instituteData.description!) (\(instituteData.code!))"
    }
}
