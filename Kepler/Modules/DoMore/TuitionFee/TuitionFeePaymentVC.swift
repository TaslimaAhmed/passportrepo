//
//  TuitionFeePaymentVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 30/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class TuitionFeePaymentVC: UIViewController {
    
    
    @IBOutlet weak var instituteImage: UIImageView!
    @IBOutlet weak var instituteNameLabel: CustomLabel!
    @IBOutlet weak var shortNameLabel: CustomLabel!
    @IBOutlet weak var shortNameTextField: CustomUITextField!
    @IBOutlet weak var shortNameUnderLine: UIView!
    @IBOutlet weak var studentIDLabel: CustomLabel!
    @IBOutlet weak var studentIDTextField: CustomUITextField!
    @IBOutlet weak var studentIDUnderLine: UIView!
    @IBOutlet weak var monthNumberView: UIView!
    
    @IBOutlet weak var monthNumberLabel: CustomLabel!
    @IBOutlet weak var monthNumberDigit: CustomLabel!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var monthNumber: Int = 1
    var instituteData: BillingItemList!
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    func setupView() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.instituteNameLabel.text = "\(self.instituteData.description!) (\(self.instituteData.code!))"
        if let iconURL = self.instituteData.iconSource {
            loadImage(imageURL: iconURL, imageView: self.instituteImage)
        } else {
            self.instituteImage.image = UIImage(named: .TUITION_FEES)
        }
        self.shortNameTextField.delegate = self
        self.studentIDTextField.delegate = self
        self.studentIDTextField.becomeFirstResponder()
        self.enableDisableNextButton()
        
        let lableViewList = instituteData.labelView!.components(separatedBy: "|")
        if instituteData.defaultNoOfMonth!.elementsEqual("false") {
            self.monthNumberView.isHidden = true
        } else {
            self.monthNumberView.isHidden = false
            self.monthNumberLabel.text = lableViewList[2]
        }
        self.shortNameLabel.text = lableViewList[1]
        self.shortNameTextField.placeholder = lableViewList[1]
        
        self.studentIDLabel.text = lableViewList[0]
        self.studentIDTextField.placeholder = lableViewList[0]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.checkTextFields), userInfo: nil, repeats: true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.timer.invalidate()
    }
    
    func enableDisableNextButton(_ shouldEnable: Bool = false) {
        if shouldEnable {
            self.nextButton.isEnabled = true
            self.nextButton.alpha = 1
        } else {
            self.nextButton.isEnabled = false
            self.nextButton.alpha = 0.5
        }
    }
    
    @objc private func checkTextFields() {
        if !self.shortNameTextField.text!.isEmpty && !self.studentIDTextField.text!.isEmpty {
            self.enableDisableNextButton(true)
        } else {
            self.enableDisableNextButton()
        }
    }
    
    @IBAction func minusClicked(_ sender: Any) {
        if monthNumber <= 1 {
            
        } else {
            self.monthNumber -= 1
            self.monthNumberDigit.text = String(self.monthNumber)
        }
    }
    
    @IBAction func plusClicked(_ sender: Any) {
        if monthNumber >= 12 {
            
        } else {
            self.monthNumber += 1
            self.monthNumberDigit.text = String(self.monthNumber)
        }
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if !self.shortNameTextField.text!.isEmpty && !self.studentIDTextField.text!.isEmpty {
            let vc = UIStoryboard(storyboard: .tuition_fee).instantiateViewController(withIdentifier: TuitionFeeConfirmationVC.identifier) as! TuitionFeeConfirmationVC
            vc.institueData = self.instituteData
            vc.studenName = self.shortNameTextField.text
            vc.studenID = self.studentIDTextField.text
            vc.monthNumber = self.monthNumber
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        } else {
            self.showToast(message: .FILL_ALL_FIELDS)
        }
    }
}

extension TuitionFeePaymentVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.shortNameTextField {
            self.shortNameUnderLine.backgroundColor = UIColor.keplerTextUnderLine
            self.studentIDUnderLine.backgroundColor = UIColor.keplerGray
        } else {
            self.shortNameUnderLine.backgroundColor = UIColor.keplerGray
            self.studentIDUnderLine.backgroundColor = UIColor.keplerTextUnderLine
        }
    }
}

extension TuitionFeePaymentVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Loading...".localized)
        
        let params = [
            "regNo" : self.studentIDTextField.text!,
            "numberOfMonth" : String(self.monthNumber),
            "shortName" : self.shortNameTextField.text!,
            "pin" : pin.toBase64(),
            "institutionCode" : self.instituteData.code!
        ] as [String : AnyObject]
        
        TuitionFeeVM.payTuitionBill(params: params) { (success, message) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if success {
                    //For adding recent desco prepaid into local
                    if SessionManager.hasValue(forKey: .RECENT_INSTITUTE) {
                        var recentRecharge: [BillingItemList] = [BillingItemList](JSONString: SessionManager.getString(forKey: .RECENT_INSTITUTE)!)!
                        recentRecharge.append(self.instituteData)
                        if recentRecharge.count > 3 {
                            recentRecharge.removeFirst()
                        }
                        SessionManager.setValue(recentRecharge.toJSONString(), forKey: .RECENT_INSTITUTE)
                    } else {
                        var recentRecharge: [BillingItemList] = []
                        recentRecharge.append(self.instituteData)
                        SessionManager.setValue(recentRecharge.toJSONString(), forKey: .RECENT_INSTITUTE)
                    }
                    //For adding recent mobile recharge into local
                    
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as! SuccessSendVC
                    vc.additionalData = "\(self.instituteData.description!) (\(self.instituteData.code!))"
                    vc.totalAmountString = "Tuition fees has been paid for".localized
                    vc.successDetailsMessage = message
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
