//
//  TuitionFeeVC+Extention.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 24/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension TuitionFeeVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.recentInstituteList != nil {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.recentInstituteList != nil {
            if section == 0 {
                return recentInstituteList.count
            } else {
                return instituteList.count
            }
        } else {
            return instituteList.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.recentInstituteList != nil {
            if section == 0 {
                return "Recent Institutes".localized
            } else {
                return "All Institutes".localized
            }
        } else {
            return "All Institutes".localized
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40)
        let myLabel = UILabel()
        headerView.addSubview(myLabel)
        myLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            myLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 25),
            myLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor)
        ])
        myLabel.font = UIFont(name: FontList.ExtraBold.rawValue, size: 16)
        myLabel.attributedText = NSMutableAttributedString().extraBold(self.tableView(tableView, titleForHeaderInSection: section)!, 16)
        headerView.backgroundColor = UIColor.keplerWhite
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if recentInstituteList != nil {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "InstituteTVC", for: indexPath) as! InstituteTVC
                cell.loadData(self.recentInstituteList[indexPath.row])
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "InstituteTVC", for: indexPath) as! InstituteTVC
                cell.loadData(self.instituteList[indexPath.row])
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InstituteTVC", for: indexPath) as! InstituteTVC
            cell.loadData(self.instituteList[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.instituteNameField.text = self.instituteList[indexPath.row].description
        
        let vc = UIStoryboard(storyboard: .tuition_fee).instantiateViewController(withIdentifier: TuitionFeePaymentVC.identifier) as! TuitionFeePaymentVC
        vc.instituteData = self.instituteList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension TuitionFeeVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            return false
        }
        if textField == self.instituteNameField {
            var searchString = self.instituteNameField.text! + "" + string
            if string == "" {
                searchString.removeLast()
            }
            
            if string.isEmpty && range.location == 0 {
                self.instituteList = self.searchInstituteList
                self.recentInstituteList = self.recentTempInstituteList
                self.instituteTableView.reloadData()
            } else {
                self.recentInstituteList = nil
                self.instituteList = self.searchInstituteList.filter{$0.description!.lowercased().contains(searchString.lowercased())}
                self.instituteTableView.reloadData()
            }
        }
        
        return true
    }
}
