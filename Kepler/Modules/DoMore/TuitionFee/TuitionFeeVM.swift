//
//  TuitionFeeVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 24/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class TuitionFeeVM {
    public static func payTuitionBill(params: [String : AnyObject], complition: @escaping(_ success: Bool, _ message: String) -> Void) {
        SVProgressHUD.show()
        AlamofireWrapper.requestPOSTURL(HttpURLs.tuitionPayment, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            let responseData = ErrorResponse (JSONString: response)
            complition(true, responseData!.message!)
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(false, DefaultMessages.ERROR_TRANSACTION.rawValue.localized)
        }
    }
}
