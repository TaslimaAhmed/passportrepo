//
//  TuitionFeeConfirmationVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 30/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class TuitionFeeConfirmationVC: UIViewController {

    @IBOutlet weak var instituteNameLabel: CustomLabel!
    @IBOutlet weak var studentIDLabel: CustomLabel!
    @IBOutlet weak var studentNameLabel: CustomLabel!
    @IBOutlet weak var confirmLabel: CustomLabel!
    
    @IBOutlet weak var confirmButton: CustomUIButton!
    @IBOutlet weak var closeButton: CustomUIButton!
    
    @IBOutlet weak var pinTexField: UITextField!
    @IBOutlet weak var pinView: UIView!
    @IBOutlet weak var pinViewHeight: NSLayoutConstraint!
    
    var institueData: BillingItemList!
    
    var studenID: String!
    var studenName: String!
    var monthNumber: Int!
    var delegate: OnConfirmClickDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
    }
    
    private func setupView() {
        self.confirmButton.cornerRadius = self.confirmButton.bounds.width / 2
        self.closeButton.cornerRadius = self.closeButton.bounds.width / 2
        
        self.instituteNameLabel.text = "\(self.institueData.description!) (\(self.institueData.code!))"
        self.studentIDLabel.text = self.studenID
        self.studentNameLabel.text = self.studenName
        
        self.pinTexField.delegate = self
        self.pinTexField.defaultTextAttributes.updateValue(24.0, forKey: NSAttributedString.Key.kern)
        self.pinView.isHidden = true
        self.pinViewHeight.constant = 0
    }
    
    @IBAction func confirmClicked(_ sender: Any) {
        if self.pinTexField.text!.isEmpty {
            self.pinView.isHidden = false
            self.pinViewHeight.constant = 80.5
            self.pinTexField.becomeFirstResponder()
            self.confirmLabel.isHidden = true
            self.enableDisableNextButton()
        } else {
            self.delegate.onConfirmClicked(pin: self.pinTexField.text!)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func enableDisableNextButton(_ shouldEnable: Bool = false) {
        if shouldEnable {
            self.confirmButton.isEnabled = true
            self.confirmButton.alpha = 1
        } else {
            self.confirmButton.isEnabled = false
            self.confirmButton.alpha = 0.5
        }
    }
}

extension TuitionFeeConfirmationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var pinLength = self.pinTexField.text!.count
        if textField == self.pinTexField {
            if string != "" {
                pinLength = self.pinTexField.text!.count + 1
            } else {
                pinLength = self.pinTexField.text!.count - 1
            }
        }
        
        if !pinTexField.text!.isEmpty {
            if pinLength >= 4 {
                self.enableDisableNextButton(true)
            } else {
                self.enableDisableNextButton()
            }
        }
        
        return true
    }
}
