//
//  PrepaidAmountBreakdownVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 22/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class PrepaidAmountBreakdownVC: UIViewController {
    
    @IBOutlet weak var descoImage: UIImageView!
    @IBOutlet weak var meterNumberLabel: CustomLabel!
    @IBOutlet weak var totalAmountLabel: CustomLabel!
    @IBOutlet weak var breakdownLabel: CustomLabel!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var availableBalance: Double!
    var breakdownMessage: String!
    var electricityData: BillingItemList!
    var meterNumber: String!
    var notificationNumber: String!
    var isForOther: Bool!
    
    var rechargeAmount: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    private func setupView() {
        loadImage(imageURL: electricityData.iconSource!, imageView: self.descoImage)
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.totalAmountLabel.text = self.rechargeAmount
        self.breakdownLabel.text = self.breakdownMessage
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let amount = (self.rechargeAmount.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
        ChargeVM.getCharge(fromUserNumber: self.notificationNumber, toUserNumber: nil, transactionType: self.electricityData.code!, amount: String(amount)) { (isSuccessful, message) in
            if isSuccessful {
                let totalAmount = (Double(message) ?? 0.0) + amount
                if totalAmount <= self.availableBalance {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ConfirmationVC.identifier) as! ConfirmationVC
                    vc.isForOthers = true
                    vc.recipientName = self.electricityData.description
                    vc.additionalMessage = "Meter number: \(self.meterNumber!)"
                    vc.sendingLabel = "Paying".localized
                    vc.totalAmount = self.rechargeAmount.replacingOccurrences(of: "৳ ", with: "")
                    vc.chargeAmount = message
                    vc.onConfirmClickDelegate = self
                    self.present(vc, animated: true, completion: nil)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = message
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}

extension PrepaidAmountBreakdownVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Loading...".localized)
        
        let params = [
            "meterNumber" : self.meterNumber,
            "notificationNumber" : self.notificationNumber,
            "billAmount" : self.rechargeAmount.replacingOccurrences(of: "৳ ", with: ""),
            "pin" : pin
        ] as [String : AnyObject]
        DLog(params)
        
        PrepaidVM.payDescoPrepaid(params: params) { (message, isSuccess) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if isSuccess {
                    //For adding recent desco prepaid into local
                    self.electricityData.meterNumber = self.meterNumber
                    if SessionManager.hasValue(forKey: .RECENT_ORGANIZATION) {
                        var recentBill: [BillingItemList] = [BillingItemList](JSONString: SessionManager.getString(forKey: .RECENT_ORGANIZATION)!)!
                        if recentBill.filter({ $0.meterNumber == self.electricityData.meterNumber }).count == 0 {
                            recentBill.append(self.electricityData)
                            if recentBill.count > 3 {
                                recentBill.removeFirst()
                            }
                            SessionManager.setValue(recentBill.toJSONString(), forKey: .RECENT_ORGANIZATION)
                        }
                    } else {
                        var recentBill: [BillingItemList] = []
                        recentBill.append(self.electricityData)
                        SessionManager.setValue(recentBill.toJSONString(), forKey: .RECENT_ORGANIZATION)
                    }
                    //For adding recent mobile recharge into local
                    
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as! SuccessSendVC
                    vc.additionalData = self.electricityData.description!
                    vc.totalAmountString = self.rechargeAmount + " has been purchased for".localized
                    vc.successDetailsMessage = message
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
