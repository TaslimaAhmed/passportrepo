//
//  PreviewCardVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 20/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class PreviewCardVC: UIViewController {
    
    @IBOutlet weak var closeButton: CustomUIButton!
    @IBOutlet weak var electricCardImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    private func setupView() {
        self.closeButton.cornerRadius = self.closeButton.bounds.width / 2
        
        loadImage(imageURL: HttpURLs.samplePrepaidCard, imageView: self.electricCardImage)
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
