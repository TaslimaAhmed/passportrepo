//
//  PrepaidElectricityVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 20/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class PrepaidElectricityVC: UIViewController {

    @IBOutlet weak var descoImage: UIImageView!
    @IBOutlet weak var meterNumber: CustomUITextField!
    @IBOutlet weak var accountNumberUnderLine: UIView!
    @IBOutlet weak var notificationNumber: CustomUITextField!
    @IBOutlet weak var contactNumberUnderLine: UIView!
    @IBOutlet weak var sampleStack: UIStackView!
    @IBOutlet weak var remembreCheckBox: CheckBox!
    
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var notificationNumberView: UIView!
    @IBOutlet weak var selfRadioView: UIStackView!
    @IBOutlet weak var selfRadio: CheckBox!
    @IBOutlet weak var otherRadioView: UIStackView!
    @IBOutlet weak var otherRadio: CheckBox!
    
    var electricityData: BillingItemList!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        enableDisableNextButton(button: self.nextButton)
        self.setupView()
    }
    
    private func setupView() {
        loadImage(imageURL: electricityData.iconSource!, imageView: self.descoImage)
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.meterNumber.delegate = self
        self.notificationNumber.delegate = self
        self.meterNumber.becomeFirstResponder()
        self.sampleStack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openPreviewCard)))
        
        self.selfRadio.style = .circle
        self.selfRadio.tintColor = UIColor.keplerDarkGray
        self.selfRadio.borderStyle = .rounded
        self.selfRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkmarkColor = UIColor.keplerDarkGray
        self.selfRadio.useHapticFeedback = true
        
        self.otherRadio.style = .circle
        self.otherRadio.tintColor = UIColor.keplerDarkGray
        self.otherRadio.borderStyle = .rounded
        self.otherRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkmarkColor = UIColor.keplerDarkGray
        self.otherRadio.useHapticFeedback = true
        
        self.selfRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.selfRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.otherRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        self.otherRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
    }
    
    @objc private func onSelfRadioClick() {
        self.selfRadio.isChecked = true
        self.otherRadio.isChecked = false
        self.notificationNumberView.isHidden = true
        if !meterNumber.text!.isEmpty {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    @objc private func onOtherRadioClick() {
        self.selfRadio.isChecked = false
        self.otherRadio.isChecked = true
        self.notificationNumberView.isHidden = false
        if !meterNumber.text!.isEmpty && !notificationNumber.text!.isEmpty {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    
    @objc private func openPreviewCard() {
        let vc = UIStoryboard(storyboard: .desco_prepaid).instantiateViewController(withIdentifier: PreviewCardVC.identifier) as! PreviewCardVC
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func nextClicked(_ sender: Any) {
        let vc = UIStoryboard(storyboard: .desco_prepaid).instantiateViewController(withIdentifier: PrepaidAmountVC.identifier) as! PrepaidAmountVC
        vc.electricityData = self.electricityData
        vc.meterNumber = self.meterNumber.text
        vc.notificationNumber = SessionManager.getString(forKey: .USER_MSISDN)
        if self.otherRadio.isChecked {
            vc.notificationNumber = self.notificationNumber.text
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension PrepaidElectricityVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.meterNumber {
            self.accountNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
            self.contactNumberUnderLine.backgroundColor = UIColor.keplerGray
        } else {
            self.accountNumberUnderLine.backgroundColor = UIColor.keplerGray
            self.contactNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.selfRadio.isChecked {
            if !meterNumber.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        } else {
            if !meterNumber.text!.isEmpty && !notificationNumber.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        }
        return true
    }
}
