//
//  PrepaidAmountVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 20/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class PrepaidAmountVC: UIViewController {
    
    @IBOutlet weak var descoImage: UIImageView!
    @IBOutlet weak var meterNumberLabel: CustomLabel!
    
    @IBOutlet weak var amountField: CustomUITextField!
    @IBOutlet weak var availableAmountFiled: CustomLabel!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    @IBOutlet weak var amount1: CustomUIButton!
    @IBOutlet weak var amount2: CustomUIButton!
    @IBOutlet weak var amount3: CustomUIButton!
    
    var electricityData: BillingItemList!
    var meterNumber: String!
    var notificationNumber: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.loadExistingBalance()
    }
    
    private func setupView() {
        loadImage(imageURL: electricityData.iconSource!, imageView: self.descoImage)
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.amountField.delegate = self
        self.amountField.becomeFirstResponder()
        
        self.meterNumberLabel.text = "Meter number: \(self.meterNumber!)"
        if let availableBalance = SessionManager.getString(forKey: .AVAILABLE_BALANCE) {
            self.availableAmountFiled.text = String(format: "৳ %.2f", (availableBalance as NSString).doubleValue)
        }
    }
    
    private func loadExistingBalance() {
        HomeVM.checkBalance() { (response, message) in
            if response {
                self.availableAmountFiled.text = String(format: "৳ %.2f", (message as NSString).doubleValue)
                SessionManager.setValue(message, forKey: .AVAILABLE_BALANCE)
            } else {
                self.showToast(message: message)
            }
        }
    }
    
    @IBAction func amount1Clicked(_ sender: Any) {
        self.amount1.backgroundColor = UIColor.keplerDarkGreen
        self.amount2.backgroundColor = UIColor.keplerGreen
        self.amount3.backgroundColor = UIColor.keplerGreen
        
        self.amountField.text = "৳ 500"
    }
    @IBAction func amount2Clicked(_ sender: Any) {
        self.amount1.backgroundColor = UIColor.keplerGreen
        self.amount2.backgroundColor = UIColor.keplerDarkGreen
        self.amount3.backgroundColor = UIColor.keplerGreen
        
        self.amountField.text = "৳ 1000"
    }
    @IBAction func amount3Clicked(_ sender: Any) {
        self.amount1.backgroundColor = UIColor.keplerGreen
        self.amount2.backgroundColor = UIColor.keplerGreen
        self.amount3.backgroundColor = UIColor.keplerDarkGreen
        
        self.amountField.text = "৳ 1500"
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let availableBalance = (self.availableAmountFiled.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
        let amount = (self.amountField.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
        
        if !self.amountField.text!.isEmpty {
            if amount < availableBalance {
                PrepaidVM.geBreakDown(meterNumber: self.meterNumber, amount: self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")) { (message, isSuccess) in
                    if isSuccess {
                        let vc = UIStoryboard(storyboard: .desco_prepaid).instantiateViewController(withIdentifier: PrepaidAmountBreakdownVC.identifier) as! PrepaidAmountBreakdownVC
                        vc.electricityData = self.electricityData
                        vc.rechargeAmount = self.amountField.text!
                        vc.meterNumber = self.meterNumber
                        vc.breakdownMessage = message
                        vc.notificationNumber = self.notificationNumber
                        vc.availableBalance = availableBalance
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                        vc.errorMessage = message
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = DefaultMessages.EMPTY_AMOUNT_FIELD.rawValue.localized
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension PrepaidAmountVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !textField.text!.isEmpty && textField.text!.count >= 2 {
            if textField == self.amountField && !self.amountField.text!.isEmpty {
                self.amountField.text = "৳ " + self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")
            }
        } else {
            self.amountField.text = "৳ "
        }
        return true
    }
}
