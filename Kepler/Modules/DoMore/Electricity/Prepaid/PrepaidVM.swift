//
//  PrepaidVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 11/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class PrepaidVM {
    public static func geBreakDown(meterNumber: String, amount: String, complition: @escaping(_ breakDownMessage: String, _ isSuccess: Bool) -> Void) {
        SVProgressHUD.show()
        AlamofireWrapper.requestPOSTURL(HttpURLs.descoPrepaidBreakdown + "?meterNo=\(meterNumber)&amount=\(amount)", params: nil, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            
            let message = DescoPrepaidBreakdownResponse (JSONString: response)
            let breakdown = message?.message!.components(separatedBy: ";")
            var breakdownMessage = ""
            
            breakdown?.forEach {
                breakdownMessage += "\($0)\n\n"
            }
            
            complition(breakdownMessage, true)
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(error, false)
        }
    }
    
    class func payDescoPrepaid(params: [String : AnyObject], complition: @escaping(_ message: String, _ isSuccess: Bool) -> Void) {
        SVProgressHUD.show()
        AlamofireWrapper.requestPOSTURL(HttpURLs.descoPrepaid, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            let message = DescoPrepaidBreakdownResponse (JSONString: response)
            complition(message!.message!, true)
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(error, false)
        }
    }
}
