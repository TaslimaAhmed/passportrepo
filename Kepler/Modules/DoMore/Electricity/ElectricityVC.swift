//
//  ElectricityVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 15/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class ElectricityVC: UIViewController {
    
    @IBOutlet weak var organizationTableView: UITableView!
    @IBOutlet weak var organizationNameField: CustomUITextField!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var electricityOrgList: [BillingItemList]! = []
    var recentElectricityOrgList: [BillingItemList]!
    var recentTempElectricityOrgList: [BillingItemList]!
    var searchElectricityOrgList: [BillingItemList]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadRecentOrganization()
        self.electricityOrgList = BillingVM.electricityList
        self.searchElectricityOrgList = self.electricityOrgList        
        self.setupView()
    }
    
    func setupView() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        self.organizationNameField.delegate = self
        
        self.organizationTableView.dataSource = self
        self.organizationTableView.delegate = self
    }
    
    private func loadRecentOrganization() {
        if SessionManager.hasValue(forKey: .RECENT_ORGANIZATION) {
            self.recentElectricityOrgList = [BillingItemList] (JSONString: SessionManager.getString(forKey: .RECENT_ORGANIZATION)!)!
            self.recentTempElectricityOrgList = self.recentElectricityOrgList
        }
    }
}
