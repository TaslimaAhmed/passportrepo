//
//  ElectricityVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 15/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

class ElectricityVM {
    public func descoPrepaidBillPay(pin: String, meterNumber: String, notificationNumber: String, billAmount: Double) {
        let params = [
            "meterNumber" : meterNumber,
            "notificationNumber" : notificationNumber,
            "billAmount" : billAmount,
            "pin" : pin
            ] as [String : AnyObject]
        AlamofireWrapper.requestPOSTURL(HttpURLs.descoPrepaid, params: params, headers: AlamofireWrapper().defaultHeader, success: { (response) in
            
        }) { (error) in
            DLog(error)
        }
    }
    
    public func descoPostpaidBillPay(pin: String, meterNumber: String, notificationNumber: String, billAmount: Double) {
        let params = [
            "meterNumber" : meterNumber,
            "notificationNumber" : notificationNumber,
            "billAmount" : billAmount,
            "pin" : pin
            ] as [String : AnyObject]
        AlamofireWrapper.requestPOSTURL(HttpURLs.descoPostpaid, params: params, headers: AlamofireWrapper().defaultHeader, success: { (response) in
            
        }) { (error) in
            DLog(error)
        }
    }
}
