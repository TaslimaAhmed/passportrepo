//
//  ElectricityVC+Extention.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 16/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension ElectricityVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.recentElectricityOrgList != nil {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.recentElectricityOrgList != nil {
            if section == 0 {
                return recentElectricityOrgList.count
            } else {
                return electricityOrgList.count
            }
        } else {
            return electricityOrgList.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.recentElectricityOrgList != nil {
            if section == 0 {
                return "Recent Payments".localized
            } else {
                return "All Organizations".localized
            }
        } else {
            return "All Organizations".localized
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40)
        let myLabel = UILabel()
        headerView.addSubview(myLabel)
        myLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            myLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 25),
            myLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor)
        ])
        myLabel.font = UIFont(name: FontList.ExtraBold.rawValue, size: 16)
        myLabel.attributedText = NSMutableAttributedString().extraBold(self.tableView(tableView, titleForHeaderInSection: section)!, 16)
        headerView.backgroundColor = UIColor.keplerWhite
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.recentElectricityOrgList != nil {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ElectricityTVC", for: indexPath) as! ElectricityTVC
                cell.loadData(self.recentElectricityOrgList[indexPath.row])
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ElectricityTVC", for: indexPath) as! ElectricityTVC
                cell.loadData(self.electricityOrgList[indexPath.row])
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ElectricityTVC", for: indexPath) as! ElectricityTVC
            cell.loadData(self.electricityOrgList[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.recentElectricityOrgList == nil {
            if indexPath.row == 0 {
                let vc = UIStoryboard(storyboard: .desco_postpaid).instantiateViewController(withIdentifier: PostpaidElectricityVC.identifier) as! PostpaidElectricityVC
                vc.electricityData = self.electricityOrgList[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row == 1 {
                let vc = UIStoryboard(storyboard: .desco_prepaid).instantiateViewController(withIdentifier: PrepaidElectricityVC.identifier) as! PrepaidElectricityVC
                vc.electricityData = self.electricityOrgList[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = UIStoryboard(storyboard: .dpdc).instantiateViewController(withIdentifier: DpdcVC.identifier) as! DpdcVC
                vc.electricityData = self.electricityOrgList[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if indexPath.section == 0 {
                let vc = UIStoryboard(storyboard: .desco_prepaid).instantiateViewController(withIdentifier: PrepaidElectricityVC.identifier) as! PrepaidElectricityVC
                vc.electricityData = self.electricityOrgList[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                if indexPath.row == 0 {
                    let vc = UIStoryboard(storyboard: .desco_postpaid).instantiateViewController(withIdentifier: PostpaidElectricityVC.identifier) as! PostpaidElectricityVC
                    vc.electricityData = self.electricityOrgList[indexPath.row]
                    self.navigationController?.pushViewController(vc, animated: true)
                } else if indexPath.row == 1 {
                    let vc = UIStoryboard(storyboard: .desco_prepaid).instantiateViewController(withIdentifier: PrepaidElectricityVC.identifier) as! PrepaidElectricityVC
                    vc.electricityData = self.electricityOrgList[indexPath.row]
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .dpdc).instantiateViewController(withIdentifier: DpdcVC.identifier) as! DpdcVC
                    vc.electricityData = self.electricityOrgList[indexPath.row]
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
extension ElectricityVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            return false
        }
        if textField == self.organizationNameField {
            var searchString = self.organizationNameField.text! + "" + string
            if string == "" {
                searchString.removeLast()
            }
            
            if string.isEmpty && range.location == 0 {
                self.electricityOrgList = self.searchElectricityOrgList
                self.recentElectricityOrgList = self.recentTempElectricityOrgList
                self.organizationTableView.reloadData()
            } else {
                self.recentElectricityOrgList = nil
                self.electricityOrgList = self.searchElectricityOrgList.filter{$0.description!.lowercased().contains(searchString.lowercased())}
                self.organizationTableView.reloadData()
            }
        }
        
        return true
    }
}
