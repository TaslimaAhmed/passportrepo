//
//  DpdcVM.swift
//  Kepler
//
//  Created by Taslima Ahmed on 23/2/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class DpdcVM {
    public static var billAmount: DpdcBillResponse!
    public static var currentBalance: String!
    
    class func loadAvailableBalance() {
        AlamofireWrapper.requestPOSTURL(HttpURLs.balanceCheck,params: nil, headers: AlamofireWrapper().defaultHeader) { (response) in
            let ballanceResposen = CheckBlanceResponse(JSONString: response)
            DpdcVM.currentBalance = String(format: "%.2f", (ballanceResposen!.currentBalance! as NSString).doubleValue)
        } failure: { (error) in
            DLog(error)
        }
    }
    
    class func getDpdcBill(params: [String : AnyObject], complition: @escaping(_ success: Bool, _ message: String) -> Void) {
        SVProgressHUD.show()
        AlamofireWrapper.requestPOSTURL(HttpURLs.dpdcBill, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            DpdcVM.billAmount = DpdcBillResponse (JSONString: response)
            if Double(DpdcVM.billAmount.billAmount!) != nil || Int(DpdcVM.billAmount.billAmount!) != nil {
                complition(true, "Success")
            } else {
                complition(false, "Your provided information is not correct".localized)
            }
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(false, error)
        }
    }
    
    class func payDpdcBill(params: [String : AnyObject], complition: @escaping(_ message: String, _ isSuccess: Bool) -> Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.payDpdcBill, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            let message = DpdcPaymentResponse (JSONString: response)
            complition(message!.message!, true)
        } failure: { (error) in
            DLog(error)
            complition(error, false)
        }
    }
}

