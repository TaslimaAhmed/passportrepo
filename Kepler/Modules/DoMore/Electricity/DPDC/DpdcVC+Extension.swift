//
//  DpdcVC+Extension.swift
//  Kepler
//
//  Created by Taslima Ahmed on 4/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

extension DpdcVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.customerNumberTextField {
            self.customerNumberTextFieldUnderLine.backgroundColor = UIColor.keplerTextUnderLine
            self.locationCodeTextFieldUnderLine.backgroundColor = UIColor.keplerGray
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerGray
        } else if textField == locationCodeTextField {
            self.locationCodeTextFieldUnderLine.backgroundColor = UIColor.keplerTextUnderLine
            self.customerNumberTextFieldUnderLine.backgroundColor = UIColor.keplerGray
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerGray
        } else {
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
            self.locationCodeTextFieldUnderLine.backgroundColor = UIColor.keplerGray
            self.customerNumberTextFieldUnderLine.backgroundColor = UIColor.keplerGray
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.billMonthTextField {
            textField.resignFirstResponder()
            self.dropDown.show()
        }
        if self.selfRadio.isChecked {
            if !customerNumberTextField.text!.isEmpty && !locationCodeTextField.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        } else {
            if !customerNumberTextField.text!.isEmpty && !locationCodeTextField.text!.isEmpty && !notificationNumberTextField.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        }
        return true
    }
}
