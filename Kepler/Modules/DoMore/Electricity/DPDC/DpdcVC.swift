//
//  DpdcVC.swift
//  Kepler
//
//  Created by Taslima Ahmed on 4/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit
import DropDown

class DpdcVC: UIViewController {
    
    @IBOutlet weak var dpdcImage: UIImageView!
    @IBOutlet weak var customerNumberTextField: CustomUITextField!
    @IBOutlet weak var customerNumberTextFieldUnderLine: UIView!
    @IBOutlet weak var locationCodeTextField: CustomUITextField!
    @IBOutlet weak var locationCodeTextFieldUnderLine: UIView!
    @IBOutlet weak var billMonthTextField: CustomUITextField!
    @IBOutlet weak var billMonthArrow: UIImageView!
    @IBOutlet weak var notificationNumberTextField: CustomUITextField!
    @IBOutlet weak var notificationNumberUnderLine: UIView!
    @IBOutlet weak var nextButton: CustomUIButton!
    
    @IBOutlet weak var notificationNumberView: UIView!
    @IBOutlet weak var selfRadioView: UIStackView!
    @IBOutlet weak var selfRadio: CheckBox!
    @IBOutlet weak var otherRadioView: UIStackView!
    @IBOutlet weak var otherRadio: CheckBox!
    
    var electricityData: BillingItemList!
    var dpdcMonthYearList: [String] = []
    var dpdcMonthYearValueList: [String] = []
    var selectedBillYearMonth: String = ""
    
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadMonthlyBillData()
        enableDisableNextButton(button: self.nextButton)
        self.setupView()
    }
    
    private func setupView() {
        loadImage(imageURL: electricityData.iconSource!, imageView: self.dpdcImage)
        self.customerNumberTextField.becomeFirstResponder()
        self.customerNumberTextField.delegate = self
        self.notificationNumberTextField.delegate = self
        self.billMonthTextField.delegate = self
        self.locationCodeTextField.delegate = self
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        
        self.selfRadio.style = .circle
        self.selfRadio.tintColor = UIColor.keplerDarkGray
        self.selfRadio.borderStyle = .rounded
        self.selfRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkmarkColor = UIColor.keplerDarkGray
        self.selfRadio.useHapticFeedback = true
        
        self.otherRadio.style = .circle
        self.otherRadio.tintColor = UIColor.keplerDarkGray
        self.otherRadio.borderStyle = .rounded
        self.otherRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkmarkColor = UIColor.keplerDarkGray
        self.otherRadio.useHapticFeedback = true
        
        self.selfRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.selfRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.otherRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        self.otherRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        
        self.billMonthTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showMonthList)))
        self.billMonthArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showMonthList)))
        
        DpdcVM.loadAvailableBalance()
    }
    
    @objc private func showMonthList() {
        dropDown.show()
    }
    @objc private func onSelfRadioClick() {
        self.selfRadio.isChecked = true
        self.otherRadio.isChecked = false
        self.notificationNumberView.isHidden = true
        if !customerNumberTextField.text!.isEmpty || !locationCodeTextField.text!.isEmpty {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    @objc private func onOtherRadioClick() {
        self.selfRadio.isChecked = false
        self.otherRadio.isChecked = true
        self.notificationNumberView.isHidden = false
        if !customerNumberTextField.text!.isEmpty || !locationCodeTextField.text!.isEmpty || !notificationNumberTextField.text!.isEmpty{
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    
    private func loadMonthlyBillData() {
        let currentDate = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMMM yyyy"
        let dateFormatValue = DateFormatter()
        dateFormatValue.dateFormat = "yyyyMM"
        var customMonth = Calendar.current.date(byAdding: .month, value: -1, to: currentDate)!
        var monthString = dateFormat.string(from: customMonth)
        var monthValue = dateFormatValue.string(from: customMonth)
        self.dpdcMonthYearList.append(monthString)
        self.dpdcMonthYearValueList.append(monthValue)
        
        for i in 0 ..< 11 {
            customMonth = Calendar.current.date(byAdding: .month, value: i, to: currentDate)!
            monthString = dateFormat.string(from: customMonth)
            monthValue = dateFormatValue.string(from: customMonth)
            self.dpdcMonthYearList.append(monthString)
            self.dpdcMonthYearValueList.append(monthValue)
        }
        
        dropDown.anchorView = self.billMonthTextField
        dropDown.dataSource = self.dpdcMonthYearList
        dropDown.selectionAction = {  [unowned self] (index: Int, item: String) in
            self.billMonthTextField.text = self.dpdcMonthYearList[index]
            self.selectedBillYearMonth = self.dpdcMonthYearValueList[index]
            dropDown.hide()
        }
        self.billMonthTextField.text = self.dpdcMonthYearList[0]
        self.selectedBillYearMonth = self.dpdcMonthYearValueList[0]
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let params = [
            "accountNumber": self.customerNumberTextField.text!,
            "locationCode" : self.locationCodeTextField.text!,
            "billMonth" : self.selectedBillYearMonth,
        ] as [String : AnyObject]
        
        DpdcVM.getDpdcBill(params: params) { (isSuccess, message) in
            if isSuccess {
                let vc = UIStoryboard(storyboard: .dpdc).instantiateViewController(withIdentifier: DpdcBillPaymentVC.identifier) as! DpdcBillPaymentVC
                vc.customerNumber = self.customerNumberTextField.text
                vc.locationCode = self.locationCodeTextField.text
                vc.billMonth = self.billMonthTextField.text
                vc.billMonthValue = self.selectedBillYearMonth
                vc.notificationNumber = SessionManager.getString(forKey: .USER_MSISDN)
                if self.otherRadio.isChecked {
                    vc.notificationNumber = self.notificationNumberTextField.text!
                }
                vc.electricityData = self.electricityData
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = message
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}
