//
//  DpdcBillPaymentVC.swift
//  Kepler
//
//  Created by Taslima Ahmed on 4/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class DpdcBillPaymentVC: UIViewController {
    
    @IBOutlet weak var descoImage: UIImageView!
    @IBOutlet weak var customerNumberLabel: CustomLabel!
    @IBOutlet weak var locationCodeLabel: CustomLabel!
    @IBOutlet weak var billMonthLabel: CustomLabel!
    @IBOutlet weak var alreadyPaidLabel: CustomLabel!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var balanceViewHeight: NSLayoutConstraint!
    @IBOutlet weak var homeButton: CustomUIButton!
    @IBOutlet weak var nextButton: CustomUIButton!
    @IBOutlet weak var nextButtonView: UIView!
    @IBOutlet weak var nextButtonViewWidth: NSLayoutConstraint!
    @IBOutlet weak var amountLabel: CustomLabel!
    @IBOutlet weak var availableBalanceLabel: CustomLabel!
    
    var customerNumber: String!
    var locationCode: String!
    var billMonth: String!
    var billMonthValue: String!
    var notificationNumber: String!
    var electricityData: BillingItemList!
    var billAmount: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    private func setupView() {
        loadImage(imageURL: electricityData.iconSource!, imageView: self.descoImage)
        self.homeButton.cornerRadius = self.homeButton.bounds.width / 2
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.customerNumberLabel.text = self.customerNumber
        self.billMonthLabel.text = self.billMonth
        self.locationCodeLabel.text = self.locationCode
        
        if let billAmount = DpdcVM.billAmount.billAmount {
            self.billAmount = billAmount
            self.amountLabel.text = "৳ \(String(format: "%.2f", (billAmount as NSString).doubleValue))"
            if (billAmount as NSString).doubleValue < 0 {
                self.balanceView.isHidden = true
                self.balanceViewHeight.constant = 0
                
                self.homeButton.isHidden = false
                self.nextButtonView.isHidden = true
                self.nextButtonViewWidth.constant = 0
            } else {
                self.balanceView.isHidden = false
                self.balanceViewHeight.constant = 80
                self.alreadyPaidLabel.attributedText = NSMutableAttributedString().underlined("Unpaid".localized, 18)
                self.alreadyPaidLabel.textColor = UIColor.keplerRed
                
                self.homeButton.isHidden = true
                self.nextButtonView.isHidden = false
                self.nextButtonViewWidth.constant = 200
            }
        } else {
            self.balanceView.isHidden = true
            self.alreadyPaidLabel.isHidden = false
            self.homeButton.isHidden = false
            enableDisableNextButton(button: self.nextButton, true)
        }
        
        self.availableBalanceLabel.text = "৳ \(DpdcVM.currentBalance!))"
    }
    
    @IBAction func homeButtonClicked(_ sender: Any) {
        initHomeView()
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let availableBalance = (DpdcVM.currentBalance as NSString).doubleValue
        let amount = (self.billAmount as NSString).doubleValue
        if amount <= availableBalance {
            ChargeVM.getCharge(fromUserNumber: self.notificationNumber, toUserNumber: nil, transactionType: self.electricityData.code!, amount: String(amount)) { (isSuccessful, message) in
                if isSuccessful {
                    let totalAmount = (Double(message) ?? 0.0) + amount
                    if totalAmount <= availableBalance {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ConfirmationVC.identifier) as! ConfirmationVC
                        vc.isForOthers = true
                        vc.recipientName = self.electricityData.description
                        vc.additionalMessage = "\("Customer Number".localized): \(self.customerNumber!)\n\("Location Code".localized): \(self.locationCode!)\n\("Bill Month".localized): \(self.billMonth!)"
                        vc.sendingLabel = "Paying".localized
                        vc.totalAmount = String(format: "%.2f", amount)
                        vc.chargeAmount = message
                        vc.onConfirmClickDelegate = self
                        self.present(vc, animated: true, completion: nil)
                    } else {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                        vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension DpdcBillPaymentVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Paying bill...".localized)
        
        let params = [
            "locationCode" : self.locationCode,
            "billMonth" : self.billMonthValue,
            "accountNumber": self.customerNumber,
            "notificationNumber" : self.notificationNumber,
            "pin" : pin
        ] as [String : AnyObject]
        
        DLog(params)
        DpdcVM.payDpdcBill(params: params) { (message, isSuccess) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if isSuccess {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as! SuccessSendVC
                    vc.additionalData = self.electricityData.description!
                    vc.totalAmountString = "" + self.billAmount + " has been paid to".localized
                    vc.successDetailsMessage = message
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
