//
//  ElectricityTVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 16/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class ElectricityTVC: UITableViewCell {
    
    @IBOutlet weak var organizationImage: UIImageView!
    @IBOutlet weak var organizationName: CustomLabel!
    @IBOutlet weak var meterNumber: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            contentView.backgroundColor = UIColor.keplerLightBackground
        } else {
            contentView.backgroundColor = UIColor.keplerWhite
        }
    }

    func loadData(_ electricityData: BillingItemList) {
        loadImage(imageURL: electricityData.iconSource!, imageView: self.organizationImage)
        self.organizationName.text = electricityData.description
        if let meterNumber = electricityData.meterNumber {
            self.meterNumber.text = meterNumber
        } else {
            self.meterNumber.isHidden = true
        }
    }
}
