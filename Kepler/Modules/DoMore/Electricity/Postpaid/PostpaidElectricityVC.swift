//
//  PostpaidElectricityVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 22/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class PostpaidElectricityVC: UIViewController {
    
    @IBOutlet weak var descoImage: UIImageView!
    @IBOutlet weak var billNumberTextField: CustomUITextField!
    @IBOutlet weak var notificationNumberTextField: CustomUITextField!
    @IBOutlet weak var billNumberUnderLine: UIView!
    @IBOutlet weak var notificationNumberUnderLine: UIView!
    @IBOutlet weak var nextButton: CustomUIButton!
    
    @IBOutlet weak var notificationNumberView: UIView!
    @IBOutlet weak var selfRadioView: UIStackView!
    @IBOutlet weak var selfRadio: CheckBox!
    @IBOutlet weak var otherRadioView: UIStackView!
    @IBOutlet weak var otherRadio: CheckBox!
    
    var electricityData: BillingItemList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enableDisableNextButton(button: self.nextButton)
        self.setupView()
    }
    
    private func setupView() {
        loadImage(imageURL: electricityData.iconSource!, imageView: self.descoImage)
        self.billNumberTextField.becomeFirstResponder()
        self.billNumberTextField.delegate = self
        self.notificationNumberTextField.delegate = self
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        
        self.selfRadio.style = .circle
        self.selfRadio.tintColor = UIColor.keplerDarkGray
        self.selfRadio.borderStyle = .rounded
        self.selfRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.selfRadio.checkmarkColor = UIColor.keplerDarkGray
        self.selfRadio.useHapticFeedback = true
        
        self.otherRadio.style = .circle
        self.otherRadio.tintColor = UIColor.keplerDarkGray
        self.otherRadio.borderStyle = .rounded
        self.otherRadio.uncheckedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkedBorderColor = UIColor.keplerDarkGray
        self.otherRadio.checkmarkColor = UIColor.keplerDarkGray
        self.otherRadio.useHapticFeedback = true
        
        self.selfRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.selfRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelfRadioClick)))
        self.otherRadio.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        self.otherRadioView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOtherRadioClick)))
        
        PostpaidVM.loadAvailableBalance()
    }
    
    @objc private func onSelfRadioClick() {
        self.selfRadio.isChecked = true
        self.otherRadio.isChecked = false
        self.notificationNumberView.isHidden = true
        if !billNumberTextField.text!.isEmpty {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    @objc private func onOtherRadioClick() {
        self.selfRadio.isChecked = false
        self.otherRadio.isChecked = true
        self.notificationNumberView.isHidden = false
        if !billNumberTextField.text!.isEmpty && !notificationNumberTextField.text!.isEmpty {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        SVProgressHUD.show()
        PostpaidVM.getPostpaidBill(billNo: self.billNumberTextField.text!) { (isSuccess, message) in
            if isSuccess {
                let vc = UIStoryboard(storyboard: .desco_postpaid).instantiateViewController(withIdentifier: PostpaidBillPaymentVC.identifier) as! PostpaidBillPaymentVC
                vc.billNumber = self.billNumberTextField.text!
                vc.notificationNumber = SessionManager.getString(forKey: .USER_MSISDN)
                if self.otherRadio.isChecked {
                    vc.notificationNumber = self.notificationNumberTextField.text!
                }
                vc.electricityData = self.electricityData
                self.navigationController?.pushViewController(vc, animated: true)
                SVProgressHUD.dismiss()
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = message
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}

extension PostpaidElectricityVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.billNumberTextField {
            self.billNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerGray
        } else {
            self.billNumberUnderLine.backgroundColor = UIColor.keplerGray
            self.notificationNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.selfRadio.isChecked {
            if !billNumberTextField.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        } else {
            if !billNumberTextField.text!.isEmpty && !notificationNumberTextField.text!.isEmpty {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        }
        return true
    }
}
