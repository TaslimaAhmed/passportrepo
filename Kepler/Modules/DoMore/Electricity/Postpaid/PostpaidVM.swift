//
//  PostpaidVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 23/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class PostpaidVM {
    public static var billAmount: DescoPostpaidBillResponse!
    public static var currentBalance: String!
    
    class func getPostpaidBill(billNo: String, complition: @escaping(_ success: Bool, _ message: String) -> Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.descoPostpaidBillGenerate + "?billNo=" + billNo, params: nil, headers: AlamofireWrapper().defaultHeader) { (response) in
            PostpaidVM.billAmount = DescoPostpaidBillResponse (JSONString: response)
            complition(true, "Success")
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(false, error)
        }
    }
    
    class func payDescoPostpaid(params: [String : AnyObject], complition: @escaping(_ message: String, _ isSuccess: Bool) -> Void) {
        SVProgressHUD.show()
        AlamofireWrapper.requestPOSTURL(HttpURLs.descoPostpaid, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            let message = DescoPrepaidBreakdownResponse (JSONString: response)
            complition(message!.message!, true)
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(error, false)
        }
    }
    
    class func loadAvailableBalance() {
        AlamofireWrapper.requestPOSTURL(HttpURLs.balanceCheck,params: nil, headers: AlamofireWrapper().defaultHeader) { (response) in
            let ballanceResposen = CheckBlanceResponse(JSONString: response)
            PostpaidVM.currentBalance = String(format: "%.2f", (ballanceResposen!.currentBalance! as NSString).doubleValue)
        } failure: { (error) in
            DLog(error)
        }
    }
}
