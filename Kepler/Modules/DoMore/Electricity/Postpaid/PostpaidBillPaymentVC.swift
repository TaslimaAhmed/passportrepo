//
//  PostpaidBillPaymentVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 22/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class PostpaidBillPaymentVC: UIViewController {
    
    @IBOutlet weak var descoImage: UIImageView!
    @IBOutlet weak var accountNumberLabel: CustomLabel!
    @IBOutlet weak var alreadyPaidLabel: CustomLabel!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var balanceViewHeight: NSLayoutConstraint!
    @IBOutlet weak var homeButton: CustomUIButton!
    @IBOutlet weak var nextButton: CustomUIButton!
    @IBOutlet weak var nextButtonView: UIView!
    @IBOutlet weak var nextButtonViewWidth: NSLayoutConstraint!
    @IBOutlet weak var amountLabel: CustomLabel!
    @IBOutlet weak var availableBalanceLabel: CustomLabel!
    
    var electricityData: BillingItemList!
    var notificationNumber: String!
    var billNumber: String!
    var billAmount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    private func setupView() {
        loadImage(imageURL: electricityData.iconSource!, imageView: self.descoImage)
        self.homeButton.cornerRadius = self.homeButton.bounds.width / 2
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.accountNumberLabel.text = self.billNumber
        
        if let billAmount = PostpaidVM.billAmount.billAmount {
            self.billAmount = billAmount
            self.amountLabel.text = "৳ \(String(format: "%.2f", (billAmount as NSString).doubleValue))"
            if (billAmount as NSString).doubleValue < 0 {
                self.balanceView.isHidden = true
                self.balanceViewHeight.constant = 0
                
                self.homeButton.isHidden = false
                self.nextButtonView.isHidden = true
                self.nextButtonViewWidth.constant = 0
            } else {
                self.balanceView.isHidden = false
                self.balanceViewHeight.constant = 72
                self.alreadyPaidLabel.attributedText = NSMutableAttributedString().underlined("Unpaid".localized, 14)
                self.alreadyPaidLabel.textColor = UIColor.keplerRed
                
                self.homeButton.isHidden = true
                self.nextButtonView.isHidden = false
                self.nextButtonViewWidth.constant = 200
            }
        } else {
            self.balanceView.isHidden = true
            self.alreadyPaidLabel.isHidden = false
            self.homeButton.isHidden = false
            enableDisableNextButton(button: self.nextButton, true)
        }
        
        self.availableBalanceLabel.text = "৳ \(PostpaidVM.currentBalance!))"
    }
    
    @IBAction func homeButtonClicked(_ sender: Any) {
        initHomeView()
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let availableBalance = (PostpaidVM.currentBalance as NSString).doubleValue
        let amount = (self.billAmount as NSString).doubleValue
        if amount <= availableBalance {
            ChargeVM.getCharge(fromUserNumber: self.notificationNumber, toUserNumber: nil, transactionType: self.electricityData.code!, amount: String(amount)) { (isSuccessful, message) in
                if isSuccessful {
                    let totalAmount = (Double(message) ?? 0.0) + amount
                    if totalAmount <= availableBalance {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ConfirmationVC.identifier) as! ConfirmationVC
                        vc.isForOthers = true
                        vc.recipientName = self.electricityData.description
                        vc.additionalMessage = "Meter number: \(self.billNumber!)"
                        vc.sendingLabel = "Paying".localized
                        vc.totalAmount = String(amount)
                        vc.chargeAmount = message
                        vc.onConfirmClickDelegate = self
                        self.present(vc, animated: true, completion: nil)
                    } else {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                        vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension PostpaidBillPaymentVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Loading...".localized)
        
        let params = [
            "billNumber" : self.billNumber,
            "notificationNumber" : self.notificationNumber,
            "billAmount" : self.billAmount,
            "pin" : pin
        ] as [String : AnyObject]
        DLog(params)
        PostpaidVM.payDescoPostpaid(params: params) { (message, isSuccess) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if isSuccess {
                    //For adding recent desco prepaid into local
                    if SessionManager.hasValue(forKey: .RECENT_ORGANIZATION) {
                        var recentBill: [BillingItemList] = [BillingItemList](JSONString: SessionManager.getString(forKey: .RECENT_ORGANIZATION)!)!
                        if recentBill.filter({ $0.code == self.electricityData.code }).count == 0 {
                            recentBill.append(self.electricityData)
                            if recentBill.count > 3 {
                                recentBill.removeFirst()
                            }
                            SessionManager.setValue(recentBill.toJSONString(), forKey: .RECENT_ORGANIZATION)
                        }
                    } else {
                        var recentBill: [BillingItemList] = []
                        recentBill.append(self.electricityData)
                        SessionManager.setValue(recentBill.toJSONString(), forKey: .RECENT_ORGANIZATION)
                    }
                    //For adding recent mobile recharge into local
                    
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as! SuccessSendVC
                    vc.additionalData = self.electricityData.description!
                    vc.totalAmountString = self.billAmount + " has been paid to".localized
                    vc.successDetailsMessage = message
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
