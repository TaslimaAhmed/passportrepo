//
//  DoMoreVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 5/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension DoMoreVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return doMoreMenu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DoMoreMenuCollectionViewCell", for: indexPath) as! DoMoreMenuCollectionViewCell
        cell.setupCell(menu: doMoreMenu[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if BillingVM.insuranceList != nil {
                let vc = UIStoryboard(storyboard: .insurance).instantiateViewController(withIdentifier: InsuranceVC.identifier) as! InsuranceVC
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.showToast(message: "Data loading please wait...")
            }
        } else if indexPath.row == 1 {
            if BillingVM.utilityList != nil {
                let vc = UIStoryboard(storyboard: .doMore).instantiateViewController(withIdentifier: BillPaymentVC.identifier) as! BillPaymentVC
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.showToast(message: "Data loading please wait...")
            }
        } else if indexPath.row == 2 {
            if BillingVM.institutionList != nil {
                let vc = UIStoryboard(storyboard: .tuition_fee).instantiateViewController(withIdentifier: TuitionFeeVC.identifier) as! TuitionFeeVC
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.showToast(message: "Data loading please wait...")
            }
        } else if indexPath.row == 3 {
            if BillingVM.otherList != nil {
                let vc = UIStoryboard(storyboard: .doMore).instantiateViewController(withIdentifier: BillPaymentVC.identifier) as! BillPaymentVC
                vc.isForUtility = false
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.showToast(message: "Data loading please wait...")
            }
        } else if indexPath.row == 4 {
            if BillingVM.utilityList != nil {
                let vc = UIStoryboard(storyboard: .fund_transfer).instantiateViewController(withIdentifier: FundTransferVC.identifier) as! FundTransferVC
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.showToast(message: "Data loading please wait...")
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.bounds.width / 3) - 15
        return CGSize(width: size, height: size )
    }
}
