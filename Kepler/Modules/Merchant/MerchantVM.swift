//
//  MerchantVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 22/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class MerchantVM {
    class func checkMerchant(msisdn: String, complition: @escaping(_ isMerchant: Bool, _ userName: String) -> Void) {
        SVProgressHUD.show()
        AlamofireWrapper.requestGETURL(HttpURLs.checkUserType + "?userNumber=\(msisdn)", params: nil, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            let merchatCheck = CheckMerchantResponse (JSONString: response)
            if merchatCheck!.userType!.lowercased().elementsEqual("merchant") {
                complition(true, merchatCheck!.userName!)
            } else {
                complition(false, merchatCheck!.userName!)
            }
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(false, error)
        }
    }
    
    class func payMerchant(params: [String : AnyObject], complition: @escaping(_ isSuccess: Bool, _ message: String) -> Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.merchantPayment, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            let response = ErrorResponse (JSONString: response)
            complition(true, response!.message!)
        } failure: { (error) in
            DLog(error)
            complition(false, error)
        }
    }
}
