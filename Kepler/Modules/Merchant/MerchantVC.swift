//
//  MerchantVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 21/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class MerchantVC: UIViewController {
    
    @IBOutlet weak var merchantNameField: CustomUITextField!
    @IBOutlet weak var merchantTableView: UITableView!
    @IBOutlet weak var noMerchantLabel: CustomLabel!
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var searchedMerchantList: [ContactsItem] = []
    var merchantList: [ContactsItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadRecentMerchant()
        self.setupview()    
    }
    
    private func loadRecentMerchant() {
        if SessionManager.hasValue(forKey: .RECENT_MERCHANT) {
            self.merchantList = [ContactsItem] (JSONString: SessionManager.getString(forKey: .RECENT_MERCHANT)!)!
            self.searchedMerchantList = self.merchantList
        }
    }
    
    private func setupview() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.merchantTableView.dataSource = self
        self.merchantTableView.delegate = self
        self.merchantNameField.delegate = self
    }
    
    public func gotoToMerchantPaymentPage() {
        if !self.merchantNameField.text!.isEmpty && self.merchantNameField.text!.count == 11 && self.merchantNameField.text!.isValidPhoneNumber {
            MerchantVM.checkMerchant(msisdn: self.merchantNameField.text!) { (isSuccess, userName) in
                if isSuccess {
                    let vc = UIStoryboard(storyboard: .merchant).instantiateViewController(withIdentifier: MerchantPaymentVC.identifier) as! MerchantPaymentVC
                    vc.name = userName
                    vc.number = self.merchantNameField.text
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: MerchantNotRegisteredVC.identifier) as! MerchantNotRegisteredVC
                    self.present(vc, animated: true, completion: nil)
                }
            }
        } else {
            self.showToast(message: .INVALID_PHONE_NUMBER)
            self.merchantNameField.text = nil
        }
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        self.gotoToMerchantPaymentPage()
        self.merchantNameField.resignFirstResponder()
    }
}
