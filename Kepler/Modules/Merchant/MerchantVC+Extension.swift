//
//  MerchantVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 21/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension MerchantVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.merchantList.count == 0 {
            self.noMerchantLabel.isHidden = false
            self.merchantTableView.isHidden = true
            return 0
        } else {
            self.noMerchantLabel.isHidden = true
            self.merchantTableView.isHidden = false
            return self.merchantList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MerchantTVC", for: indexPath) as! MerchantTVC
        cell.loadData(data: self.merchantList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.merchantNameField.text = self.merchantList[indexPath.row].number
        self.gotoToMerchantPaymentPage()
    }
}

extension MerchantVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var searchString = ""
        if string == "\n" {
            textField.resignFirstResponder()
            return false
        }
        if textField == self.merchantNameField {
            searchString = self.merchantNameField.text! + "" + string
            if string == "" {
                searchString.removeLast()
            }
            if let num = Int(searchString) {
                self.merchantList = self.searchedMerchantList.filter{$0.number.lowercased().contains(String(num).lowercased())}
                self.merchantTableView.reloadData()
            } else {
                self.merchantList = self.searchedMerchantList.filter{$0.name.lowercased().contains(searchString.lowercased())}
                self.merchantTableView.reloadData()
            }
            
            if string.isEmpty && range.location == 0 {
                self.merchantList = self.searchedMerchantList
                self.merchantTableView.reloadData()
            }
        }
        
        return true
    }
}
