//
//  MerchantTVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 21/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class MerchantTVC: UITableViewCell {
    @IBOutlet weak var merchantName: CustomLabel!
    @IBOutlet weak var merchantNumber: CustomLabel!
    @IBOutlet weak var nameLabel: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func loadData(data: ContactsItem) {
        if let name = data.name {
            self.merchantName.text = name
            let shortName = self.merchantName.text!.components(separatedBy: " ")
            if !self.merchantName.text!.isEmpty {
                if shortName.count >= 2 {
                    self.nameLabel.text = "\(shortName[0].first!) \(shortName[1].first!)"
                } else if name.count == 1 {
                    self.nameLabel.text = "\(shortName[0].first!) \(shortName[0].first!)"
                }
            }
        } else {
            self.merchantName.text = ""
        }
        if let number = data.number {
            self.merchantNumber.text = number
        } else {
            self.merchantNumber.text = ""
        }
    }
}
