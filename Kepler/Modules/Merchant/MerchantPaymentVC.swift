//
//  MerchantPaymentVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 22/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class MerchantPaymentVC: UIViewController {
    
    @IBOutlet weak var nameLabel: CustomLabel!
    @IBOutlet weak var merchantNameLabel: CustomLabel!
    @IBOutlet weak var merchantNumberLabel: CustomLabel!
    
    @IBOutlet weak var amountTextField: CustomUITextField!
    @IBOutlet weak var counterTextField: CustomUITextField!
    @IBOutlet weak var availableBalanceLabel: CustomLabel!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var name: String!
    var number: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    func setupView() {
        self.merchantNameLabel.text = self.name
        self.merchantNumberLabel.text = self.number
        let shortName = self.merchantNameLabel.text!.split(separator: " ")
        if shortName.count >= 2 {
            self.nameLabel.text = String(shortName[0].first!) + " " + String(shortName[1].first!)
        } else if name.count == 1 {
            self.nameLabel.text = String(shortName[0].first!) + " " + String(shortName[1].first!)
        }
        
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.amountTextField.delegate = self
        self.counterTextField.delegate = self
        self.amountTextField.becomeFirstResponder()
        enableDisableNextButton(button: self.nextButton)
        
        self.loadExistingBalance()
    }
    
    private func loadExistingBalance() {
        HomeVM.checkBalance() { (response, message) in
            if response {
                self.availableBalanceLabel.text = String(format: "৳ %.2f", (message as NSString).doubleValue)
                SessionManager.setValue(message, forKey: .AVAILABLE_BALANCE)
            } else {
                self.showToast(message: message)
            }
        }
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        let availableBalance = (self.availableBalanceLabel.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
        let transactionType = TransactionHistoryVM.transactionTypeList.filter { $0.featureName == "MERCHANT_PAYMENT" }.first
        let amount = (self.amountTextField.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
        if amount <= availableBalance {
            if amount >= Double(transactionType!.minAmount!) {
                if amount <= Double(transactionType!.maxAmount!) || transactionType!.maxAmount! == -1 {
                    ChargeVM.getCharge(fromUserNumber: SessionManager.getString(forKey: .USER_MSISDN)!, toUserNumber: self.number, transactionType: transactionType!.code!, amount: String(amount)) { (isSuccessful, message) in
                        if isSuccessful {
                            let totalAmount = (Double(message) ?? 0.0) + amount
                            if totalAmount <= availableBalance {
                                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ConfirmationVC.identifier) as! ConfirmationVC
                                vc.isForOthers = true
                                vc.recipientName = "Merchant (\(self.number!))"
                                vc.sendingLabel = "Paying".localized
                                vc.totalAmount = String(amount)
                                vc.chargeAmount = message
                                vc.onConfirmClickDelegate = self
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                                vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                                self.present(vc, animated: true, completion: nil)
                            }
                        } else {
                            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                            vc.errorMessage = message
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = "Amount should be less than ".localized + "\(transactionType!.maxAmount!)" + " taka".localized
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = "Amount should be more than ".localized + "\(transactionType!.minAmount!)" + " taka".localized
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension MerchantPaymentVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            return false
        }
        if textField == self.amountTextField {
            if (string == "" && (textField.text! as NSString).replacingCharacters(in: range, with: string).count <= 2 ) || self.counterTextField.text!.count < 1 {
                self.amountTextField.text = "৳ "
                enableDisableNextButton(button: self.nextButton)
                return false
            } else {
                self.amountTextField.text = "৳ " + self.amountTextField.text!.replacingOccurrences(of: "৳ ", with: "")
                enableDisableNextButton(button: self.nextButton, true)
            }
        }
        
        if textField == self.counterTextField {
            if (string == "" && (textField.text! as NSString).replacingCharacters(in: range, with: string).count == 0) || self.amountTextField.text!.count < 2 {
                enableDisableNextButton(button: self.nextButton)
            } else {
                enableDisableNextButton(button: self.nextButton, true)
            }
        }
        return true
    }
}

extension MerchantPaymentVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Cashing out...".localized)
        
        let parmas = [
            "recipientNumber" : self.number,
            "amount" : self.amountTextField.text!.replacingOccurrences(of: "৳ ", with: ""),
            "notificationNumber" : SessionManager.getString(forKey: .USER_MSISDN),
            "purpose" : self.counterTextField.text!,
            "pin" : pin
        ] as [String : AnyObject]
        
        MerchantVM.payMerchant(params: parmas) { (isSuccess, message) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if isSuccess {
                    //For adding recent desco prepaid into local
                    var marchentData: ContactsItem!
                    if let name = self.name {
                        marchentData = ContactsItem(name: name, number: self.number)
                    } else {
                        marchentData = ContactsItem(name: "", number: self.number)
                    }
                    
                    if SessionManager.hasValue(forKey: .RECENT_MERCHANT) {
                        var recentMerchant: [ContactsItem] = [ContactsItem](JSONString: SessionManager.getString(forKey: .RECENT_MERCHANT)!)!
                        if recentMerchant.filter({ $0.number == marchentData.number }).count == 0 {
                            recentMerchant.append(marchentData)
                            SessionManager.setValue(recentMerchant.toJSONString(), forKey: .RECENT_MERCHANT)
                        }
                    } else {
                        var recentMerchant: [ContactsItem] = []
                        recentMerchant.append(marchentData)
                        SessionManager.setValue(recentMerchant.toJSONString(), forKey: .RECENT_MERCHANT)
                    }
                    //For adding recent mobile recharge into local
                    
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as! SuccessSendVC
                    vc.additionalData = "Merchant (\(self.number!))"
                    vc.totalAmountString = self.amountTextField.text! + " has been paid to".localized
                    vc.successDetailsMessage = message
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
