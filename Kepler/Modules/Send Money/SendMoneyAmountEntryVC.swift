//
//  SendMoneyAmountEntryVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 15/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class SendMoneyAmountEntryVC: UIViewController {
    
    @IBOutlet weak var shortName: CustomLabel!
    @IBOutlet weak var senderName: CustomLabel!
    @IBOutlet weak var phoneNumber: CustomLabel!
    @IBOutlet weak var amountField: CustomUITextField!
    @IBOutlet weak var availableAmountFiled: CustomLabel!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var name: String?
    var msisdn: String!
    
    var sendMoneyVM = SendMoneyVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.loadExistingBalance()
    }
    
    private func setupView() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        
        self.amountField.delegate = self
        self.amountField.becomeFirstResponder()
        if name == nil {
            let name = self.senderName.text!.split(separator: " ")
            if !self.senderName.text!.isEmpty {
                if name.count >= 2 {
                    self.shortName.text = String(name[0].first!) + "" + String(name[1].first!)
                } else {
                    self.shortName.text = String(name[0].first!) + "" + String(name[0].first!)
                }
            }
            self.senderName.text = msisdn
            self.phoneNumber.text = msisdn
        } else {
            self.senderName.text = name
            self.phoneNumber.text = msisdn
        }
    }
    
    private func loadExistingBalance() {
        HomeVM.checkBalance() { (response, message) in
            if response {
                self.availableAmountFiled.text = String(format: "৳ %.2f", (message as NSString).doubleValue)
                SessionManager.setValue(message, forKey: .AVAILABLE_BALANCE)
            } else {
                self.showToast(message: message)
            }
        }
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if !amountField.text!.isEmpty {
            let availableBalance = (self.availableAmountFiled.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
            let transactionType = TransactionHistoryVM.transactionTypeList.filter { $0.featureName == "P2P" }.first
            let amount = (self.amountField.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).doubleValue
            if amount >= Double(transactionType!.minAmount!) {
                if amount <= Double(transactionType!.maxAmount!) {
                    if amount <= availableBalance {
                        ChargeVM.getCharge(fromUserNumber: SessionManager.getString(forKey: .USER_MSISDN)!, toUserNumber: self.msisdn, transactionType: transactionType!.code!, amount: String(amount)) { (isSuccessful, message) in
                            if isSuccessful {
                                let totalAmount = (Double(message) ?? 0.0) + amount
                                if totalAmount <= availableBalance {
                                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ConfirmationVC.identifier) as! ConfirmationVC
                                    vc.recipientName = self.name
                                    vc.recipientNumber = self.msisdn
                                    vc.chargeAmount = message
                                    vc.totalAmount = String(amount)
                                    vc.onConfirmClickDelegate = self
                                    self.present(vc, animated: true, completion: nil)
                                } else {
                                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                                    vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                                    self.present(vc, animated: true, completion: nil)
                                }
                            } else {
                                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                                vc.errorMessage = message
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                    } else {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                        vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = "Amount should be less than ".localized + "\(transactionType!.maxAmount!)" + " taka".localized
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = "Amount should be more than ".localized + "\(transactionType!.minAmount!)" + " taka".localized
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = DefaultMessages.EMPTY_AMOUNT_FIELD.rawValue.localized
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension SendMoneyAmountEntryVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !textField.text!.isEmpty && textField.text!.count >= 2 {
            if textField == self.amountField && !self.amountField.text!.isEmpty {
                self.amountField.text = "৳ " + self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")
            }
        } else {
            self.amountField.text = "৳ "
        }
        return true
    }
}

extension SendMoneyAmountEntryVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Sending money...".localized)
        let totalAmount = self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")
        sendMoneyVM.sendMoney(pin: pin, sendMoneyNumber: self.msisdn, amount: totalAmount) { (response, message) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if response {
                    //For adding recent send money into local
                    let contactData = ContactsItem(name: self.name ?? "", number: self.msisdn)
                    if SessionManager.hasValue(forKey: .RECENT_SEND_MONEY) {
                        var recentSendMoney: [ContactsItem] = [ContactsItem] (JSONString: SessionManager.getString(forKey: .RECENT_SEND_MONEY)!)!
                        if recentSendMoney.filter({ $0.number == contactData.number }).count == 0 {
                            recentSendMoney.append(contactData)
                            if recentSendMoney.count > 3 {
                                recentSendMoney.removeFirst()
                            }
                            SessionManager.setValue(recentSendMoney.toJSONString(), forKey: .RECENT_SEND_MONEY)
                        }
                    } else {
                        var recentSendMoney: [ContactsItem] = []
                        recentSendMoney.append(contactData)
                        SessionManager.setValue(recentSendMoney.toJSONString(), forKey: .RECENT_SEND_MONEY)
                    }
                    //For adding recent send money into local
                    
                    NotificationHelper.showNotification(message: message)
                    if let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as? SuccessSendVC {
                        vc.name = self.name
                        vc.number = self.msisdn!
                        vc.totalAmountString = self.amountField.text! + " has been sent to".localized
                        vc.successDetailsMessage = message
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
