//
//  SendMoneyVC.swift
//  Kepler
//
//  Created by Mufad Monwar on 11/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import Contacts

class SendMoneyVC: UIViewController {
    
    var contacts: [ContactsItem] = []
    var searchedContacts: [ContactsItem] = []
    var recentContacts: [ContactsItem]!
    var recentTempContacts: [ContactsItem]!
    
    @IBOutlet weak var allContactList: UITableView!
    @IBOutlet weak var phoneNumberField: CustomUITextField!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var senderName: String!
    var hasRecentData: Bool = false
    var msisdn: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadRecentSendMoney()
        self.setupView()
        self.loadContacts()
    }
    
    func setupView() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        self.phoneNumberField.delegate = self
        
        self.allContactList.delegate = self
        self.allContactList.dataSource = self
    }
    
    private func loadContacts() {
        self.contacts = ContactHelper.contactData
        self.searchedContacts = self.contacts
        self.allContactList.reloadData()
        
        if self.contacts.count == 0 {
            emptyMessageInTableView(self.allContactList, "No contacts found".localized)
        } else {
            emptyMessageInTableView(self.allContactList, "")
        }
    }
    
    private func loadRecentSendMoney() {
        if SessionManager.hasValue(forKey: .RECENT_SEND_MONEY) {
            self.recentContacts = [ContactsItem] (JSONString: SessionManager.getString(forKey: .RECENT_SEND_MONEY)!)!
            self.recentTempContacts = self.recentContacts
        }
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        checkUserAvailability()
        self.phoneNumberField.resignFirstResponder()
    }
    
    func checkUserAvailability() {
        self.msisdn = self.phoneNumberField.text!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: "88", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ".", with: "")
        if !self.msisdn.isEmpty && self.msisdn.count == 11 && self.msisdn.isValidPhoneNumber {
            LoginRegistrationVM().checkUser(msisdn: self.msisdn) { (isSuccess, message) in
                if isSuccess {
                    let vc = UIStoryboard(storyboard: .send_money).instantiateViewController(withIdentifier: SendMoneyAmountEntryVC.identifier) as! SendMoneyAmountEntryVC
                    vc.msisdn = self.msisdn
                    vc.name = self.senderName
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    if message == "User not registered." {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: UserNotRegisteredErrorVC.identifier) as! UserNotRegisteredErrorVC
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        } else {
            self.showToast(message: .INVALID_PHONE_NUMBER)
            self.senderName = nil
        }
    }
}
