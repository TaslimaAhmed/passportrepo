//
//  SendMoneyVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 16/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

class SendMoneyVM {
    func sendMoney(pin: String, sendMoneyNumber: String, amount: String, response: @escaping(_ success: Bool, _ message: String) -> Void) {
        let params = [
            "recipientNumber": sendMoneyNumber,
            "amount": amount,
            "pin": pin
            ] as [String : AnyObject]
        AlamofireWrapper.requestPOSTURL(HttpURLs.sendMoney, params: params, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            let sendMoneyResponse = SendMoneyResponse (JSONString: responseData)
            if let message = sendMoneyResponse?.message, message != "" {
                response(true, message)
            } else {
                response(false, DefaultMessages.ERROR_MESSAGE.rawValue.localized)
            }
        }) { (error) in
            DLog(error)
            response(false, error)
        }
    }
}
