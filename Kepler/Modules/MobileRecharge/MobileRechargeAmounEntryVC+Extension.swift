//
//  MobileRechargeAmounEntryVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 9/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension MobileRechargeAmounEntryVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !textField.text!.isEmpty && textField.text!.count >= 2 {
            if textField == self.amountField && !self.amountField.text!.isEmpty {
                self.amountField.text = "৳ " + self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")
            }
        } else {
            self.amountField.text = "৳ "
        }
        return true
    }
}

extension MobileRechargeAmounEntryVC: OnConfirmClickDelegate {
    func onConfirmClicked(pin: String) {
        let loadinVC: LoadingVC = showLoadingView(navigationController: self.navigationController!, loadingText: "Recharging...".localized)
        
        let totalAmount = self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")
        mobileRechargeVM.mobileRecharge(pin: pin, recipientNumber: self.msisdn, amount: totalAmount, connectionType: self.connectionType, operatorType: self.operatorType) { (response, message) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                loadinVC.dismisViewController()
                if response {
                    //For adding recent mobile recharge into local
                    let contactData = ContactsItem(name: self.name ?? "", number: self.msisdn)
                    if SessionManager.hasValue(forKey: .RECENT_MOBILE_RECHARGE) {
                        var recentRecharge: [ContactsItem] = [ContactsItem](JSONString: SessionManager.getString(forKey: .RECENT_MOBILE_RECHARGE)!)!
                        if recentRecharge.filter({ $0.number == contactData.number }).count == 0 {
                            recentRecharge.append(contactData)
                            if recentRecharge.count > 3 {
                                recentRecharge.removeFirst()
                            }
                            SessionManager.setValue(recentRecharge.toJSONString(), forKey: .RECENT_MOBILE_RECHARGE)
                        }
                    } else {
                        var recentRecharge: [ContactsItem] = []
                        recentRecharge.append(contactData)
                        SessionManager.setValue(recentRecharge.toJSONString(), forKey: .RECENT_MOBILE_RECHARGE)
                    }
                    //For adding recent mobile recharge into local
                    
                    NotificationHelper.showNotification(message: message)
                    if let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: SuccessSendVC.identifier) as? SuccessSendVC {
                        vc.name = self.name
                        vc.number = self.msisdn!
                        vc.totalAmountString = self.amountField.text! + " has been sent to".localized
                        vc.successDetailsMessage = message
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}

extension MobileRechargeAmounEntryVC: MobileOperatorSelectionDelegeate {
    func onOperatorSelect(operatorName: String, operatorImage: UIImage?) {
        self.operatorName.text = operatorName
        self.operatorType = operatorName
    }
}
