//
//  MobileRechargeAmounEntryVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 19/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class MobileRechargeAmounEntryVC: UIViewController {
    
    @IBOutlet weak var shortName: CustomLabel!
    @IBOutlet weak var senderName: CustomLabel!
    @IBOutlet weak var phoneNumber: CustomLabel!
    @IBOutlet weak var amountField: CustomUITextField!
    @IBOutlet weak var availableAmountFiled: CustomLabel!
    @IBOutlet weak var connectionTypeLable: CustomLabel!
    @IBOutlet weak var connectionTypeView: UIView!
    
    @IBOutlet weak var operatorName: CustomLabel!
    @IBOutlet weak var operatorSelectionView: UIView!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    @IBOutlet weak var amount1: CustomUIButton!
    @IBOutlet weak var amount2: CustomUIButton!
    @IBOutlet weak var amount3: CustomUIButton!
    
    var name: String?
    var msisdn: String!
    var operatorType: String!
    var connectionType: String = "Prepaid"
    
    var mobileRechargeVM = MobileRechargeVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.loadExistingBalance()
    }
    
    private func setupView() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.amountField.delegate = self
        self.amountField.becomeFirstResponder()
        if name == nil {
            self.senderName.text = msisdn
            self.phoneNumber.text = msisdn
        } else {
            self.senderName.text = name
            self.phoneNumber.text = msisdn
        }
        
        let phnNumber = msisdn!
        let tempOperatorName = getMobileOperator(phoneNumber: phnNumber)
        operatorName.text = tempOperatorName
        operatorType = tempOperatorName
        
        self.operatorSelectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openOperatorSelection)))
        self.connectionTypeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.connectionTypeClicked)))
    }
    
    @objc private func openOperatorSelection() {
        if let viewController = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: MobileOperatorVC.identifier) as? MobileOperatorVC {
            viewController.mobileOperatorDelegate = self
            self.navigationController?.present(viewController, animated: true)
        }
    }
    
    @objc private func connectionTypeClicked() {
        let alertVC = UIAlertController(title: "Select Connection Type".localized, message: "", preferredStyle: .actionSheet)
        let prepaidAction = UIAlertAction(title: "Prepaid".localized, style: .default) { (action) in
            self.connectionTypeLable.text = "Prepaid".localized
            self.connectionType = "Prepaid"
        }
        
        let postpaidAction = UIAlertAction(title: "Postpaid".localized, style: .default) { (action) in
            self.connectionTypeLable.text = "Postpaid".localized
            self.connectionType = "Postpaid"
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel) { (action) in
        }
        
        alertVC.addAction(prepaidAction)
        alertVC.addAction(postpaidAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    private func loadExistingBalance() {
        HomeVM.checkBalance() { (response, message) in
            if response {
                self.availableAmountFiled.text = String(format: "৳ %.2f", (message as NSString).doubleValue)
                SessionManager.setValue(message, forKey: .AVAILABLE_BALANCE)
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = message
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if !amountField.text!.isEmpty {
            let amount = Int(amountField.text!.replacingOccurrences(of: "৳ ", with: ""))
            var transactionType: FeaturesConfigList!
            if self.connectionType.elementsEqual("Prepaid") {
                transactionType = TransactionHistoryVM.transactionTypeList.filter { $0.featureName == "TOP_UP_PREPAID" }.first
            } else {
                transactionType = TransactionHistoryVM.transactionTypeList.filter { $0.featureName == "TOP_UP_POSTPAID" }.first
            }
            
            if amount ?? 0 >= transactionType.minAmount! {
                if amount ?? 0 <= transactionType.maxAmount! {
                    if amount ?? 0 <= (self.availableAmountFiled.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).integerValue {
                        let amount = self.amountField.text!.replacingOccurrences(of: "৳ ", with: "")
                        /*ChargeVM.getCharge(fromUserNumber: SessionManager.getString(forKey: .USER_MSISDN)!, toUserNumber: self.msisdn, transactionType: transactionType!.code!, amount: amount) { (isSuccessful, message) in
                         if isSuccessful {*/
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ConfirmationVC.identifier) as! ConfirmationVC
                        
                        vc.recipientName = self.name
                        vc.recipientNumber = self.msisdn
                        //vc.chargeAmount = message
                        vc.totalAmount = amount
                        vc.onConfirmClickDelegate = self
                        self.present(vc, animated: true, completion: nil)
                        /*} else {
                         self.showToast(message: message)
                         }
                         }*/
                    } else {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                        vc.errorMessage = DefaultMessages.INSUFFICIENT_BALANCE.rawValue.localized.localized
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = "Amount should be less than ".localized + "\(transactionType!.maxAmount!)" + " taka".localized
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = "Amount should be more than ".localized + "\(transactionType!.minAmount!)" + " taka".localized
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = DefaultMessages.EMPTY_AMOUNT_FIELD.rawValue.localized
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func amount1Clicked(_ sender: Any) {
        self.amount1.backgroundColor = UIColor.keplerDarkGreen
        self.amount2.backgroundColor = UIColor.keplerGreen
        self.amount3.backgroundColor = UIColor.keplerGreen
        
        self.amountField.text = "৳ 20"
    }
    @IBAction func amount2Clicked(_ sender: Any) {
        self.amount1.backgroundColor = UIColor.keplerGreen
        self.amount2.backgroundColor = UIColor.keplerDarkGreen
        self.amount3.backgroundColor = UIColor.keplerGreen
        
        self.amountField.text = "৳ 50"
    }
    @IBAction func amount3Clicked(_ sender: Any) {
        self.amount1.backgroundColor = UIColor.keplerGreen
        self.amount2.backgroundColor = UIColor.keplerGreen
        self.amount3.backgroundColor = UIColor.keplerDarkGreen
        
        self.amountField.text = "৳ 100"
    }
}
