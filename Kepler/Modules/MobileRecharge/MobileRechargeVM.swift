//
//  MobileRechargeVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 19/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

class MobileRechargeVM {
    func mobileRecharge(pin: String, recipientNumber: String, amount: String, connectionType: String, operatorType: String, response: @escaping(_ success: Bool, _ message: String)->Void) {
        let params = [
            "recipientNumber" : recipientNumber,
            "amount" : Int(amount)!,
            "connectionType" : connectionType,
            "operator" : operatorType,
            "pin" : pin
            ] as [String : AnyObject]
        
        AlamofireWrapper.requestPOSTURL(HttpURLs.mobileRecharge, params: params, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            let mobileRechareResponse = MobileRechareResponse (JSONString: responseData)
            if let message = mobileRechareResponse?.message, message != "" {
                response(true, message)
            } else {
                response(false, DefaultMessages.ERROR_MESSAGE.rawValue.localized)
            }
        }) { (error) in
            DLog(error)
            response(false, error)
        }
    }
}
