//
//  MobileRechargeRootVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 18/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class MobileRechargeVC: UIViewController {
    
    var contacts: [ContactsItem] = []
    var searchedContacts: [ContactsItem] = []
    var recentContacts: [ContactsItem]!
    var recentTempContacts: [ContactsItem]!
    
    @IBOutlet weak var allContactList: UITableView!
    @IBOutlet weak var phoneNumberField: CustomUITextField!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    var senderName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    func setupView() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        
        self.loadRecentMobileRecharge()
        self.loadContacts()
        
        self.allContactList.delegate = self
        self.allContactList.dataSource = self
        self.phoneNumberField.delegate = self
    }
    
    func loadContacts() {
        self.contacts = ContactHelper.contactData
        self.searchedContacts = self.contacts
        if self.contacts.count == 0 {
            emptyMessageInTableView(self.allContactList, "No contacts found".localized)
        } else {
            emptyMessageInTableView(self.allContactList, "")
        }
    }
    
    private func loadRecentMobileRecharge() {
        if SessionManager.hasValue(forKey: .RECENT_MOBILE_RECHARGE) {
            self.recentContacts = [ContactsItem] (JSONString: SessionManager.getString(forKey: .RECENT_MOBILE_RECHARGE)!)!
            self.recentTempContacts = self.recentContacts
        }
    }
    
    func gotoRechargePage() {
        if !self.phoneNumberField.text!.isEmpty && self.phoneNumberField.text!.count == 11 && self.phoneNumberField.text!.isValidPhoneNumber {
            if let vc = UIStoryboard(storyboard: .mobileRecharge).instantiateViewController(withIdentifier: MobileRechargeAmounEntryVC.identifier) as? MobileRechargeAmounEntryVC {
                vc.name = senderName
                vc.msisdn = self.phoneNumberField.text!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: "88", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ".", with: "")
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            self.showToast(message: .INVALID_PHONE_NUMBER)
            self.senderName = nil
        }
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        gotoRechargePage()
        self.phoneNumberField.resignFirstResponder()
    }
}
