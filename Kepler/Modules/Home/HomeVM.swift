//
//  HomeVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 15/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

class HomeVM {
    class func checkBalance(response: @escaping(_ success: Bool, _ message: String)->Void){
        AlamofireWrapper.requestPOSTURL(HttpURLs.balanceCheck, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            let currentBalanceResponse = CheckBlanceResponse (JSONString: responseData)
            if let balance = currentBalanceResponse?.currentBalance {
                response(true, balance)
            } else {
                response(false, DefaultMessages.ERROR_MESSAGE.rawValue.localized)
            }
            DLog(responseData)
        }) { (error) in
            DLog(error)
            response(false, error)
        }
    }
}
