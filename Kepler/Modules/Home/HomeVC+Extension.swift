//
//  HomeVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 15/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import XLPagerTabStrip

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.recentDataList.count != 0 {
            return self.recentDataList.count
        } else {
            return 3
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.recentDataList.count != 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentMenuCell", for: indexPath) as! RecentMenuCell
            cell.setupCell(recentData: recentDataList[indexPath.row])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentMenuCell", for: indexPath) as! RecentMenuCell
            cell.setupCell(menu: recent_menu_data[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.recentDataList.count != 0 {
            let msisdn = self.recentDataList[indexPath.row].receiverPhoneNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ".", with: "")
            let userName = self.recentDataList[indexPath.row].receiverName
            
            switch self.recentDataList[indexPath.row].transactionType {
            case .RECENT_CASH_OUT:
                CashOutVM.checkCashOutAgent(msisdn: msisdn) { (isSuccess, userName) in
                    if isSuccess {
                        if msisdn.count == 11 {
                            let vc = UIStoryboard(storyboard: .cashOut).instantiateViewController(withIdentifier: CashOutAmountEntryVC.identifier) as! CashOutAmountEntryVC
                            vc.msisdn = msisdn
                            vc.name = userName
                            self.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            self.showToast(message: .INVALID_PHONE_NUMBER)
                        }
                    } else {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: AgentNotRegisteredVC.identifier) as! AgentNotRegisteredVC
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            case .RECENT_MOBILE_RECHARGE:
                let vc = UIStoryboard(storyboard: .mobileRecharge).instantiateViewController(withIdentifier: MobileRechargeAmounEntryVC.identifier) as! MobileRechargeAmounEntryVC
                vc.name = userName
                vc.msisdn = msisdn
                self.navigationController?.pushViewController(vc, animated: true)
            case .RECENT_MERCHANT:
                MerchantVM.checkMerchant(msisdn: msisdn) { (isSuccess, userName) in
                    if isSuccess {
                        let vc = UIStoryboard(storyboard: .merchant).instantiateViewController(withIdentifier: MerchantPaymentVC.identifier) as! MerchantPaymentVC
                        vc.name = userName
                        vc.number = msisdn
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: MerchantNotRegisteredVC.identifier) as! MerchantNotRegisteredVC
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            case .RECENT_SEND_MONEY:
                LoginRegistrationVM().checkUser(msisdn: msisdn) { (isSuccess, message) in
                    if isSuccess {
                        let vc = UIStoryboard(storyboard: .send_money).instantiateViewController(withIdentifier: SendMoneyAmountEntryVC.identifier) as! SendMoneyAmountEntryVC
                        vc.msisdn = msisdn
                        vc.name = userName
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        if message == "User not registered." {
                            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: UserNotRegisteredErrorVC.identifier) as! UserNotRegisteredErrorVC
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
            default:
                break
            }
        } else {
            if indexPath.row == 0 {
                if BillingVM.insuranceList != nil {
                    let vc = UIStoryboard(storyboard: .insurance).instantiateViewController(withIdentifier: InsuranceVC.identifier) as! InsuranceVC
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.showToast(message: "Data loading please wait...")
                }
            } else if indexPath.row == 1 {
                if BillingVM.utilityList != nil {
                    let vc = UIStoryboard(storyboard: .doMore).instantiateViewController(withIdentifier: BillPaymentVC.identifier) as! BillPaymentVC
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.showToast(message: "Data loading please wait...")
                }
            } else if indexPath.row == 2 {
                if BillingVM.institutionList != nil {
                    let vc = UIStoryboard(storyboard: .tuition_fee).instantiateViewController(withIdentifier: TuitionFeeVC.identifier) as! TuitionFeeVC
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.showToast(message: "Data loading please wait...")
                }
            } else if indexPath.row == 3 {
                if BillingVM.otherList != nil {
                    let vc = UIStoryboard(storyboard: .doMore).instantiateViewController(withIdentifier: BillPaymentVC.identifier) as! BillPaymentVC
                    vc.isForUtility = false
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.showToast(message: "Data loading please wait...")
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row < 3 {
            let size = (collectionView.bounds.width / 3) - 10
            heightSize = size
            return CGSize(width: size, height: size - 5)
        } else {
            let size = (collectionView.bounds.width / 2) - 20
            return CGSize(width: size, height: heightSize - 5)
        }
    }
}

extension HomeVC: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

extension HomeVC: OnPopupDismissDelegate {
    func onPopupdismissClicked() {
        let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: PictureGuidelineVC.identifier) as! PictureGuidelineVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
