//
//  MainVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 23/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Contacts
import NotificationBannerSwift

class MainVC: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var balanceCheckView: UIView!
    @IBOutlet weak var checkBalanceTitle: UILabel!
    
    @IBOutlet weak var walletIcon: UIImageView!
    @IBOutlet weak var loadignIndicator: UIActivityIndicatorView!
    
    var timer: Timer!
    
    override func viewDidLoad() {
        self.loadignIndicator.isHidden = true
        
        self.setupStyle()
        super.viewDidLoad()
        
        self.setupView()
        self.loadAllDataIntoCache()
        
        if SessionManager.hasValue(forKey: .IS_FOR_LANGUAGE_RELOAD) {
            if SessionManager.getBool(forKey: .IS_FOR_LANGUAGE_RELOAD)! {
                SessionManager.setValue(false, forKey: .IS_FOR_LANGUAGE_RELOAD)
                DispatchQueue.main.async {
                    self.moveToViewController(at: 3, animated: false)
                }
            }
        }
    }
    
    func setupView() {
        self.balanceCheckView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.checkBalanceClicked)))
        CNContactStore().requestAccess(for: .contacts) { (granted, error) in
            
        }
    }
    
    private func loadAllDataIntoCache() {
        DispatchQueue.global(qos: .background).async {
            ContactHelper.getInstance()
            TransactionHistoryVM.getInstance()
            MyAccountVM.getAccounts()
            BillingVM.getInstance()
            NIDFeesVM.getInstance()

        }
    }
    
    func addTopShadow() {
        let frame = CGRect(x: 0, y: 35, width: self.view.frame.width, height: 6)
        
        let backgroundView = UIView(frame: frame)
        self.view.addSubview(backgroundView)
        
        //A linear Gradient Consists of two colours: top colour and bottom colour
        let topColor = UIColor.black
        let bottomColor = UIColor.clear
        
        //Add the top and bottom colours to an array and setup the location of those two.
        let gradientColors: [CGColor] = [topColor.cgColor, bottomColor.cgColor]
        let gradientLocations: [CGFloat] = [0.0, 1.0]
        
        //Create a Gradient CA layer
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations as [NSNumber]?
        gradientLayer.frame = frame
        gradientLayer.opacity = 0.2
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)
        backgroundView.layer.zPosition = 100
        
    }
    
    func setupStyle() {
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.buttonBarItemFont = UIFont(name: FontList.Regular.rawValue, size: 0)!
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarMinimumInteritemSpacing = 0
        settings.style.selectedBarHeight = 2
        settings.style.selectedBarBackgroundColor = UIColor.keplerRed
        settings.style.buttonBarBackgroundColor = .clear
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.imageView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            newCell?.imageView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            
            oldCell?.label.textColor = UIColor.keplerGray
            newCell?.label.textColor = UIColor.keplerRed
            
            oldCell?.imageView.tintColor = UIColor.keplerGray
            newCell?.imageView.tintColor = UIColor.keplerRed
            
            if let new  = newCell {
                if new.label.text!.isEqual(TabMenuItem.DO_MORE.rawValue.localized) {
                    new .imageView.image = UIImage(named: "ic_do_more_tab")
                }
            }
            if let old = oldCell {
                if old.label.text!.isEqual(TabMenuItem.DO_MORE.rawValue.localized) {
                    old.imageView.image = UIImage(named: "ic_do_more_tab_1")
                }
            }
            
            if newCell?.label.text != nil {
                self?.pageTitle.text = newCell?.label.text
            }
        }
    }
    
    //MARK: Setup main menu tab bar
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        //"Top Seller", "Deal", "Most Liked"
        
        let home = UIStoryboard(storyboard: .home).instantiateViewController(withIdentifier: HomeVC.identifier) as! HomeVC
        home.itemInfo = IndicatorInfo(title: TabMenuItem.HOME.rawValue.localized, image: UIImage(named: Images.HOME_TAB.rawValue))
        
        let doMore = UIStoryboard(storyboard: .doMore).instantiateViewController(withIdentifier: DoMoreVC.identifier) as! DoMoreVC
        doMore.itemInfo = IndicatorInfo(title: TabMenuItem.DO_MORE.rawValue.localized, image: UIImage(named: Images.DO_MORE_TAB.rawValue))
        
        let transactions = UIStoryboard(storyboard: .transactionHistory).instantiateViewController(withIdentifier: TransactionHistoryVC.identifier) as! TransactionHistoryVC
        transactions.itemInfo = IndicatorInfo(title: TabMenuItem.TRANSACTIONS.rawValue.localized, image: UIImage(named: Images.TRANSACTIONS_TAB.rawValue))
        
        let myAccount = UIStoryboard(storyboard: .my_account).instantiateViewController(withIdentifier: MyAccountVC.identifier) as! MyAccountVC
        myAccount.itemInfo = IndicatorInfo(title: TabMenuItem.MY_ACCOUNT.rawValue.localized, image: UIImage(named: Images.MY_ACCOUNT_TAB.rawValue))
        myAccount.reloadViewForLanguage = self
        
        return [home, doMore, transactions, myAccount]
    }
    
    @objc func checkBalanceClicked() {
        if checkBalanceTitle.text == "Tap to View Balance".localized {
            self.walletIcon.isHidden = true
            self.loadignIndicator.isHidden = false
            self.loadignIndicator.startAnimating()
            
            HomeVM.checkBalance() { (response, message) in
                self.walletIcon.isHidden = false
                self.loadignIndicator.isHidden = true
                self.loadignIndicator.stopAnimating()
                
                if response {
                    self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.changeToNormalText), userInfo: nil, repeats: false) // For revert back to previous text
                    self.checkBalanceTitle.text = String(format: "৳ %.2f", (message as NSString).doubleValue)
                } else {
                    if message != "No Internet!.".localized {
                        StatusBarNotificationBanner(title: message, style: .danger).show()
                    }
                }
            }
        } else {
            self.changeToNormalText()
        }
    }
    
    @objc func changeToNormalText() {
        timer.invalidate()
        self.checkBalanceTitle.text = "Tap to View Balance".localized
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        self.navigationItem.title = " "
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}

extension MainVC: OnReloadViewForLanguageDelegate {
    func onReloadViewForLanguage() {
        SessionManager.setValue(true, forKey: .IS_FOR_LANGUAGE_RELOAD)
        if let vc = UIStoryboard(storyboard: .home).instantiateViewController(withIdentifier: MainVC.identifier) as? MainVC {
            let navVC = UINavigationController(rootViewController: vc)
            let window = UIApplication.shared.keyWindow!
            window.rootViewController = navVC
            UIView.transition(with: window, duration: 0.2, options: UIView.AnimationOptions.transitionCrossDissolve) {
                window.makeKeyAndVisible()
            }
        }
    }
}
