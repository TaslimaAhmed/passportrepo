//
//  HomeViewController.swift
//  Kepler
//
//  Created by Mufad Monwar on 7/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class HomeVC: UIViewController {
    
    
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var iv_profile_image: UIImageView!
    
    @IBOutlet weak var sendMoneyView: UIView!
    @IBOutlet weak var disableSendMoneyView: UIView!
    @IBOutlet weak var mobileRechargeView: UIView!
    @IBOutlet weak var disableMobileRechargeView: UIView!
    @IBOutlet weak var makePaymentView: UIView!
    @IBOutlet weak var disableMakePaymentView: UIView!
    @IBOutlet weak var cashOutView: UIView!
    @IBOutlet weak var disableCashOutView: UIView!
    @IBOutlet weak var addMoneyView: UIView!
    @IBOutlet weak var disableAddMoneyView: UIView!
    
    @IBOutlet weak var recentTabTitle: CustomLabel!
    
    @IBOutlet weak var cv_recent_items: UICollectionView!
    
    @IBOutlet weak var verificationView: UIView!
    @IBOutlet weak var verificationTitleLabel: CustomLabel!
    @IBOutlet weak var verificationMessageLabel: CustomLabel!
    
    var heightSize: CGFloat = 0
    
    var itemInfo: IndicatorInfo!
    
    var recent_menu_data:[HomeMenu] = []
    var recentDataList: [RecentDataModel] = []
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recent_menu_data = gatherDataForRecent()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadRecentData()
    }
    
    func setupView() {
        self.cv_recent_items.delegate = self
        self.cv_recent_items.dataSource = self
        self.viewDidLayoutSubviews()
        self.cv_recent_items.layoutIfNeeded()
        self.loadRecentData()
        
        self.sendMoneyView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSendMoney)))
        self.mobileRechargeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openMobileRecharge)))
        self.makePaymentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openMakePayment)))
        self.cashOutView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openCashOut)))
        self.addMoneyView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openAddMoney)))
        
        if !SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.SUCCESS.rawValue) {
            let featureLists = LoginRegistrationVM.loginResponse!.userLoginInfo!.featureAccesibilityMatrix!.featureList!
            featureLists.forEach {
                switch ($0.featureName!) {
                case "P2P":
                    if $0.featureAccess! {
                        self.sendMoneyView.isUserInteractionEnabled = true
                        self.disableSendMoneyView.isHidden = true
                    } else {
                        self.sendMoneyView.isUserInteractionEnabled = false
                        self.disableSendMoneyView.isHidden = false
                    }
                case "TOP_UP_PREPAID", "TOP_UP_POSTPAID":
                    if $0.featureAccess! {
                        self.mobileRechargeView.isUserInteractionEnabled = true
                        self.disableMobileRechargeView.isHidden = true
                    } else {
                        self.mobileRechargeView.isUserInteractionEnabled = false
                        self.disableMobileRechargeView.isHidden = false
                    }
                case "MERCHANT_PAYMENT":
                    if $0.featureAccess! {
                        self.makePaymentView.isUserInteractionEnabled = true
                        self.disableMakePaymentView.isHidden = true
                    } else {
                        self.makePaymentView.isUserInteractionEnabled = false
                        self.disableMakePaymentView.isHidden = false
                    }
                case "CASH_OUT":
                    if $0.featureAccess! {
                        self.cashOutView.isUserInteractionEnabled = true
                        self.disableCashOutView.isHidden = true
                    } else {
                        self.cashOutView.isUserInteractionEnabled = false
                        self.disableCashOutView.isHidden = false
                    }
                case "ADD_MONEY_TBL":
                    if $0.featureAccess! {
                        self.addMoneyView.isUserInteractionEnabled = true
                        self.disableAddMoneyView.isHidden = true
                    } else {
                        self.addMoneyView.isUserInteractionEnabled = false
                        self.disableAddMoneyView.isHidden = false
                    }
                default:
                    break
                }
            }
        }
        
        if let userName = SessionManager.getString(forKey: .USER_NAME) {
            var name = userName.components(separatedBy: " ")
            if name.count > 1 {
                name.removeLast()
            }
            var fName = ""
            name.forEach {
                fName += $0 + " "
            }
            self.user_name.text = "Hi, \(fName)"
        } else {
            self.user_name.text = "Hi, \(SessionManager.getString(forKey: .USER_MSISDN)!)"
        }
        
        //----------------For EKYC verification-----------------------//
        self.verificationView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.startEKYCProcess)))
        self.setVerificationBarMessage()
        
        if SessionManager.getBool(forKey: .IS_FIRST_LAUNCH)! {
            SessionManager.setValue(false, forKey: .IS_FIRST_LAUNCH)
            if !SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.SUCCESS.rawValue){
                self.startEKYCProcess()
            }
        }
        //----------------For EKYC verification-----------------------//
    }
    
    //----------------For EKYC verification-----------------------//
    func setVerificationBarMessage() {
        if SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.INCOMPLETE.rawValue) || SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.FAILED.rawValue) {
            
            self.verificationView.isHidden = false
            if SessionManager.getString(forKey: .EKYC_USER_TYPE)!.elementsEqual(EkycUserType.OLD_USER.rawValue){
                self.verificationTitleLabel.text = "Access to all services will expire after six months!".localized
                self.verificationMessageLabel.text = "Complete your registration to get unlimited access".localized
            } else if SessionManager.getString(forKey: .EKYC_USER_TYPE)!.elementsEqual(EkycUserType.NEW_USER.rawValue){
                self.verificationTitleLabel.text = "Complete registration to unlock all TAP services!".localized
                self.verificationMessageLabel.text = "Click here to check your registration status".localized
            }
        } else if SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.PENDING.rawValue) {
            self.verificationView.isHidden = false
            self.verificationTitleLabel.text = "Your verification is in progress!".localized
            self.verificationMessageLabel.text = "Click here to check your registration status".localized
        } else {
            self.verificationView.isHidden = true
        }
    }
    
    @objc func startEKYCProcess() {
        LoginRegistrationVM.ekycRefresh { (isSuccess) in
            self.setVerificationBarMessage()
            if SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.FAILED.rawValue){
                let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: VerificationFailedPopupVC.identifier) as! VerificationFailedPopupVC
                vc.onPopupDismissDelegate = self
                self.present(vc, animated: true, completion: nil)
            } else if SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.INCOMPLETE.rawValue) {
                let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: RegistrationIncompletePopupVC.identifier) as! RegistrationIncompletePopupVC
                vc.onPopupDismissDelegate = self
                self.present(vc, animated: true, completion: nil)
            } else if SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.PENDING.rawValue) {
                let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: VerificationPendingVC.identifier) as! VerificationPendingVC
                self.present(vc, animated: true, completion: nil)
            } else if SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.SUCCESS.rawValue){
                let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: VerificationSuccessfullVC.identifier) as! VerificationSuccessfullVC
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    //----------------For EKYC verification-----------------------//
    
    @objc private func openSendMoney() {
        if let viewController = UIStoryboard(storyboard: .send_money).instantiateViewController(withIdentifier: SendMoneyVC.identifier) as? SendMoneyVC {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    @objc private func openMobileRecharge() {
        if let viewController = UIStoryboard(storyboard: .mobileRecharge).instantiateViewController(withIdentifier: MobileRechargeVC.identifier) as? MobileRechargeVC {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    @objc private func openMakePayment() {
        if let viewController = UIStoryboard(storyboard: .merchant).instantiateViewController(withIdentifier: MerchantVC.identifier) as? MerchantVC {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    @objc private func openCashOut() {
        if let viewController = UIStoryboard(storyboard: .cashOut).instantiateViewController(withIdentifier: CashOutVC.identifier) as? CashOutVC {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    @objc private func openAddMoney() {
        if let viewController = UIStoryboard(storyboard: .addMoney).instantiateViewController(withIdentifier: AddMoneyRootVC.identifier) as? AddMoneyRootVC {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func gatherDataForRecent()->[HomeMenu] {
        var temp:[HomeMenu] = []
        
        let featureLists = LoginRegistrationVM.loginResponse!.userLoginInfo!.featureAccesibilityMatrix!.featureList!
        
        let menu1 = HomeMenu(name: .INSURANCE, image: UIImage(named: .INSURANCE), featureName: "INSURANCE_PAYMENT", isActive: true)
        let menu2 = HomeMenu(name: .BILL_PAYMENT, image: UIImage(named: .BILL_PAY), featureName: "BILL_PAYMENT", isActive: true)
        let menu3 = HomeMenu(name: .TUITION_FEES, image: UIImage(named: .TUITION_FEES), featureName: "TUTION_FEE_PAYMENT", isActive: true)
        let menu4 = HomeMenu(name: .OTHERS, image: UIImage(named: .OTHERS), featureName: "OTHERS_PAYMENT", isActive: true)
        
        if !SessionManager.getString(forKey: .EKYC_STATUS)!.elementsEqual(EkycUserStatus.SUCCESS.rawValue) {
            featureLists.forEach { feature in
                if feature.featureName == "INSURANCE_PAYMENT" {
                    menu1.isActive = feature.featureAccess!
                }
                if feature.featureName == "BILL_PAYMENT" {
                    menu2.isActive = feature.featureAccess!
                }
                if feature.featureName == "TUTION_FEE_PAYMENT" {
                    menu3.isActive = feature.featureAccess!
                }
                if feature.featureName == "INSURANCE_PAYMENT" {
                    menu4.isActive = feature.featureAccess!
                }
            }
        }
        
        temp.append(menu1)
        temp.append(menu2)
        temp.append(menu3)
        temp.append(menu4)
        return temp
    }
    
    func loadRecentData() {
        if SessionManager.hasValue(forKey: .RECENT_MERCHANT) || SessionManager.hasValue(forKey: .RECENT_CASH_OUT) || SessionManager.hasValue(forKey: .RECENT_SEND_MONEY) || SessionManager.hasValue(forKey: .RECENT_MOBILE_RECHARGE) {
            self.recentTabTitle.text = "Recents".localized
            self.recentDataList.removeAll()
            var recentData: [ContactsItem] = []
            
            if SessionManager.hasValue(forKey: .RECENT_MERCHANT) {
                recentData = [ContactsItem] (JSONString: SessionManager.getString(forKey: .RECENT_MERCHANT)!)!
                for i in 0 ..< recentData.count {
                    if i == 0 {
                        self.recentDataList.append(RecentDataModel(transactionType: .RECENT_MERCHANT, recentData[i].number, recentData[i].name))
                    } else if i == 1 {
                        self.recentDataList.append(RecentDataModel(transactionType: .RECENT_MERCHANT, recentData[i].number, recentData[i].name))
                    }
                }
            }
            if SessionManager.hasValue(forKey: .RECENT_CASH_OUT) {
                recentData = [ContactsItem] (JSONString: SessionManager.getString(forKey: .RECENT_CASH_OUT)!)!
                for i in 0 ..< recentData.count {
                    if i == 0 {
                        self.recentDataList.append(RecentDataModel(transactionType: .RECENT_CASH_OUT, recentData[i].number, recentData[i].name))
                    } else if i == 1 {
                        self.recentDataList.append(RecentDataModel(transactionType: .RECENT_CASH_OUT, recentData[i].number, recentData[i].name))
                    }
                }
            }
            if SessionManager.hasValue(forKey: .RECENT_SEND_MONEY) {
                recentData = [ContactsItem] (JSONString: SessionManager.getString(forKey: .RECENT_SEND_MONEY)!)!
                for i in 0 ..< recentData.count {
                    if i == 0 {
                        self.recentDataList.append(RecentDataModel(transactionType: .RECENT_SEND_MONEY, recentData[i].number, recentData[i].name))
                    } else if i == 1 {
                        self.recentDataList.append(RecentDataModel(transactionType: .RECENT_SEND_MONEY, recentData[i].number, recentData[i].name))
                    }
                }
            }
            if SessionManager.hasValue(forKey: .RECENT_MOBILE_RECHARGE) {
                recentData = [ContactsItem] (JSONString: SessionManager.getString(forKey: .RECENT_MOBILE_RECHARGE)!)!
                for i in 0 ..< recentData.count {
                    if i == 0 {
                        self.recentDataList.append(RecentDataModel(transactionType: .RECENT_MOBILE_RECHARGE, recentData[i].number, recentData[i].name))
                    } else if i == 1 {
                        self.recentDataList.append(RecentDataModel(transactionType: .RECENT_MOBILE_RECHARGE, recentData[i].number, recentData[i].name))
                    }
                }
            }
            self.cv_recent_items.reloadData()
        }
    }
}
