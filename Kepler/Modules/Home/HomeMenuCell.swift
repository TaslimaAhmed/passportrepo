//
//  HomeMenuCell.swift
//  Kepler
//
//  Created by Mufad Monwar on 7/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class HomeMenuCell: UICollectionViewCell {
    @IBOutlet weak var iv_menu_icon: UIImageView!
    @IBOutlet weak var tv_menu_name: UILabel!
    @IBOutlet weak var disable_view: UIView!
    
    override class func awakeFromNib() {
        
    }
    
    func setupCell(menu: HomeMenu){
        iv_menu_icon.image = menu.icon
        tv_menu_name.text = menu.name
        if menu.isActive {
            disable_view.isHidden = true
        } else {
            self.isUserInteractionEnabled = false
            disable_view.isHidden = false
        }
    }
}
