//
//  RecentMenuCell.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 23/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class RecentMenuCell: UICollectionViewCell {
    @IBOutlet weak var iv_menu_icon: UIImageView!
    @IBOutlet weak var tv_menu_name: UILabel!
    @IBOutlet weak var disable_view: UIView!
    
    func setupCell(menu: HomeMenu) {
        iv_menu_icon.image = menu.icon
        tv_menu_name.text = menu.name
        if menu.isActive {
            disable_view.isHidden = true
        } else {
            self.isUserInteractionEnabled = false
            disable_view.isHidden = false
        }
    }
    
    func setupCell(recentData: RecentDataModel) {
        if recentData.receiverName != "" {
            tv_menu_name.text = recentData.receiverName
        } else {
            tv_menu_name.text = recentData.receiverPhoneNumber
        }
        switch recentData.transactionType {
        case .RECENT_MOBILE_RECHARGE:
            iv_menu_icon.image = UIImage(named: Images.MOBILE)
        case .RECENT_CASH_OUT:
            iv_menu_icon.image = UIImage(named: Images.CASH_OUT)
        case .RECENT_MERCHANT:
            iv_menu_icon.image = UIImage(named: Images.MARCHENT_PAY)
        case .RECENT_SEND_MONEY:
            iv_menu_icon.image = UIImage(named: Images.SEND_MONEY)
        default:
            break
        }
    }
}
