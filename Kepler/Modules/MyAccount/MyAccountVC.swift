//
//  MyAccountVC.swift
//  Kepler
//
//  Created by Mufad Monwar on 10/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD
import YPImagePicker
import XLPagerTabStrip

class MyAccountVC: UIViewController {
    var itemInfo: IndicatorInfo!
    
    @IBOutlet weak var profileTableView: UITableView!
    
    var reloadViewForLanguage: OnReloadViewForLanguageDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    private func setupView() {
        self.profileTableView.dataSource = self
        self.profileTableView.delegate = self
    }
    
    func logoutUser() {
        SVProgressHUD.show()
        MyAccountVM.logoutUser { (response, message) in
            SVProgressHUD.dismiss()
            if (response){
                self.showToast(message: message)
                
                SessionManager.removeData(forKey: .ACCESS_TOKEN)
                initOnboardingView()
            } else{
                self.showToast(message: message)
            }
        }
    }
    
    @IBAction func languageChangeClicked(_ sender: Any) {
        if SessionManager.getString(forKey: .DEFAULT_LANGUAGE)!.elementsEqual("en") {
            SessionManager.setValue("bn", forKey: .DEFAULT_LANGUAGE)
        } else {
            SessionManager.setValue("en", forKey: .DEFAULT_LANGUAGE)
        }
        reloadViewForLanguage.onReloadViewForLanguage()   
    }
}

extension MyAccountVC: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
