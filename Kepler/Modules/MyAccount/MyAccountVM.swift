//
//  MyAccountVC.swift
//  Kepler
//
//  Created by Mufad Monwar on 10/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class MyAccountVM {
    private static var myAccountInstance: MyAccountVM!
    
    public static var existingAccounts : [AccountInfo] = []
    
    private init() {
        
    }
    
    public static func getInstance() {
        if self.myAccountInstance == nil {
            self.myAccountInstance = MyAccountVM()
            self.getAccounts()
        }
    }
    
    public static func logoutUser(response: @escaping(_ success: Bool, _ message: String)->Void){
        //Bearer Token, MSSIDN, SystemCurrentTime will be in Header
        AlamofireWrapper.requestPOSTURL(HttpURLs.logout, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            let logoutResponse = RegistrationResponse (JSONString: responseData)
            if logoutResponse?.message == "Successfully Logged out" {
                response(true, logoutResponse!.message!)
            } else {
                response(false, "Some error occurred. Please try again later.")
            }
        }) { (error) in
            response(false, "Cannot Log Out Now")
            DLog(error)
        }
    }
    
    public static func getAccounts(response: @escaping(_ success: Bool, _ message: String)->Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.linkedAccounts, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            let connectedAccountResponse = ConnectedAccountsResopnse (JSONString: responseData)
            
            if connectedAccountResponse!.accountInfo!.count>0 {
                self.existingAccounts = connectedAccountResponse!.accountInfo!
                response(true,"Success")
            } else {
                response(false, "")
            }
        }) { (error) in
            DLog(error)
            response(false, DefaultMessages.ERROR_MESSAGE.rawValue.localized)
        }
    }
    
    public static func getAccounts() {
        AlamofireWrapper.requestPOSTURL(HttpURLs.linkedAccounts, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            let connectedAccountResponse = ConnectedAccountsResopnse (JSONString: responseData)
            self.existingAccounts = connectedAccountResponse!.accountInfo!
        }) { (error) in
            DLog(error)
        }
    }
    
    public static func resetPIN(params: [String: AnyObject], complition: @escaping(_ isSuccessfull: Bool, _ message: String) -> Void) {
        SVProgressHUD.show()
        AlamofireWrapper.requestPOSTURL(HttpURLs.update_pin, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            let responseData = ErrorResponse (JSONString: response)
            if let message = responseData?.message, message.lowercased().contains("success") {
                complition(true, message)
            } else {
                complition(false, responseData!.message!)
            }
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(false, error)
        }
    }
}
