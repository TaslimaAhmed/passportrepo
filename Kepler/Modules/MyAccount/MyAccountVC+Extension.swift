//
//  MyAccountVC+Extension.swift
//  Kepler
//
//  Created by Mufad Monwar on 8/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import UIKit


extension MyAccountVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProfileModel.profileDataList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTVC", for: indexPath) as! MyAccountTVC
        cell.loadCell(data: ProfileModel.profileDataList()[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = UIStoryboard(storyboard: .my_account).instantiateViewController(withIdentifier: ResetPinVC.identifier) as! ResetPinVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 1:
            break
        case 2:
            let vc = UIStoryboard(storyboard: .my_account).instantiateViewController(withIdentifier: BankInformationVC.identifier) as! BankInformationVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 3:
            let vc = UIStoryboard(storyboard: .my_account).instantiateViewController(withIdentifier: AboutVC.identifier) as! AboutVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            self.logoutUser()
        }
    }
}
