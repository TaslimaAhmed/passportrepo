//
//  ResetPinVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 1/11/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class ResetPinVC: UIViewController {
    
    @IBOutlet weak var oldPinTextField: CustomUITextField!
    @IBOutlet weak var newPinTextField: CustomUITextField!
    @IBOutlet weak var confirmNewPinTextField: CustomUITextField!
    @IBOutlet weak var pinDoesNotMatchErrorLabel: CustomLabel!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    public func setupView() {
        self.pinDoesNotMatchErrorLabel.isHidden = true
        enableDisableNextButton(button: self.nextButton)
        self.timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.checkInputs), userInfo: nil, repeats: true)
        
        self.oldPinTextField.defaultTextAttributes.updateValue(28.0, forKey: NSAttributedString.Key.kern)
        self.newPinTextField.defaultTextAttributes.updateValue(28.0, forKey: NSAttributedString.Key.kern)
        self.confirmNewPinTextField.defaultTextAttributes.updateValue(28.0, forKey: NSAttributedString.Key.kern)
    }
    
    @objc private func checkInputs() {
        if (!self.oldPinTextField.text!.isEmpty && self.oldPinTextField.text!.count == 4) &&
            (!self.newPinTextField.text!.isEmpty && self.newPinTextField.text!.count == 4) &&
            (!self.confirmNewPinTextField.text!.isEmpty && self.confirmNewPinTextField.text!.count == 4) {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.timer.invalidate()
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if self.newPinTextField.text!.elementsEqual(self.confirmNewPinTextField.text!) {
            self.pinDoesNotMatchErrorLabel.isHidden = true
            let params = [
                "oldPin" : self.oldPinTextField.text!.toBase64(),
                "newPin" : self.newPinTextField.text!.toBase64()
            ] as [String: AnyObject]
            MyAccountVM.resetPIN(params: params) { (isSuccess, message) in
                if isSuccess {
                    let vc = UIStoryboard(storyboard: .my_account).instantiateViewController(withIdentifier: SuccessPinChangeVC.identifier) as! SuccessPinChangeVC
                    self.present(vc, animated: true, completion: nil)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorTitle = "Incorrect PIN".localized
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        } else {
            self.pinDoesNotMatchErrorLabel.isHidden = false
        }
    }
}
