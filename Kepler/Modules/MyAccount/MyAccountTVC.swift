//
//  MyAccountTVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 25/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class MyAccountTVC: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func loadCell(data: ProfileModel) {
        self.profileImage.image = UIImage(named: data.profileImage!)
        self.profileName.text = data.profileName
    }
}
