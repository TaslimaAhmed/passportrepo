//
//  BankListTVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/12/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class BankListTVC: UITableViewCell {

    @IBOutlet weak var bankNameLabel: CustomLabel!
    @IBOutlet weak var userNameLable: CustomLabel!
    @IBOutlet weak var accountNumberLabel: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func loadData(accountInfo: AccountInfo) {
        self.bankNameLabel.text = "Trust Bank Limited".localized
        self.userNameLable.text = accountInfo.accountName
        self.accountNumberLabel.text = accountInfo.maskedAccountNo
    }
}
