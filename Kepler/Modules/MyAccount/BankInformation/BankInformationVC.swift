//
//  BankInformationVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/12/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class BankInformationVC: UIViewController {

    @IBOutlet weak var bankListTableView: UITableView!
    
    var addMoneyVM: AddMoneyVM = AddMoneyVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bankListTableView.delegate = self
        self.bankListTableView.dataSource = self
        
        self.loadData()
    }
    
    private func loadData() {
        SVProgressHUD.show()
        self.addMoneyVM.getAccounts{(response, message) in
            SVProgressHUD.dismiss()
            if response {
                self.bankListTableView.reloadData()
            }
        }
    }
}

extension BankInformationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if addMoneyVM.existingAccounts.count == 0 {
            emptyMessageInTableView(tableView, "No account found".localized)
            return 0
        } else {
            emptyMessageInTableView(tableView, "")
           return addMoneyVM.existingAccounts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BankListTVC", for: indexPath) as! BankListTVC
        cell.loadData(accountInfo: self.addMoneyVM.existingAccounts[indexPath.row])
        return cell
    }
}
