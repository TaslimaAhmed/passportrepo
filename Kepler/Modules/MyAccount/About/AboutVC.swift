//
//  AboutVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 29/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {
    
    @IBOutlet weak var aboutTableView: UITableView!
    @IBOutlet weak var aboutAppLabel: CustomLabel!
    
    var aboutList: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.aboutList.append("About Us".localized)
        self.aboutList.append("Visit our website".localized)
        self.aboutList.append("Terms & Conditions".localized)
        self.aboutList.append("Privacy policy".localized)
        
        self.aboutTableView.dataSource = self
        self.aboutTableView.delegate = self
        
        self.setupView()
    }
    
    func setupView() {
        let versionNumber: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject
        let bundleNumber: AnyObject? = Bundle.main.infoDictionary!["CFBundleVersion"] as AnyObject
        
        self.aboutAppLabel.text = "App version \(versionNumber as! String), Build \(bundleNumber as! String)"
    }
}

extension AboutVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aboutList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutTVC", for: indexPath) as! AboutTVC
        cell.aboutTitle.text = self.aboutList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = UIStoryboard(storyboard: .my_account).instantiateViewController(withIdentifier: AboutUsVC.identifier) as! AboutUsVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 1:
            let vc = UIStoryboard(storyboard: .my_account).instantiateViewController(withIdentifier: VisitWebsiteVC.identifier) as! VisitWebsiteVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 2:
            let vc = UIStoryboard(storyboard: .my_account).instantiateViewController(withIdentifier: AboutTermsConditionVC.identifier) as! AboutTermsConditionVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            let vc = UIStoryboard(storyboard: .my_account).instantiateViewController(withIdentifier: PrivacyPolicyVC.identifier) as! PrivacyPolicyVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        }
    }
}

class AboutTVC: UITableViewCell {
    @IBOutlet weak var aboutTitle: CustomLabel!
    
    override class func awakeFromNib() {
        
    }
}
