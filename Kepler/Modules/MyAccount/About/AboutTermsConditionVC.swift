//
//  AboutTermsConditionVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 29/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class AboutTermsConditionVC: UIViewController {
    
    @IBOutlet weak var termsWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.show()
        self.termsWebView.delegate = self
        if SessionManager.getString(forKey: .DEFAULT_LANGUAGE)!.elementsEqual("en") {
            self.termsWebView.loadRequest(URLRequest(url: URL(string: HttpURLs.termsConditionLinkEN)!))
        } else {
            self.termsWebView.loadRequest(URLRequest(url: URL(string: HttpURLs.termsConditionLinkBN)!))
        }
    }
}

extension AboutTermsConditionVC: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
}
