//
//  PrivacyPolicyVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 3/11/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class PrivacyPolicyVC: UIViewController {
    
    @IBOutlet weak var privacyWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.show()
        self.privacyWebView.delegate = self
        if SessionManager.getString(forKey: .DEFAULT_LANGUAGE)!.elementsEqual("en") {
            self.privacyWebView.loadRequest(URLRequest(url: URL(string: HttpURLs.privacyPolicyLink)!))
        } else {
            self.privacyWebView.loadRequest(URLRequest(url: URL(string: HttpURLs.privacyPolicyLink)!))
        }
    }
}

extension PrivacyPolicyVC: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
}
