//
//  VisitWebsiteVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 29/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class VisitWebsiteVC: UIViewController {
    
    @IBOutlet weak var visitWebsiteWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.show()
        self.visitWebsiteWebView.delegate = self
        self.visitWebsiteWebView.loadRequest(URLRequest(url: URL(string: HttpURLs.visitWebsiteLink)!))
    }
}

extension VisitWebsiteVC: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
}
