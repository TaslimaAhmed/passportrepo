//
//  AboutUsVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 29/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class AboutUsVC: UIViewController {

    @IBOutlet weak var aboutUsWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.show()
        self.aboutUsWebView.delegate = self
        self.aboutUsWebView.loadRequest(URLRequest(url: URL(string: HttpURLs.aboutLink)!))
    }
}

extension AboutUsVC: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
}
