//
//  TermsConditionVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 10/8/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class TermsConditionVC: UIViewController {

    @IBOutlet weak var nextButton: CustomUIButton!
    @IBOutlet weak var termsWebKit: UIWebView!
    
    let onBoardingVM = LoginRegistrationVM()
    var otpReciever: String?
    var isExistingUser: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        
        SVProgressHUD.show()
        self.termsWebKit.delegate = self
        if SessionManager.getString(forKey: .DEFAULT_LANGUAGE)!.elementsEqual("en") {
            self.termsWebKit.loadRequest(URLRequest(url: URL(string: HttpURLs.termsConditionLinkEN)!))
        } else {
            self.termsWebKit.loadRequest(URLRequest(url: URL(string: HttpURLs.termsConditionLinkBN)!))
        }
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        if let existingUser = self.isExistingUser {
            if existingUser {
                let initialViewController = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: SigninVC.identifier) as! SigninVC
                initialViewController.userMSISDN = self.otpReciever
                UIApplication.shared.keyWindow?.rootViewController = initialViewController
                UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.3, options: UIView.AnimationOptions.transitionCrossDissolve) {
                    UIApplication.shared.keyWindow?.makeKeyAndVisible()
                }
            } else {
                self.openSetupAccount()
            }
        } else {
            self.checkUser()
            self.showToast(message: "Error occured. Please try again.".localized)
        }
    }
    
    private func checkUser() {
        self.onBoardingVM.checkUser(msisdn: otpReciever!) { (response, mesasge) in
            if(response) {
                self.isExistingUser = true
            } else {
                if mesasge == "User not registered." {
                    self.isExistingUser = false
                }
            }
        }
    }
    private func openSetupAccount(){
        let vc = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: SetupAccountVC.identifier) as! SetupAccountVC
        vc.userMSISDN = self.otpReciever
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension TermsConditionVC: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }
}
