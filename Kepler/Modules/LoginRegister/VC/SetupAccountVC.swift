//
//  SetupAccountVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 16/11/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class SetupAccountVC: UIViewController {
    
    @IBOutlet weak var fullNameTextField: CustomUITextField!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var genderTextField: CustomUITextField!
    @IBOutlet weak var occupationTextField: CustomUITextField!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    var userMSISDN: String!
    var gender: String = "M"
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    private func setupView() {
        enableDisableNextButton(button: self.nextButton)
        self.genderTextField.text = "Male".localized
        self.nextButton.cornerRadius = self.nextButton.bounds.width / 2
        self.genderView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showGenderSelection)))
        self.timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(self.checkInputFiled), userInfo: nil, repeats: true)
        self.fullNameTextField.becomeFirstResponder()
    }
    
    @objc private func checkInputFiled() {
        if !self.fullNameTextField.text!.isEmpty && !self.occupationTextField.text!.isEmpty && !self.genderTextField.text!.isEmpty {
            enableDisableNextButton(button: self.nextButton, true)
        } else {
            enableDisableNextButton(button: self.nextButton)
        }
    }
    
    @objc private func showGenderSelection() {
        let alertController = UIAlertController(title: "Gender".localized, message: "", preferredStyle: .actionSheet)
        let maleAction = UIAlertAction(title: "Male".localized, style: .default) { (action) in
            self.genderTextField.text = "Male".localized
            self.gender = "M"
        }
        let femaleAction = UIAlertAction(title: "Female".localized, style: .default) { (action) in
            self.genderTextField.text = "Female".localized
            self.gender = "F"
        }
        let otherAction = UIAlertAction(title: "Other".localized, style: .default) { (action) in
            self.genderTextField.text = "Other".localized
            self.gender = "O"
        }
        alertController.addAction(maleAction)
        alertController.addAction(femaleAction)
        alertController.addAction(otherAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        let name = self.fullNameTextField.text!.components(separatedBy: " ")
        let fName = name[0]
        var lName = ""
        if name.count > 1 {
            for i in 1 ..< name.count {
                lName += name[i] + " "
            }
        }
        if lName == "" {
            lName = " "
        }
        
        let userData = [
            "mobileNumber" : self.userMSISDN,
            "pin" : "",
            "firstName" : fName,
            "lastName" : lName,
            "gender" : self.gender,
            "accountType" : "GENERAL",
            "occupation" : self.occupationTextField.text!,
            "mno" : SessionManager.getString(forKey: .MOBILE_OPERATOR_TYPE)
        ] as [String : AnyObject]
        
        let vc = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: SetupPinVC.identifier) as! SetupPinVC
        vc.userData = userData
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
