//
//  SigninVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 23/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD
import NotificationBannerSwift

class SigninVC: UIViewController {
    
    @IBOutlet weak var phoneNumber: CustomUITextField!
    @IBOutlet weak var phoneNumberUnderline: UIView!
    @IBOutlet weak var pin: UITextField!
    @IBOutlet weak var nextButton: CustomUIButton!
    
    let onBoardingVM = LoginRegistrationVM()
    var userMSISDN: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    private func setupView() {
        self.enableDisableNextButton()
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        self.phoneNumber.delegate = self
        self.pin.delegate = self
        
        if self.userMSISDN != nil {
            self.phoneNumber.text = self.userMSISDN
        } else {
            self.phoneNumber.text = SessionManager.getString(forKey: .USER_MSISDN)
        }
        
        pin.defaultTextAttributes.updateValue(28.0, forKey: NSAttributedString.Key.kern)
        SessionManager.setValue(true, forKey: .IS_FIRST_LAUNCH)
    }
    
    @IBAction func forgetPinClicked(_ sender: Any) {
        let vc = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: ForgetPinVC.identifier) as! ForgetPinVC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let validate = validatePhoneNumber(self.phoneNumber.text)
        if(validate.0) {
            self.userMSISDN = validate.1
            
            SVProgressHUD.show()
            self.onBoardingVM.loginUser(msisdn: self.userMSISDN, pin: self.pin.text!) { (response, message) in
                SVProgressHUD.dismiss()
                if response {
                    initHomeView()
                } else {
                    if message != "No Internet!.".localized {
                        StatusBarNotificationBanner(title: message, style: .danger).show()
                    }
                }
            }
        }
    }
    
    func enableDisableNextButton(_ shouldEnable: Bool = false) {
        if shouldEnable {
            self.nextButton.isEnabled = true
            self.nextButton.alpha = 1
        } else {
            self.nextButton.isEnabled = false
            self.nextButton.alpha = 0.5
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.pin.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        self.navigationItem.title = " "
    }
    
    @IBAction func languageClicked(_ sender: Any) {
        if SessionManager.getString(forKey: .DEFAULT_LANGUAGE)!.elementsEqual("en") {
            SessionManager.setValue("bn", forKey: .DEFAULT_LANGUAGE)
            self.loadView()
            self.setupView()
        } else {
            SessionManager.setValue("en", forKey: .DEFAULT_LANGUAGE)
            self.loadView()
            self.setupView()
        }
    }
}
