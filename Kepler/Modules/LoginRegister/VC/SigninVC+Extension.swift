//
//  SigninVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 9/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension SigninVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.phoneNumber {
            self.phoneNumberUnderline.backgroundColor = UIColor.keplerTextUnderLine
        } else {
            self.phoneNumberUnderline.backgroundColor = UIColor.keplerGray
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var phoneLength = self.phoneNumber.text!.count
        if textField == self.phoneNumber {
            if string != "" {
                phoneLength = self.phoneNumber.text!.count + 1
            } else {
                phoneLength = self.phoneNumber.text!.count - 1
            }
        }
        var pinLength = self.pin.text!.count
        if textField == self.pin {
            if string != "" {
                pinLength = self.pin.text!.count + 1
            } else {
                pinLength = self.pin.text!.count - 1
            }
        }
        
        if !phoneNumber.text!.isEmpty && !pin.text!.isEmpty {
            if pinLength >= 4 && phoneLength >= 11 {
                self.enableDisableNextButton(true)
            } else {
                self.enableDisableNextButton()
            }
        }
        
        return true
    }
}
