//
//  RegisterVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 24/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class RegisterVC: UIViewController {
    
    @IBOutlet weak var operatorView: UIView!
    @IBOutlet weak var operatorName: CustomUITextField!
    @IBOutlet weak var phoneNumber: CustomUITextField!
    @IBOutlet weak var phoneNumberUnderLine: UIView!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    let onBoardingVM = LoginRegistrationVM()
    var userMSISDN: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.enableDisableNextButton()
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        self.operatorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openOperatorSelection)))
        self.phoneNumber.delegate = self
        self.phoneNumber.becomeFirstResponder()
    }
    
    @IBAction func onChangeNumber(_ sender: Any) {
        let phoneLength = self.phoneNumber.text!.count
        
        // For replacing unexpected character and change the length
        var tempNumber = self.phoneNumber.text
        tempNumber = tempNumber!.replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        if tempNumber!.hasPrefix("80") {
            tempNumber!.remove(at: tempNumber!.startIndex)
        } else if tempNumber!.hasPrefix("880") {
            tempNumber!.remove(at: tempNumber!.startIndex)
            tempNumber!.remove(at: tempNumber!.startIndex)
        }
        self.phoneNumber.text = tempNumber
        self.phoneNumber.maxLength = 11
        // For replacing unexpected character and change the length
        
        let phnNumber = phoneNumber.text!
        let tempOperatorName = getMobileOperator(phoneNumber: phnNumber)
        operatorName.text = tempOperatorName
        SessionManager.setValue(tempOperatorName, forKey: .MOBILE_OPERATOR_TYPE)
        
        if !phoneNumber.text!.isEmpty && !operatorName.text!.isEmpty {
            if phoneLength >= 11 {
                self.enableDisableNextButton(true)
            } else {
                self.enableDisableNextButton()
            }
        }
    }
    
    @objc private func openOperatorSelection() {
        if let viewController = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: MobileOperatorVC.identifier) as? MobileOperatorVC {
            viewController.mobileOperatorDelegate = self
            self.navigationController?.present(viewController, animated: true)
        }
    }
    
    @IBAction func nextbuttonClicked(_ sender: Any) {
        self.validUser()
    }
    
    private func validUser() {
        let validate = validatePhoneNumber(self.phoneNumber.text)
        if(validate.0) {
            self.userMSISDN = validate.1
            let vc = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: VerifyOTPVC.identifier) as! VerifyOTPVC
            vc.otpReciever = self.userMSISDN
            SessionManager.setValue(self.userMSISDN, forKey: .USER_MSISDN)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.showToast(message: .INVALID_PHONE_NUMBER)
        }
    }
    
    func enableDisableNextButton(_ shouldEnable: Bool = false) {
        if shouldEnable {
            self.nextButton.isEnabled = true
            self.nextButton.alpha = 1
        } else {
            self.nextButton.isEnabled = false
            self.nextButton.alpha = 0.5
        }
    }
}
