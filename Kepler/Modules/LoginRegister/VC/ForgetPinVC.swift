//
//  ForgetPinVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 20/12/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class ForgetPinVC: UIViewController {
    
    @IBOutlet weak var callHelpline: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    private func setupView() {
        self.callHelpline.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(startCallHelpline)))
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func startCallHelpline() {
        if let url = URL(string: "tel://09612201201"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
