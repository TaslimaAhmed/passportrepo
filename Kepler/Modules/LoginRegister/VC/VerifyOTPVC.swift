//
//  VerifyOTPViewController.swift
//  Kepler
//
//  Created by Mufad Monwar on 6/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD
import NotificationBannerSwift

class VerifyOTPVC: UIViewController {
    
    @IBOutlet weak var msisdnLabel: CustomLabel!
    @IBOutlet weak var tf_otp: CustomUITextField!
    @IBOutlet weak var nextButton: CustomUIButton!
    @IBOutlet weak var pinUnderLine: UIView!
    
    @IBOutlet weak var resendButton: CustomUIButton!
    @IBOutlet weak var resendTimerLabel: CustomLabel!
    
    let onBoardingVM = LoginRegistrationVM()
    private var otpReference: String?
    var otpReciever: String?
    var isExistingUser: Bool?
    var resendTimer: Timer!
    var constantBeginTime: Int = 30
    var beginTime: Int = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.generateOTP()
        self.checkUser()
        self.enableDisableNextButton()
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        self.tf_otp.delegate = self
        self.tf_otp.becomeFirstResponder()
        
        self.msisdnLabel.attributedText = NSMutableAttributedString().normal("(You will receive an SMS at ".localized, 10).semiBold("\(self.otpReciever!)", 10).normal(")", 10)
        
        self.enableDisableResend()
        self.startResendTimer()
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let validate = validateOTP(tf_otp.text)
        if(validate.0){
            tf_otp.resignFirstResponder()
            SVProgressHUD.show()
            //self.onBoardingVM.validateOTP(mssidn: self.otpReciever!, otp: validate.1, otpReference: self.otpReference!){ (resopnse, message) in
            self.onBoardingVM.validateOTP(mssidn: SessionManager.getString(forKey: .USER_MSISDN)!, otp: validate.1, otpReference: self.otpReference!){ (resopnse, message) in
                SVProgressHUD.dismiss()
                if (resopnse) {
                    let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: TermsConditionVC.identifier) as! TermsConditionVC
                    vc.isExistingUser = self.isExistingUser
                    vc.otpReciever = self.otpReciever
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true, completion: nil)
                }
            }
        } else {
            let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
            vc.errorMessage = validate.1
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func resendCodeClicked(_ sender: Any) {
        self.generateOTP()
        self.beginTime = self.constantBeginTime
        self.enableDisableResend()
        self.startResendTimer()
    }
    
    private func startResendTimer() {
        self.constantBeginTime += 15
        self.resendTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    @objc private func updateTimer() {
        self.beginTime -= 1
        self.resendTimerLabel.text = String(self.beginTime)
        if beginTime == 0 {
            self.resendTimerLabel.text = ""
            self.resendTimer.invalidate()
            self.enableDisableResend(true)
        }
    }
    
    private func generateOTP() {
        self.onBoardingVM.generateOTP(msisdn: self.otpReciever!) { (response1, message1) in
            if response1 {
                StatusBarNotificationBanner(title: DefaultMessages.OTP_SENT.rawValue.localized, style: .success).show()
                self.otpReference = self.onBoardingVM.generateOTPResponse.otpReferenceId!
            } else {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                vc.errorMessage = message1
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    private func checkUser() {
        self.onBoardingVM.checkUser(msisdn: otpReciever!) { (response, mesasge) in
            if(response){
                self.isExistingUser = true
            } else {
                if mesasge == "User not registered." {
                    self.isExistingUser = false
                }
            }
        }
    }
    
    func enableDisableResend(_ shouldEnable: Bool = false) {
        if shouldEnable {
            self.resendButton.isEnabled = true
            self.resendButton.alpha = 1
            self.resendTimerLabel.alpha = 1
        } else {
            self.resendButton.isEnabled = false
            self.resendButton.alpha = 0.5
            self.resendTimerLabel.alpha = 0.5
        }
    }
    
    func enableDisableNextButton(_ shouldEnable: Bool = false) {
        if shouldEnable {
            self.nextButton.isEnabled = true
            self.nextButton.alpha = 1
        } else {
            self.nextButton.isEnabled = false
            self.nextButton.alpha = 0.5
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        self.navigationItem.title = " "
    }
}

extension VerifyOTPVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.tf_otp {
            self.pinUnderLine.backgroundColor = UIColor.keplerTextUnderLine
        } else {
            self.pinUnderLine.backgroundColor = UIColor.keplerLightGray
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var pinLength = self.tf_otp.text!.count
        if textField == self.tf_otp {
            if string != "" {
                pinLength = self.tf_otp.text!.count + 1
            } else {
                pinLength = self.tf_otp.text!.count - 1
            }
        }
        
        if !tf_otp.text!.isEmpty {
            if pinLength >= 6 {
                self.enableDisableNextButton(true)
            } else {
                self.enableDisableNextButton()
            }
        }
        
        return true
    }
}
