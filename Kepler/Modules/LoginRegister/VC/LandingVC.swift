//
//  LandingVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 23/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class LandingVC: UIViewController {

    @IBOutlet weak var languageChangeButton: CustomUIButton!
    @IBOutlet weak var registerButton: CustomUIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func languageClicked(_ sender: Any) {
        if SessionManager.getString(forKey: .DEFAULT_LANGUAGE)!.elementsEqual("en") {
            SessionManager.setValue("bn", forKey: .DEFAULT_LANGUAGE)
            self.loadView()

        } else {
            SessionManager.setValue("en", forKey: .DEFAULT_LANGUAGE)
            self.loadView()
        }
    }
    
    @IBAction func registerClicked(_ sender: Any) {
        
        if let vc = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: RegisterVC.identifier) as? RegisterVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        self.navigationItem.title = " "
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}
