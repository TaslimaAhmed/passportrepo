//
//  NIDPictureVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 21/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import AVFoundation
import SVProgressHUD

class NIDPictureVC: UIViewController {
    
    @IBOutlet weak var label1: CustomLabel!
    @IBOutlet weak var label2: CustomLabel!
    @IBOutlet weak var label3: CustomLabel!
    
    @IBOutlet weak var captureButton: CustomUIButton!
    @IBOutlet weak var retryStack: UIStackView!
    @IBOutlet weak var confirmStack: UIStackView!
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var previewImage: UIImageView!
    
    var navController: UINavigationController!
    
    var captureSession = AVCaptureSession()
    var sessionOutput = AVCaptureStillImageOutput()
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    var fronImageData: Data!
    var backImageData: Data!
    var isFrontImage: Bool = true
    
    var loginVM = LoginRegistrationVM()
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setupView()
    }
    
    func resize(image: Data) -> Data? {
        // The image returned in initialImageData will be larger than what
        //  is shown in the AVCaptureVideoPreviewLayer, so we need to crop it.
        let image : UIImage = UIImage(data: image)!
        
        let originalSize : CGSize
        let visibleLayerFrame = self.cameraView!.bounds // THE ACTUAL VISIBLE AREA IN THE LAYER FRAME
        
        // Calculate the fractional size that is shown in the preview
        let metaRect : CGRect = self.previewLayer.metadataOutputRectConverted(fromLayerRect: visibleLayerFrame)
        if (image.imageOrientation == UIImage.Orientation.left || image.imageOrientation == UIImage.Orientation.right) {
            // For these images (which are portrait), swap the size of the
            // image, because here the output image is actually rotated
            // relative to what you see on screen.
            originalSize = CGSize(width: image.size.height, height: image.size.width)
        }
        else {
            originalSize = image.size
        }
        
        // metaRect is fractional, that's why we multiply here.
        let cropRect : CGRect = CGRect(x: metaRect.origin.x * originalSize.width,
                                       y: metaRect.origin.y * originalSize.height,
                                       width: metaRect.size.width * originalSize.width,
                                       height: metaRect.size.height * originalSize.height).integral
        
        let finalImage : UIImage = UIImage(cgImage: image.cgImage!.cropping(to: cropRect)!,
                                           scale:1,
                                           orientation: image.imageOrientation)
        
        return finalImage.jpegData(compressionQuality: 1)
    }
    
    private func setupView() {
        self.retryStack.isHidden = true
        self.confirmStack.isHidden = true
        self.previewImage.isHidden = true
        self.label2.isHidden = false
        
        self.label1.attributedText = NSMutableAttributedString()
            .normal("Step 1: Take a photo of the ".localized, 12)
            .extraBold("front portion ".localized, 12)
            .normal("of your NID card".localized, 12)
        self.label3.attributedText = NSMutableAttributedString().bold("Place your NID card within the frame".localized, 12)
        
        let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)!
        do {
            let input = try AVCaptureDeviceInput(device: device)
            
            if captureSession.canAddInput(input) {
                captureSession.addInput(input)
                sessionOutput.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                
                if captureSession.canAddOutput(sessionOutput) {
                    captureSession.addOutput(sessionOutput)
                    captureSession.startRunning()
                    
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                    previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                    
                    self.cameraView.layer.addSublayer(previewLayer)
                    
                    previewLayer.frame = self.cameraView.layer.frame
                    previewLayer.position = CGPoint(x: self.cameraView.frame.width / 2, y: self.cameraView.frame.height / 2)
                }
            }
            
        } catch {
            DLog(error.localizedDescription)
        }
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        if isFrontImage {
            if let videoConnection = sessionOutput.connection(with: .video) {
                sessionOutput.captureStillImageAsynchronously(from: videoConnection) { (buffer, error) in
                    self.fronImageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer!, previewPhotoSampleBuffer: .none)
                    self.fronImageData = self.resize(image: self.fronImageData!)
                    
                    self.retryStack.isHidden = false
                    self.confirmStack.isHidden = false
                    self.captureButton.isHidden = true
                    
                    self.previewImage.isHidden = false
                    self.previewImage.image = UIImage(data: self.fronImageData!)
                    
                    self.label2.isHidden = true
                    self.label3.attributedText = NSMutableAttributedString().normal("Make sure your NID details are clear to read and are not blurry.".localized, 12)
                }
            }
        } else {
            if let videoConnection = sessionOutput.connection(with: .video) {
                sessionOutput.captureStillImageAsynchronously(from: videoConnection) { (buffer, error) in
                    self.backImageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer!, previewPhotoSampleBuffer: .none)
                    self.backImageData = self.resize(image: self.backImageData!)
                    
                    self.retryStack.isHidden = false
                    self.confirmStack.isHidden = false
                    self.captureButton.isHidden = true
                    
                    self.previewImage.isHidden = false
                    self.previewImage.image = UIImage(data: self.backImageData!)
                    
                    self.label2.isHidden = true
                    self.label3.attributedText = NSMutableAttributedString().normal("Make sure your NID details are clear to read and are not blurry.".localized, 12)
                    
                    //                    UIImageWriteToSavedPhotosAlbum(UIImage(data: fronImageData!)!, nil, nil, nil)
                }
            }
        }
    }
    
    @IBAction func confirmClicked(_ sender: Any) {
        if isFrontImage {
            self.captureButton.isHidden = false
            self.retryStack.isHidden = true
            self.confirmStack.isHidden = true
            self.previewImage.isHidden = true
            self.label2.isHidden = false
            
            self.isFrontImage = false
            self.label1.attributedText = NSMutableAttributedString()
                .normal("Step 2: Take a photo of the ".localized, 12)
                .extraBold("back portion ".localized, 12)
                .normal("of your NID card".localized, 12)
            self.label2.isHidden = false
            self.label3.attributedText = NSMutableAttributedString().bold("Place your NID card within the frame".localized, 12)
        } else {
            SVProgressHUD.show(withStatus: "Getting NID information")
            self.loginVM.getNIDData(frontImage: self.fronImageData, backImage: self.backImageData) { (response, message, responseData) in
                SVProgressHUD.dismiss()
                if response {
                    DLog(message)
                    let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: NIDInformationVC.identifier) as! NIDInformationVC
                    vc.nidData = responseData
                    vc.navController = self.navController
                    self.navController.popViewController(animated: false)
                    self.navController.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: ErrorVC.identifier) as! ErrorVC
                    vc.errorMessage = message
                    self.present(vc, animated: true) {
                        self.navController.popViewController(animated: false)
                    }
                }
            }
            
            DLog(self.fronImageData)
            DLog(self.backImageData)
        }
    }
    
    @IBAction func retryClicked(_ sender: Any) {
        self.captureButton.isHidden = false
        self.retryStack.isHidden = true
        self.confirmStack.isHidden = true
        self.previewImage.isHidden = true
        self.label2.isHidden = false
        self.label3.attributedText = NSMutableAttributedString().bold("Place your NID card within the frame".localized, 12)
        
        if isFrontImage {
            self.fronImageData = nil
        } else {
            self.backImageData = nil
        }
    }
}
