//
//  VerificationFailedPopupVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 26/11/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class VerificationFailedPopupVC: UIViewController {

    var onPopupDismissDelegate: OnPopupDismissDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registrationClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.onPopupDismissDelegate.onPopupdismissClicked()
    }
}
