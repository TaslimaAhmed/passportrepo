//
//  VerificationSuccessfullVC.swift
//  Kepler
//
//  Created by Mushfiqur Rahman on 29/12/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class VerificationSuccessfullVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        initHomeView()
    }
}
