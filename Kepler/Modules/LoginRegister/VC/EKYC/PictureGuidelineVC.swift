//
//  PictureGuidelineVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 10/8/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class PictureGuidelineVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: NIDPictureVC.identifier) as! NIDPictureVC
        vc.navController = self.navigationController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
