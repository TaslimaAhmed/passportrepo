//
//  ProfilePictureVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 10/8/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import AVFoundation
import SVProgressHUD

class ProfilePictureVC: UIViewController {
    
    @IBOutlet weak var label1: CustomLabel!
    @IBOutlet weak var label2: CustomLabel!
    @IBOutlet weak var label3: CustomLabel!
    
    @IBOutlet weak var captureButton: CustomUIButton!
    @IBOutlet weak var retryStack: UIStackView!
    @IBOutlet weak var confirmStack: UIStackView!
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var previewImage: UIImageView!
    
    @IBOutlet weak var pictureBorderView: UIView!

    var captureSession = AVCaptureSession()
    var sessionOutput = AVCaptureStillImageOutput()
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    var userData: NIDData!
    var selfiImageData: Data!
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupView()
        
        self.pictureBorderView.cornerRadius = self.pictureBorderView.bounds.width / 2
        
        let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)!
        do {
            let input = try AVCaptureDeviceInput(device: device)
            
            if captureSession.canAddInput(input) {
                captureSession.addInput(input)
                sessionOutput.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                
                if captureSession.canAddOutput(sessionOutput) {
                    captureSession.addOutput(sessionOutput)
                    captureSession.startRunning()
                    
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                    previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                    
                    self.cameraView.layer.addSublayer(previewLayer)
                    
                    previewLayer.frame = self.cameraView.layer.frame
                    previewLayer.position = CGPoint(x: self.cameraView.frame.width / 2, y: self.cameraView.frame.height / 2)
                }
            }
            
        } catch {
            DLog(error.localizedDescription)
        }
    }
    
    private func setupView() {
        self.retryStack.isHidden = true
        self.confirmStack.isHidden = true
        self.previewImage.isHidden = true
    }
    
    func resize(image: Data) -> Data? {
        // The image returned in initialImageData will be larger than what
        //  is shown in the AVCaptureVideoPreviewLayer, so we need to crop it.
        let image : UIImage = UIImage(data: image)!
        
        let originalSize : CGSize
        let visibleLayerFrame = self.cameraView!.bounds // THE ACTUAL VISIBLE AREA IN THE LAYER FRAME
        
        // Calculate the fractional size that is shown in the preview
        let metaRect : CGRect = self.previewLayer.metadataOutputRectConverted(fromLayerRect: visibleLayerFrame)
        if (image.imageOrientation == UIImage.Orientation.left || image.imageOrientation == UIImage.Orientation.right) {
            // For these images (which are portrait), swap the size of the
            // image, because here the output image is actually rotated
            // relative to what you see on screen.
            originalSize = CGSize(width: image.size.height, height: image.size.width)
        }
        else {
            originalSize = image.size
        }
        
        // metaRect is fractional, that's why we multiply here.
        let cropRect : CGRect = CGRect(x: metaRect.origin.x * originalSize.width,
                                       y: metaRect.origin.y * originalSize.height,
                                       width: metaRect.size.width * originalSize.width,
                                       height: metaRect.size.height * originalSize.height).integral
        
        let finalImage : UIImage = UIImage(cgImage: image.cgImage!.cropping(to: cropRect)!,
                                           scale:1,
                                           orientation: .leftMirrored)
        
        return finalImage.jpegData(compressionQuality: 1)
    }
    
    
    @IBAction func takePhoto(_ sender: Any) {
        if let videoConnection = sessionOutput.connection(with: .video) {
            sessionOutput.captureStillImageAsynchronously(from: videoConnection) { (buffer, error) in
                self.selfiImageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer!, previewPhotoSampleBuffer: .none)
                self.selfiImageData = self.resize(image: self.selfiImageData!)
                
                self.retryStack.isHidden = false
                self.confirmStack.isHidden = false
                self.captureButton.isHidden = true
                
                self.previewImage.isHidden = false
                self.previewImage.image = UIImage(data: self.selfiImageData!)
                
                self.label2.isHidden = true
                self.label3.attributedText = NSMutableAttributedString().normal("Position your face within the frame".localized, 12)
            }
        }
    }
    
    @IBAction func confirmClicked(_ sender: Any) {
        LoginRegistrationVM.ekycVerify(userData: self.userData, applicantPhoto: self.selfiImageData) { (isSuccess, message) in
            if isSuccess {
                SessionManager.setValue(EkycUserStatus.PENDING.rawValue, forKey: .EKYC_STATUS)
                let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: VerifyingVC.identifier) as! VerifyingVC
                let window = UIApplication.shared.keyWindow!
                window.rootViewController = vc
                
                UIView.transition(with: window, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve) {
                    window.makeKeyAndVisible()
                }
            } else {
                self.showToast(message: message)
                self.retryAgain()
            }
        }
    }
    
    @IBAction func retryClicked(_ sender: Any) {
        self.retryAgain()
    }
    
    private func retryAgain() {
        self.captureButton.isHidden = false
        self.retryStack.isHidden = true
        self.confirmStack.isHidden = true
        self.previewImage.isHidden = true
        self.label2.isHidden = false
        
        self.navigationItem.title = "Profile Image".localized
        self.selfiImageData = nil
    }
}
