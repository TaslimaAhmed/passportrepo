//
//  VerifyingVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 19/11/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class VerifyingVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if SessionManager.getBool(forKey: .IS_LOGGED_IN)! {
            initHomeView()
        } else {
            initOnboardingView()
        }
    }
}
