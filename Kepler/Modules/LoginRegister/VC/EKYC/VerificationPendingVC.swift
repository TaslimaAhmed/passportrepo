//
//  VerificationPendingVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 26/11/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class VerificationPendingVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
