//
//  NIDInformationVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 25/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class NIDInformationVC: UIViewController {

    @IBOutlet weak var nameBangla: CustomUITextField!
    @IBOutlet weak var nameEnglish: CustomUITextField!
    @IBOutlet weak var fathersName: CustomUITextField!
    @IBOutlet weak var mothersName: CustomUITextField!
    @IBOutlet weak var spouseName: CustomUITextField!
    @IBOutlet weak var dateOfBirth: CustomUITextField!
    @IBOutlet weak var nidNo: CustomUITextField!
    @IBOutlet weak var address: CustomUITextView!
    @IBOutlet weak var nomineeName: CustomUITextField!
    @IBOutlet weak var relationWitNominee: CustomUITextField!
    
    var navController: UINavigationController!
    
    var gender: String = "M"
    var nidData: NIDData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.loadData()
    }
    
    private func setupView() {
        
    }
    
    private func loadData() {
        self.nameBangla.text = self.nidData.applicant_name_ben!
        self.nameEnglish.text = self.nidData.applicant_name_eng!
        self.fathersName.text = self.nidData.father_name!
        self.mothersName.text = self.nidData.mother_name!
        self.spouseName.text = self.nidData.spouse_name!
        self.dateOfBirth.text = self.nidData.dob!.replacingOccurrences(of: "-", with: "/")
        self.nidNo.text = self.nidData.nid_no!
        self.address.text = self.nidData.address!
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        if !self.nameBangla.text!.isEmpty && !self.nameEnglish.text!.isEmpty && !self.fathersName.text!.isEmpty &&
            !self.mothersName.text!.isEmpty && !self.spouseName.text!.isEmpty && !self.dateOfBirth.text!.isEmpty &&
            !self.nidNo.text!.isEmpty && !self.address.text!.isEmpty && !self.nomineeName.text!.isEmpty &&
            !self.relationWitNominee.text!.isEmpty {
            
            self.nidData.applicant_name_ben = self.nameBangla.text
            self.nidData.father_name = self.fathersName.text
            self.nidData.mother_name = self.mothersName.text
            self.nidData.spouse_name = self.spouseName.text
            self.nidData.address = self.address.text
            self.nidData.dob = self.nidData.dob!
            self.nidData.gender = self.gender
            self.nidData.nomineeName = self.nomineeName.text
            self.nidData.nomineeeRelation = self.relationWitNominee.text
            
            let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: ProfilePictureVC.identifier) as! ProfilePictureVC
            vc.userData = self.nidData
            self.navController.pushViewController(vc, animated: true)
        } else {
            self.showToast(message: .FILL_ALL_FIELDS)
        }
    }
    
    @IBAction func rescanClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
