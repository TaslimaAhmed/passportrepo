//
//  RegisterVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 9/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension RegisterVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.phoneNumber {
            self.phoneNumberUnderLine.backgroundColor = UIColor.keplerTextUnderLine
        } else {
            self.phoneNumberUnderLine.backgroundColor = UIColor.keplerLightGray
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // For changing the length when there is no character or there is some character
        if string == "" && range.location == 0 {
            self.phoneNumber.maxLength = 25
        } else {
            self.phoneNumber.maxLength = 11
        }
        
        var phoneLength = self.phoneNumber.text!.count
        if textField == self.phoneNumber {
            if string != "" {
                phoneLength = self.phoneNumber.text!.count + 1
            } else {
                phoneLength = self.phoneNumber.text!.count - 1
            }
        }
        
        let phnNumber = textField.text! + "" + string
        let tempOperatorName = getMobileOperator(phoneNumber: phnNumber)
        operatorName.text = tempOperatorName
        SessionManager.setValue(tempOperatorName, forKey: .MOBILE_OPERATOR_TYPE)
        
        if !phoneNumber.text!.isEmpty && !operatorName.text!.isEmpty {
            if phoneLength >= 11 {
                self.enableDisableNextButton(true)
            } else {
                self.enableDisableNextButton()
            }
        }
        
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        self.navigationItem.title = " "
    }
}

extension RegisterVC: MobileOperatorSelectionDelegeate {
    func onOperatorSelect(operatorName: String, operatorImage: UIImage?) {
        SessionManager.setValue(operatorName, forKey: .MOBILE_OPERATOR_TYPE)
        self.operatorName.text = operatorName
    }
}
