//
//  SetupPinViewController.swift
//  Kepler
//
//  Created by Mufad Monwar on 4/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class SetupPinVC: UIViewController {
    
    @IBOutlet weak var userPIN: UITextField!
    @IBOutlet weak var userConfirmPIN: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    var userData: [String: AnyObject]!
    
    let loginRegistrationVM = LoginRegistrationVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViews()
    }
    
    private func setupViews() {
        enableDisableNextButton(button: self.nextButton)
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        self.userPIN.delegate = self
        self.userConfirmPIN.delegate = self
        self.userPIN.becomeFirstResponder()
        
        userPIN.defaultTextAttributes.updateValue(28.0, forKey: NSAttributedString.Key.kern)
        userConfirmPIN.defaultTextAttributes.updateValue(28.0, forKey: NSAttributedString.Key.kern)
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        if !userPIN.text!.isEmpty && userPIN.text!.count == 4 && !userConfirmPIN.text!.isEmpty && userConfirmPIN.text!.count == 4 {
            if userPIN.text == userConfirmPIN.text {
                self.userData.merge(dict: ["pin" : self.userPIN.text!.toBase64()] as [String : AnyObject])
                SVProgressHUD.show()
                self.loginRegistrationVM.registerUser(userData: self.userData) { (success, message) in
                    SVProgressHUD.dismiss()
                    if success {
                        SessionManager.setValue(self.userData["mobileNumber"], forKey: SessionManagerKey.USER_MSISDN)
                        let vc = UIStoryboard(storyboard: .ekyc).instantiateViewController(withIdentifier: PictureGuidelineVC.identifier) as! PictureGuidelineVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        self.showToast(message: message)
                    }
                }
            } else {
                self.showToast(message: .PIN_ERROR)
            }
        } else {
            self.showToast(message: .PIN_REQUEST)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        self.navigationItem.title = " "
    }
}

extension SetupPinVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var pinLength = self.userPIN.text!.count
        var confirmPinLength = self.userConfirmPIN.text!.count
        if textField == self.userPIN {
            if string != "" {
                pinLength = self.userPIN.text!.count + 1
            } else {
                pinLength = self.userPIN.text!.count - 1
            }
        }
        if textField == self.userConfirmPIN {
            if string != "" {
                confirmPinLength = self.userConfirmPIN.text!.count + 1
            } else {
                confirmPinLength = self.userConfirmPIN.text!.count - 1
            }
        }
        
        if !userPIN.text!.isEmpty || !userConfirmPIN.text!.isEmpty {
            if pinLength >= 4 && confirmPinLength >= 4 {
                enableDisableNextButton(button: self.nextButton, true)
            } else {
                enableDisableNextButton(button: self.nextButton)
            }
        }
        
        return true
    }
}
