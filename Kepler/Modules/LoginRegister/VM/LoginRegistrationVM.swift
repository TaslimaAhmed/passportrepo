//
//  OnBoardingVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 9/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

class LoginRegistrationVM {
    //MARK: OTP generation response model data
    var generateOTPResponse: GenerateOTPResponse!
    var verifyOTP: VerifyOTPResponse!
    static var loginResponse: LoginResponse!
    
    func checkUser(msisdn: String, response: @escaping(_ success: Bool, _ message: String)->Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.checkUser + "?msisdn=" + msisdn, params: nil, headers: nil, success: { (responseData) in
            SVProgressHUD.dismiss()
            let userStatusResponse = UserStatusResponse(JSONString: responseData)
            if userStatusResponse!.userStatus! {
                response(true, "User already exists.")
            } else {
                response(false, "User not registered.")
            }
        }) {(error) in
            response(false, error)
            DLog(error)
        }
    }
    
    func generateOTP(msisdn: String, response: @escaping(_ success: Bool, _ message: String)->Void) {
        let params = [
            "serviceName" : "REGISTRATION",
            "deviceType": "IOS",
            "receiverInfo" : msisdn
        ] as [String : AnyObject]
        
        AlamofireWrapper.requestPOSTURL(HttpURLs.generateOTP, params: params, headers: nil, success: { (responseData) in
            self.generateOTPResponse = GenerateOTPResponse(JSONString: responseData)
            if let message = self.generateOTPResponse!.message {
                if message.contains("Success") {
                    response(true, "Message")
                } else {
                    response(false, self.generateOTPResponse.message!)
                }
            } else {
                response(false, "Error")
            }
        }) { (error) in
            response(false, error)
            DLog(error)
        }
    }
    
    func validateOTP(mssidn : String, otp : String, otpReference : String, response: @escaping(_ success: Bool, _ message: String)->Void) {
        let params = [
            "otp" : otp,
            "otpReferenceId": otpReference,
            "receiverInfo" : mssidn
        ] as [String : AnyObject]
        
        AlamofireWrapper.requestPOSTURL(HttpURLs.verifyOTP, params: params, headers: nil, success: { (responseData) in
            self.verifyOTP = VerifyOTPResponse(JSONString: responseData)
            
            if self.verifyOTP!.message! == "Success!" {
                response(true, DefaultMessages.SUCCESS_OTP.rawValue.localized)
            } else {
                response(false, self.verifyOTP!.message!)
            }
        }) { (error) in
            response(false, error)
            DLog(error)
        }
    }
    
    //For upload NID image to server
    func getNIDData(frontImage: Data, backImage: Data, response: @escaping(_ success: Bool, _ message: String, _ responseData: NIDData?)->Void) {
        AF.upload(multipartFormData: { multiPart in
            
            multiPart.append(frontImage, withName: "id_front", fileName: "frontImage.jpeg", mimeType: "image/jpeg")
            multiPart.append(backImage, withName: "id_back", fileName: "backImage.jpeg", mimeType: "image/jpeg")
            
        }, to: HttpURLs.nidUpload, method: .post).uploadProgress(queue: .main, closure: { progress in
            DLog("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON { responseData in
            switch responseData.result {
            case .success(let responseString):
                let nidResponse = NIDResponse (JSON: responseString as! [String : Any])
                DLog(nidResponse)
                if let responseStatus = nidResponse!.status, responseStatus.lowercased().elementsEqual("success") {
                    response(true, "NID Scan Successfull.", nidResponse?.data)
                } else {
                    response(false, nidResponse!.message!, nil)
                }
            case .failure(let error):
                response(false, error.localizedDescription, nil)
            }
        }
    }
    
    func registerUser(userData: [String : AnyObject], complition: @escaping(_ success: Bool, _ message: String)->Void) {
        AlamofireWrapper.requestPOSTURL(HttpURLs.asyncRegistration, params: userData, headers: AlamofireWrapper().defaultHeader) { (respose) in
            DLog(respose)
            complition(true, "Success")
        } failure: { (error) in
            DLog(error)
            complition(false, error)
        }
    }
    
    class func ekycVerify(userData: NIDData, applicantPhoto: Data, response: @escaping(_ success: Bool, _ message: String)->Void) {
        SVProgressHUD.show()
        let param = [
            "nid_no" : userData.nid_no,
            "dob" : userData.dob?.replacingOccurrences(of: "-", with: "/"),
            "applicant_name_ben" : userData.applicant_name_ben,
            "applicant_name_eng" : userData.applicant_name_eng,
            "father_name" : userData.father_name,
            "mother_name" : userData.mother_name,
            "spouse_name" : userData.spouse_name,
            "pres_address" : userData.address,
            "perm_address" : userData.address,
            "id_front_name" : userData.id_front_name,
            "id_back_name" : userData.id_back_name,
            "gender" : userData.gender,
            "profession" : "",
            "nominee" : userData.nomineeName,
            "nominee_relation" : userData.nomineeeRelation,
            "mobile_number" : SessionManager.getString(forKey: .USER_MSISDN),
        ] as [String : AnyObject]
        
        AF.upload(multipartFormData: { multiPart in
            multiPart.append(applicantPhoto, withName: "applicant_photo", fileName: "applicant_photo.jpeg", mimeType: "image/jpeg")
            
            param.forEach { (key, value) in
                multiPart.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        }, to: HttpURLs.ekyc_verify, method: .post).uploadProgress(queue: .main, closure: { progress in
            DLog("Upload Progress: \(progress.fractionCompleted)")
        }).responseString { responseData in
            SVProgressHUD.dismiss()
            switch responseData.result {
            case .success(let responseString):
                let ekyvVerifyResponse = EkycVerifyResponse (JSONString: responseString)
                if ekyvVerifyResponse!.status == "success" {
                    response(true, ekyvVerifyResponse!.message!)
                } else {
                    response(false, ekyvVerifyResponse!.message!)
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                DLog(error.localizedDescription)
                response(false, error.localizedDescription)
            }
        }
    }
    
    class func ekycRefresh(response: @escaping(_ success: Bool)->Void) {
        SVProgressHUD.show()
        AlamofireWrapper.requestPOSTURL(HttpURLs.ekyc_refresh, params: nil, headers: AlamofireWrapper().defaultHeader) { (responseData) in
            SVProgressHUD.dismiss()
            let ekycRefreshResponse = EkycRefreshResponse (JSONString: responseData)
            if let data = ekycRefreshResponse {
                if data.gigatechStatus!.lowercased().elementsEqual("not_found") && data.tblStatus == nil {
                    
                    SessionManager.setValue(EkycUserStatus.INCOMPLETE.rawValue, forKey: .EKYC_STATUS)
                } else if (data.gigatechStatus!.lowercased().elementsEqual("success") && data.tblStatus == nil) ||
                            (data.gigatechStatus!.lowercased().elementsEqual("pending") && data.tblStatus == nil) {
                    
                    SessionManager.setValue(EkycUserStatus.PENDING.rawValue, forKey: .EKYC_STATUS)
                } else if (data.gigatechStatus!.lowercased().elementsEqual("failed")) || (data.gigatechStatus!.lowercased().elementsEqual("invalid")) ||
                            (data.gigatechStatus!.lowercased().elementsEqual("success") && data.tblStatus!.lowercased().elementsEqual("failed")) {
                    
                    SessionManager.setValue(EkycUserStatus.FAILED.rawValue, forKey: .EKYC_STATUS)
                } else if data.gigatechStatus!.lowercased().elementsEqual("success") && data.tblStatus!.lowercased().elementsEqual("success") {
                    
                    SessionManager.setValue(EkycUserStatus.SUCCESS.rawValue, forKey: .EKYC_STATUS)
                }
                response(true)
            }
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            response(false)
        }
    }
    
    func loginUser(msisdn: String, pin: String, response: @escaping(_ success: Bool, _ message: String)->Void) {
        let params = [
            "msisdn" : msisdn,
            "pin" : pin.toBase64()
        ] as [String : AnyObject]
        
        AlamofireWrapper.requestPOSTURL(HttpURLs.loginUser, params: params, success: { (responseData) in
            let loginResponse = LoginResponse (JSONString: responseData)
            LoginRegistrationVM.loginResponse = LoginResponse (JSONString: responseData)
            if loginResponse?.jwt != nil {
                if loginResponse!.userLoginInfo!.ekycComplete == true && loginResponse!.userLoginInfo!.userStatus!.elementsEqual("Active") {
                    SessionManager.setValue(EkycUserStatus.SUCCESS.rawValue, forKey: .EKYC_STATUS)
                } else if loginResponse!.userLoginInfo!.ekycComplete == false {
                    if(loginResponse!.userLoginInfo!.userStatus!.elementsEqual("Active")) {
                        SessionManager.setValue(EkycUserType.OLD_USER.rawValue, forKey: .EKYC_USER_TYPE)
                    } else if(loginResponse!.userLoginInfo!.userStatus!.elementsEqual("Authorized")) {
                        SessionManager.setValue(EkycUserType.NEW_USER.rawValue, forKey: .EKYC_USER_TYPE)
                    } else {
                        SessionManager.setValue(EkycUserType.NEW_USER.rawValue, forKey: .EKYC_USER_TYPE)
                    }
                }
                
                SessionManager.setValue(msisdn, forKey: .USER_MSISDN)
                SessionManager.setValue("Bearer " + loginResponse!.jwt!, forKey: .ACCESS_TOKEN)
                SessionManager.setValue(loginResponse?.userLoginInfo?.userName, forKey: .USER_NAME)
                SessionManager.setValue(loginResponse?.userLoginInfo?.balance, forKey: .AVAILABLE_BALANCE)
                SessionManager.setValue(true, forKey: SessionManagerKey.IS_LOGGED_IN)
                
                response(true, "Login successfull.")
            } else {
                response(false, "Login failed.")
            }
        }) { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            response(false, error)
        }
    }
}
