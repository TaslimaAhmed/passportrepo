//
//  CashOutVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 28/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension CashOutVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.recentContacts != nil {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.recentContacts != nil {
            if section == 0 {
                return recentContacts.count
            } else {
                return contacts.count
            }
        } else {
            return contacts.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.recentContacts != nil {
            if section == 0 {
                return "Recent Contacts".localized
            } else {
                return "All Contacts".localized
            }
        } else {
            return "All Contacts".localized
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40)
        let myLabel = UILabel()
        headerView.addSubview(myLabel)
        myLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            myLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 25),
            myLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor)
        ])
        myLabel.font = UIFont(name: FontList.ExtraBold.rawValue, size: 16)
        myLabel.attributedText = NSMutableAttributedString().extraBold(self.tableView(tableView, titleForHeaderInSection: section)!, 16)
        headerView.backgroundColor = UIColor.keplerWhite
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if recentContacts != nil {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListTableViewCell", for: indexPath) as! ContactListTableViewCell
                cell.setupCell(contactData: self.recentContacts[indexPath.row])
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListTableViewCell", for: indexPath) as! ContactListTableViewCell
                cell.setupCell(contactData: self.contacts[indexPath.row])
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListTableViewCell", for: indexPath) as! ContactListTableViewCell
            cell.setupCell(contactData: self.contacts[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if recentContacts != nil {
            if indexPath.section == 0 {
                self.phoneNumberField.text = self.recentContacts[indexPath.row].number.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: "88", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ".", with: "")
            } else {
                self.phoneNumberField.text = self.contacts[indexPath.row].number.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: "88", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ".", with: "")
            }
        } else {
            self.phoneNumberField.text = self.contacts[indexPath.row].number.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: "88", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ".", with: "")
        }
        self.gotoCashoutAmountEntryPage()
    }
}

extension CashOutVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            return false
        }
        if textField == self.phoneNumberField {
            var searchString = self.phoneNumberField.text! + "" + string
            if string == "" {
                searchString.removeLast()
            }
            if searchString.isNumeric {
                self.contacts = self.searchedContacts.filter{$0.number.lowercased().contains(String(searchString).lowercased())}
            } else {
                self.contacts = self.searchedContacts.filter{$0.name.lowercased().contains(searchString.lowercased())}
            }
        
            if string.isEmpty && range.location == 0 {
                self.contacts = self.searchedContacts
                self.recentContacts = self.recentTempContacts
                self.allContactList.reloadData()
            } else {
                self.recentContacts = nil
                self.allContactList.reloadData()
            }
        }
        
        return true
    }
}

extension CashOutVC: OnQRCodeScannedDelegate {
    func onQRCodeScanned(code: String) {
        self.phoneNumberField.text = code
        self.gotoCashoutAmountEntryPage()
    }
}
