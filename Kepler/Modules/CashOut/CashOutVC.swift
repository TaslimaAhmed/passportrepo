//
//  CashOutVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 28/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class CashOutVC: UIViewController {
    
    var contacts: [ContactsItem] = []
    var searchedContacts: [ContactsItem] = []
    var recentContacts: [ContactsItem]!
    var recentTempContacts: [ContactsItem]!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var phoneNumberField: CustomUITextField!
    
    @IBOutlet weak var allContactList: UITableView!
    
    @IBOutlet weak var nextButton: CustomUIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    func setupView() {
        self.nextButton.cornerRadius = self.nextButton.bounds.width/2
        self.phoneNumberField.delegate = self
        
        self.allContactList.delegate = self
        self.allContactList.dataSource = self
        
        self.loadContacts()
    }
    
    private func loadContacts() {
        self.contacts = ContactHelper.contactData
        self.searchedContacts = self.contacts
        if self.contacts.count == 0 {
            emptyMessageInTableView(self.allContactList, "No contacts found".localized)
        } else {
            emptyMessageInTableView(self.allContactList, "")
        }
    }
    
    private func loadRecentSendMoney() {
        if SessionManager.hasValue(forKey: .RECENT_CASH_OUT) {
            self.recentContacts = [ContactsItem] (JSONString: SessionManager.getString(forKey: .RECENT_CASH_OUT)!)!
            self.recentTempContacts = self.recentContacts
        }
    }
    
    func gotoCashoutAmountEntryPage() {
        let msisdn = self.phoneNumberField.text!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+88", with: "").replacingOccurrences(of: "88", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ".", with: "")
        
        if !msisdn.isEmpty && msisdn.count == 11 && msisdn.isValidPhoneNumber {
            CashOutVM.checkCashOutAgent(msisdn: msisdn) { (isSuccess, userName) in
                if isSuccess {
                    let vc = UIStoryboard(storyboard: .cashOut).instantiateViewController(withIdentifier: CashOutAmountEntryVC.identifier) as! CashOutAmountEntryVC
                    vc.msisdn = msisdn
                    vc.name = userName
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: AgentNotRegisteredVC.identifier) as! AgentNotRegisteredVC
                    self.present(vc, animated: true, completion: nil)
                    self.phoneNumberField.text = nil
                }
            }
        } else {
            self.showToast(message: .INVALID_PHONE_NUMBER)
            self.phoneNumberField.text = nil
        }
    }
    
    @IBAction func openQRScanner(_ sender: Any) {
        let vc = UIStoryboard(storyboard: .cashOut).instantiateViewController(withIdentifier: CashOutScannerVC.identifier) as! CashOutScannerVC
        vc.onQRCodeScannedDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        gotoCashoutAmountEntryPage()
        self.phoneNumberField.resignFirstResponder()
    }
}
