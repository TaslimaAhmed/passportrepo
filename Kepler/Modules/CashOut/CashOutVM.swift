//
//  CashOutVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 29/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class CashOutVM {
    class func checkCashOutAgent(msisdn: String, complition: @escaping(_ isMerchant: Bool, _ userName: String) -> Void) {
        SVProgressHUD.show()
        AlamofireWrapper.requestGETURL(HttpURLs.checkUserType + "?userNumber=\(msisdn)", params: nil, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            let merchatCheck = CheckMerchantResponse (JSONString: response)
            if merchatCheck!.userType!.lowercased().elementsEqual("paypoint") {
                complition(true, merchatCheck!.userName!)
            } else {
                complition(false, merchatCheck!.userName!)
            }
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(false, error)
        }
    }
    
    class func cashOut(pin: String, cashOutAgentNumber: String, amount: String, response: @escaping(_ success: Bool, _ message: String) -> Void) {
        let params = [
            "agentAccountNo": cashOutAgentNumber,
            "amount": Int(amount)!,
            "pin": pin
        ] as [String : AnyObject]
        AlamofireWrapper.requestPOSTURL(HttpURLs.cashOut, params: params, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            let sendMoneyResponse = SendMoneyResponse (JSONString: responseData)
            if let message = sendMoneyResponse?.message, message != "" {
                if !message.lowercased().contains("you are not authorized") {
                    response(true, message)
                } else {
                    response(false, message)
                }
            } else {
                response(false, DefaultMessages.ERROR_MESSAGE.rawValue.localized)
            }
        }) { (error) in
            DLog(error)
            response(false, error)
        }
    }
}
