//
//  ContactListTableViewCell.swift
//  Kepler
//
//  Created by Mufad Monwar on 14/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class ContactListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainUIView: UIView!
    @IBOutlet weak var tv_contact_name: CustomLabel!
    @IBOutlet weak var tv_contact_number: CustomLabel!
    @IBOutlet weak var nameLabel: CustomLabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            contentView.backgroundColor = UIColor.keplerLightBackground
        } else {
            contentView.backgroundColor = UIColor.keplerWhite
        }
    }
    
    func setupCell(contactData: ContactsItem?) {
        if let contact = contactData {
            tv_contact_name.text = contact.name
            tv_contact_number.text = contact.number
            let name = self.tv_contact_name.text!.split(separator: " ")
            if !self.tv_contact_name.text!.isEmpty {
                if name.count >= 2 {
                    self.nameLabel.text = String(name[0].first!) + " " + String(name[1].first!)
                } else if name.count == 1 {
                    self.nameLabel.text = String(name[0].first!) + " " + String(name[0].first!)
                } else {
                    self.nameLabel.text = "A A"
                }
            } else {
                self.nameLabel.text = "A A"
            }
        }
    }
}
