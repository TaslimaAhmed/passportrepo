//
//  MobileOperatorVC+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

extension MobileOperatorVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        operatorData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MobileOperatorCell", for: indexPath) as! MobileOperatorCell
        cell.setupCell(mobileOperator: operatorData[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dismiss(animated: true)
        mobileOperator = operatorData[indexPath.row].name
        self.mobileOperatorDelegate.onOperatorSelect(operatorName: mobileOperator ?? "", operatorImage: operatorData[indexPath.row].icon)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.bounds.width / 3) - 10
        return CGSize(width: size, height: size)
    }
}
