//
//  MobileOperatorVC.swift
//  Kepler
//
//  Created by Mufad Monwar on 10/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class MobileOperatorVC: UIViewController {
    var mobileOperator: String?
    
    @IBOutlet weak var cv_mobile_operators: UICollectionView!
    @IBOutlet weak var cancelView: UILabel!
    
    var mobileOperatorDelegate: MobileOperatorSelectionDelegeate!
    
    var operatorData:[MobileOperator] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        operatorData = gatherData()
        cv_mobile_operators.dataSource = self
        cv_mobile_operators.delegate = self
        
        self.cancelView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didmissView)))
    }
    
    func gatherData()-> [MobileOperator]{
        var temp:[MobileOperator] = []
        
        let op1 = MobileOperator(icon: UIImage(named: .ROBI), name: "Robi")
        let op2 = MobileOperator(icon: UIImage(named: .GP), name: "GrameenPhone")
        let op3 = MobileOperator(icon: UIImage(named: .AIRTEL), name: "Airtel")
        let op4 = MobileOperator(icon: UIImage(named: .BANGLALINK), name: "Banglalink")
        let op5 = MobileOperator(icon: UIImage(named: .TELETALK), name: "Teletalk")
        
        temp.append(op1)
        temp.append(op2)
        temp.append(op3)
        temp.append(op4)
        temp.append(op5)
        
        return temp
    }
    
    @objc fileprivate func didmissView() {
        dismiss(animated: true, completion: nil)
    }
}
