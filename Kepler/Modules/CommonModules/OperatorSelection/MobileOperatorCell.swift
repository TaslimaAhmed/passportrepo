//
//  MobileOperatorCell.swift
//  Kepler
//
//  Created by Mufad Monwar on 10/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class MobileOperatorCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var iv_mobile_operator: UIImageView!
    
    override func awakeFromNib() {
        self.mainView.layer.masksToBounds = false
        self.mainView.layer.borderColor = UIColor.keplerGray.cgColor
        self.mainView.layer.borderWidth = 0.5
        self.mainView.layer.cornerRadius = 10
    }
    
    func setupCell(mobileOperator: MobileOperator){
        iv_mobile_operator.image = mobileOperator.icon
    }
}
