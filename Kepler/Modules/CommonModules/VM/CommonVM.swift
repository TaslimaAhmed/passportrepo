//
//  CommonVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 12/1/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation

class CommonVM {
    static func checkForceUpdate(complition: @escaping(_ ifHasUpdate: Bool, _ versionNumber: String) -> Void) {
        let current = Bundle.main.appVersion
        let currentVersion = current!.components(separatedBy: ".")
        
        AlamofireWrapper.requestGETURL(HttpURLs.force_update_check, params: nil) { (response) in
            let response = ForceUpdateResponse (JSONString: response)
            let new = response?.forceUpdateVersion
            if response!.blacklistVersion!.components(separatedBy: ",").filter({$0 == current}).count == 0 { // If the current version is not enlisted to blacklist
                let newVersion = new!.components(separatedBy: ".")
                
                if Int(currentVersion[0])! < Int(newVersion[0])! {
                    complition (true, new!)
                } else if Int(currentVersion[0])! > Int(newVersion[0])! { // If first portioin of the current version is greater then the previous version then its updated
                    complition (false, current!)
                } else if Int(currentVersion[1])! < Int(newVersion[1])! {
                    complition (true, new!)
                } else if Int(currentVersion[1])! > Int(newVersion[1])! { // If second portioin of the current version is greater then the previous version then its updated
                    complition (false, current!)
                } else if Int(currentVersion[2])! < Int(newVersion[2])! {
                    complition (true, new!)
                } else {
                    complition (false, current!)
                }
            } else {
                complition (true, new!)
            }
        } failure: { (error) in
            DLog(error)
        }
    }
}
