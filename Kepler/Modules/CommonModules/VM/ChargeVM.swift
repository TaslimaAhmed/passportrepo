//
//  ChargeVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 27/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import SVProgressHUD

class ChargeVM {
    class func getCharge(fromUserNumber: String, toUserNumber: String?, transactionType: String, amount: String, complition: @escaping(_ isSuccess: Bool, _ message: String) -> Void) {
        SVProgressHUD.show()
        let params = [
            "fromUserNumber": fromUserNumber,
            "toUserNumber": toUserNumber,
            "transactionType": transactionType,
            "amount": amount
        ] as [String : AnyObject]
        
        AlamofireWrapper.requestPOSTURL(HttpURLs.checkCharge, params: params, headers: AlamofireWrapper().defaultHeader) { (response) in
            SVProgressHUD.dismiss()
            let chargeResponse = ChargeResponse (JSONString: response)
            if let amount = chargeResponse!.chargeAmount {
                complition(true, String(format: "%.2f", (amount as NSString).doubleValue))
            } else {
                complition(false, DefaultMessages.ERROR_MESSAGE.rawValue)
            }
        } failure: { (error) in
            SVProgressHUD.dismiss()
            DLog(error)
            complition(false, error)
        }
    }
}
