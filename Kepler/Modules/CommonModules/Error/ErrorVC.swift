//
//  ErrorVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 12/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class ErrorVC: UIViewController {

    @IBOutlet weak var closeButton: CustomUIButton!
    @IBOutlet weak var errorTitleLabel: CustomLabel!
    @IBOutlet weak var errorMessageLabel: CustomLabel!
    
    var errorTitle: String!
    var errorMessage: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
    }
    
    private func setupView() {
        self.closeButton.cornerRadius = self.closeButton.bounds.width / 2
        self.errorMessageLabel.text = self.errorMessage
        
        if self.errorTitle != nil {
            self.errorTitleLabel.text = self.errorTitle
        }
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
