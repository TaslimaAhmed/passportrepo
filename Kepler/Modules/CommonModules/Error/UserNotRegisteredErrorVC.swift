//
//  UserNotRegisteredErrorVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 19/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class UserNotRegisteredErrorVC: UIViewController {
    
    @IBOutlet weak var cancelButton: CustomUIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cancelButton.cornerRadius = self.cancelButton.bounds.width / 2
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
