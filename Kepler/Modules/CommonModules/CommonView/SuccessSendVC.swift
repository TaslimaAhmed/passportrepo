//
//  SuccessSendVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 27/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class SuccessSendVC: UIViewController {

    @IBOutlet weak var amountLabel: CustomLabel!
    @IBOutlet weak var recipientDetails: CustomLabel!
    
    @IBOutlet weak var sendButton: CustomUIButton!
    @IBOutlet weak var rechargeButton: CustomUIButton!
    @IBOutlet weak var addButton: CustomUIButton!
    
    @IBOutlet weak var homeButton: CustomUIButton!
    
    @IBOutlet weak var forCashOutLabel: CustomLabel!
    @IBOutlet weak var successDetailsLabel: CustomLabel!
    
    var totalAmountString: String = ""
    var additionalData: String = ""
    var name: String? = ""
    var number: String = ""
    var successDetailsMessage: String = ""
    
    var isForSendMoney = true

    override func viewDidLoad() {
        super.viewDidLoad()

        if isForSendMoney {
            self.forCashOutLabel.isHidden = true
        } else {
            self.forCashOutLabel.isHidden = false
        }
        
        self.homeButton.cornerRadius = self.homeButton.bounds.width / 2
        
        self.amountLabel.text = self.totalAmountString
        if self.additionalData != "" {
            self.recipientDetails.text = self.additionalData
        } else if name != nil {
            self.recipientDetails.text = "\(self.name!) (\(number))"
        } else {
            self.recipientDetails.text = "\(number)"
        }
        
        self.successDetailsLabel.text = self.successDetailsMessage
    }
    
    @IBAction func sendClicked(_ sender: Any) {
        self.sendButton.layer.backgroundColor = UIColor.keplerRed.cgColor
        self.sendButton.layer.borderWidth = 0
        self.sendButton.setTitleColor(UIColor.keplerWhite, for: .normal)

        self.rechargeButton.layer.borderWidth = 1
        self.rechargeButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.rechargeButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.rechargeButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        self.addButton.layer.borderWidth = 1
        self.addButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.addButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.addButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        if let vc = UIStoryboard(storyboard: .send_money).instantiateViewController(withIdentifier: SendMoneyVC.identifier) as? SendMoneyVC {
            if let rootVC = UIStoryboard(storyboard: .home).instantiateViewController(withIdentifier: MainVC.identifier) as? MainVC {
                let navVC = UINavigationController(rootViewController: rootVC)
                UIApplication.shared.keyWindow?.rootViewController = navVC
                navVC.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func rechargeClicked(_ sender: Any) {
        self.rechargeButton.layer.backgroundColor = UIColor.keplerRed.cgColor
        self.rechargeButton.layer.borderWidth = 0
        self.rechargeButton.setTitleColor(UIColor.keplerWhite, for: .normal)
        
        self.sendButton.layer.borderWidth = 1
        self.sendButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.sendButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.sendButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        self.addButton.layer.borderWidth = 1
        self.addButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.addButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.addButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        if let vc = UIStoryboard(storyboard: .mobileRecharge).instantiateViewController(withIdentifier: MobileRechargeVC.identifier) as? MobileRechargeVC {
            if let rootVC = UIStoryboard(storyboard: .home).instantiateViewController(withIdentifier: MainVC.identifier) as? MainVC {
                let navVC = UINavigationController(rootViewController: rootVC)
                UIApplication.shared.keyWindow?.rootViewController = navVC
                navVC.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func addClicked(_ sender: Any) {
        self.addButton.layer.backgroundColor = UIColor.keplerRed.cgColor
        self.addButton.layer.borderWidth = 0
        self.addButton.setTitleColor(UIColor.keplerWhite, for: .normal)
        
        self.sendButton.layer.borderWidth = 1
        self.sendButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.sendButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.sendButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        self.rechargeButton.layer.borderWidth = 1
        self.rechargeButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.rechargeButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.rechargeButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        if let vc = UIStoryboard(storyboard: .addMoney).instantiateViewController(withIdentifier: AddMoneyRootVC.identifier) as? AddMoneyRootVC {
            if let rootVC = UIStoryboard(storyboard: .home).instantiateViewController(withIdentifier: MainVC.identifier) as? MainVC {
                let navVC = UINavigationController(rootViewController: rootVC)
                UIApplication.shared.keyWindow?.rootViewController = navVC
                navVC.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func homeClicked(_ sender: Any) {
        initHomeView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        self.navigationItem.title = " "
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}
