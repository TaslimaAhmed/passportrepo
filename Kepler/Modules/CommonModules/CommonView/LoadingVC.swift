//
//  LoadingVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 10/1/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class LoadingVC: UIViewController {

    @IBOutlet weak var loadingLabel: CustomLabel!
    
    var loadingText: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadingLabel.text = self.loadingText
    }
    
    func dismisViewController() {
        self.dismiss(animated: true, completion: nil)
    }
}
