//
//  UpdateAvailableVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 12/1/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class UpdateAvailableVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func gotoUpdate(_ sender: Any) {
        if let url = URL(string: "itms-apps://itunes.apple.com/us/app/robi-intelligent-solution/id1478265922"), //MARK: Need to change this link
           UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            dismiss(animated: true, completion: nil)
        }
    }
}
