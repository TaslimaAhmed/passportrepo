//
//  NoInternetView.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/1/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class NoInternetView: UIView {
    var view: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
}

extension NoInternetView {
    func setupFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = String(describing: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
}
