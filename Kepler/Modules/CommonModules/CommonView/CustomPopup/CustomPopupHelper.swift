//
//  CustomPopupHelper.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 17/1/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import UIKit

class ShowCustomPopup {
    var noInterneTimer: Timer!
    var popupView: UIView!
    var parentView: UIView!
    
    func showPopup(parentView: UIView, popupView: UIView) {
        self.parentView = parentView
        self.popupView = popupView
        popupView.frame = CGRect(x: parentView.frame.width - popupView.frame.width - 10, y: parentView.frame.minY + 50, width: popupView.frame.width, height: popupView.frame.height)
        parentView.addSubview(popupView)
        UIView.transition(with: parentView, duration: 0.5,options: .transitionCrossDissolve) {
            parentView.addSubview(popupView)
        }
        noInterneTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(removePopup), userInfo: nil, repeats: false)
    }
    
    @objc func removePopup() {
        UIView.transition(with: parentView, duration: 0.5,options: .transitionCrossDissolve) {
            self.popupView.removeFromSuperview()
        }
    }
}
