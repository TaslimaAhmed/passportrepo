//
//  ConfirmationVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 27/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SwiftyJSON

class ConfirmationVC: UIViewController {
    
    @IBOutlet weak var sendingTextLabel: CustomLabel!
    @IBOutlet weak var confirmTextLabel: CustomLabel!
    
    @IBOutlet weak var confirmButton: CustomUIButton!
    @IBOutlet weak var closeButton: CustomUIButton!
    
    
    @IBOutlet weak var transactionDetailsView: UIView!
    @IBOutlet weak var pinInputView: UIView!
    @IBOutlet weak var topAmountLabel: CustomLabel!
    @IBOutlet weak var recipientDetailsLabel: CustomLabel!
    @IBOutlet weak var additionalDetailsLabel: CustomLabel!
    @IBOutlet weak var amountLabel: CustomLabel!
    @IBOutlet weak var chargeLabel: CustomLabel!
    @IBOutlet weak var totalLabel: CustomLabel!
    @IBOutlet weak var newBalanceLabel: CustomLabel!
    
    @IBOutlet weak var pinInputField: UITextField!
    
    
    var onConfirmClickDelegate: OnConfirmClickDelegate!
    
    var sendingLabel: String!
    var recipientName: String!, recipientNumber: String = ""
    var chargeAmount: String = "0"
    var totalAmount: String = "0"
    
    var total: String!
    var newBalance: Double!
    
    var additionalMessage: String?
    
    var isForCashOut: Bool = false
    var isForOthers: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    func enableDisableNextButton(_ shouldEnable: Bool = false) {
        if shouldEnable {
            self.confirmButton.isEnabled = true
            self.confirmButton.alpha = 1
        } else {
            self.confirmButton.isEnabled = false
            self.confirmButton.alpha = 0.5
        }
    }
    
    private func setupView() {
        self.confirmButton.cornerRadius = self.confirmButton.bounds.width / 2
        self.closeButton.cornerRadius = self.closeButton.bounds.width / 2
        
        self.topAmountLabel.text = "৳ \(self.totalAmount)"
        self.amountLabel.text = "৳ \(self.totalAmount)"
        self.chargeLabel.text = "+ ৳ \(self.chargeAmount)"
        
        self.total = String(format: "%.2f", Double(self.totalAmount)! + Double(self.chargeAmount)!).replacingOccurrences(of: ".00", with: "")
        self.totalLabel.text = "৳ " + String(self.total)
        
        self.newBalance = Double(SessionManager.getString(forKey: .AVAILABLE_BALANCE)!)! - Double(self.total)!
        self.newBalanceLabel.text = "৳ " + String(format: "%.2f", self.newBalance)
        
        self.pinInputField.delegate = self
        self.pinInputField.defaultTextAttributes.updateValue(24.0, forKey: NSAttributedString.Key.kern)
        
        if self.additionalMessage != nil {
            self.additionalDetailsLabel.isHidden = false
            self.additionalDetailsLabel.text = self.additionalMessage
        } else {
            self.additionalDetailsLabel.isHidden = true
        }
        
        if !isForOthers {
            if let name = self.recipientName, !name.isEmpty {
                self.recipientDetailsLabel.text = "\(self.recipientName!) (\(self.recipientNumber))"
            } else {
                self.recipientDetailsLabel.text = "\(self.recipientNumber)"
            }
            
            if isForCashOut {
                self.sendingTextLabel.text = "Sending for Cash Out".localized
                self.confirmTextLabel.text = "Confirm Cash Out?".localized
            }
        } else {
            self.recipientDetailsLabel.text = self.recipientName
            self.sendingTextLabel.text = self.sendingLabel
        }
    }
    
    @IBAction func confirmClicked(_ sender: Any) {
        if !self.transactionDetailsView.isHidden {
            self.transactionDetailsView.isHidden = true
            self.pinInputView.isHidden = false
            self.pinInputField.becomeFirstResponder()
            self.enableDisableNextButton()
        } else {
            if !pinInputField.text!.isEmpty {
                self.dismiss(animated: true, completion: nil)
                self.onConfirmClickDelegate.onConfirmClicked(pin: pinInputField.text!.toBase64())
            } else {
                self.showToast(message: .PIN_REQUEST)
            }
        }
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ConfirmationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var pinLength = self.pinInputField.text!.count
        if textField == self.pinInputField {
            if string != "" {
                pinLength = self.pinInputField.text!.count + 1
            } else {
                pinLength = self.pinInputField.text!.count - 1
            }
        }
        
        if !pinInputField.text!.isEmpty {
            if pinLength >= 4 {
                self.enableDisableNextButton(true)
            } else {
                self.enableDisableNextButton()
            }
        }
        
        return true
    }
}
