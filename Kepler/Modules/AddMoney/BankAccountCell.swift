//
//  BankAccountCell.swift
//  Kepler
//
//  Created by Mufad Monwar on 5/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class BankAccountCell: UITableViewCell {
    
    @IBOutlet weak var tv_bank_id: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            contentView.backgroundColor = UIColor.keplerLightBackground
        } else {
            contentView.backgroundColor = UIColor.keplerWhite
        }
    }
    
    func setupCell(account:String){
        tv_bank_id.text = account
    }
}
