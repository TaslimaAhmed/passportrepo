//
//  AddMoneyRootVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 17/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

import SVProgressHUD

class AddMoneyRootVC: UIViewController {
    
    @IBOutlet weak var tv_no_accounts: CustomLabel!
    @IBOutlet weak var list_user_accounts: UITableView!
    
    var addMoneyVM = AddMoneyVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        loadData()
    }
    
    private func setupViews(){
        list_user_accounts.delegate = self
        list_user_accounts.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        self.loadData()
    }
    
    private func loadData(){
        SVProgressHUD.show()
        self.addMoneyVM.getAccounts{(response, message) in
            SVProgressHUD.dismiss()
            
            if response {
                self.list_user_accounts.reloadData()
            }else{
                //self.showToast(message: message)
            }
        }
    }
}
