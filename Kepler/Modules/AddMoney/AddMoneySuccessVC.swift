//
//  AddMoneySuccessVC.swift
//  Kepler
//
//  Created by Mufad Monwar on 5/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class AddMoneySuccessVC: UIViewController {
    
    @IBOutlet weak var label_sent_amount: CustomLabel!
    @IBOutlet weak var label_recipent: CustomLabel!
    
    @IBOutlet weak var sendButton: CustomUIButton!
    @IBOutlet weak var rechargeButton: CustomUIButton!
    @IBOutlet weak var addButton: CustomUIButton!
    
    @IBOutlet weak var homeButton: CustomUIButton!
    
    var totalAmount: String!
    var accountNumber: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.homeButton.cornerRadius = self.homeButton.frame.width / 2
        self.label_sent_amount.text = "৳ \(self.totalAmount!) " + "has been added to your TAP from".localized
        self.label_recipent.text = self.accountNumber
    }
    
    @IBAction func sendClicked(_ sender: Any) {
        self.sendButton.layer.backgroundColor = UIColor.keplerRed.cgColor
        self.sendButton.layer.borderWidth = 0
        self.sendButton.setTitleColor(UIColor.keplerWhite, for: .normal)

        self.rechargeButton.layer.borderWidth = 1
        self.rechargeButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.rechargeButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.rechargeButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        self.addButton.layer.borderWidth = 1
        self.addButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.addButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.addButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        if let vc = UIStoryboard(storyboard: .send_money).instantiateViewController(withIdentifier: SendMoneyVC.identifier) as? SendMoneyVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func rechargeClicked(_ sender: Any) {
        self.rechargeButton.layer.backgroundColor = UIColor.keplerRed.cgColor
        self.rechargeButton.layer.borderWidth = 0
        self.rechargeButton.setTitleColor(UIColor.keplerWhite, for: .normal)
        
        self.sendButton.layer.borderWidth = 1
        self.sendButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.sendButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.sendButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        self.addButton.layer.borderWidth = 1
        self.addButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.addButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.addButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        if let vc = UIStoryboard(storyboard: .mobileRecharge).instantiateViewController(withIdentifier: MobileRechargeVC.identifier) as? MobileRechargeVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func addClicked(_ sender: Any) {
        self.addButton.layer.backgroundColor = UIColor.keplerRed.cgColor
        self.addButton.layer.borderWidth = 0
        self.addButton.setTitleColor(UIColor.keplerWhite, for: .normal)
        
        self.sendButton.layer.borderWidth = 1
        self.sendButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.sendButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.sendButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        self.rechargeButton.layer.borderWidth = 1
        self.rechargeButton.layer.borderColor = UIColor.keplerDarkGray.cgColor
        self.rechargeButton.layer.backgroundColor = UIColor.keplerWhite.cgColor
        self.rechargeButton.setTitleColor(UIColor.keplerDarkGray, for: .normal)
        
        if let vc = UIStoryboard(storyboard: .addMoney).instantiateViewController(withIdentifier: AddMoneyRootVC.identifier) as? AddMoneyRootVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func homeClicked(_ sender: Any) {
        initHomeView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        self.navigationItem.title = " "
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}
