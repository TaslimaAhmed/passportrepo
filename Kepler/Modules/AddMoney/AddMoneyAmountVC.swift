//
//  AddMoneyAmountVC.swift
//  Kepler
//
//  Created by Mufad Monwar on 6/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class AddMoneyAmountVC: UIViewController {
    
    @IBOutlet weak var label_account_number: CustomLabel!
    @IBOutlet weak var tf_amount: CustomUITextField!
    @IBOutlet weak var label_current_balance: CustomLabel!
    @IBOutlet weak var btn_confirm_amount: CustomUIButton!
    
    var account_number:String?
    var account_id:String?
    var currentBalance:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.loadCurrentBalance()
        // Do any additional setup after loading the view.
    }
    
    private func setupView(){
        self.btn_confirm_amount.cornerRadius = self.btn_confirm_amount.bounds.width/2
        label_account_number.text = account_number
        self.tf_amount.delegate = self
        self.tf_amount.becomeFirstResponder()
    }
    
    private func loadCurrentBalance() {
        HomeVM.checkBalance() { (response, message) in
            if response {
                self.label_current_balance.text = String(format: "৳ %.2f", (message as NSString).doubleValue)
                self.currentBalance = message
                SessionManager.setValue(message, forKey: .AVAILABLE_BALANCE)
            } else {
                self.showToast(message: message)
            }
        }
    }
    
    @IBAction func confirmAmount(_ sender: Any) {
        if !tf_amount.text!.isEmpty {
            if Int(tf_amount.text!.replacingOccurrences(of: "৳ ", with: "")) ?? 0 >= 10 {
                if (tf_amount.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).integerValue <= (self.label_current_balance.text!.replacingOccurrences(of: "৳ ", with: "") as NSString).integerValue{
                    let vc = UIStoryboard(storyboard: .addMoney).instantiateViewController(withIdentifier: AddMoneyConfirmationVC.identifier) as! AddMoneyConfirmationVC
                    
                    vc.accountId = self.account_id!
                    vc.accountNumber = self.account_number!
                    vc.amount = tf_amount.text!.replacingOccurrences(of: "৳ ", with: "")
                    vc.currentBalance = currentBalance
                    vc.viewController = self
                    self.present(vc, animated: true, completion: nil)
                } else {
                    self.showToast(message: .INSUFFICIENT_BALANCE)
                }
            } else {
                self.showToast(message: .MINIMUM_SENDING_AMOUNT)
            }
        } else {
            self.showToast(message: .EMPTY_AMOUNT_FIELD)
        }
    }
    
}

extension AddMoneyAmountVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !textField.text!.isEmpty && textField.text!.count >= 2 {
            if textField == self.tf_amount && !self.tf_amount.text!.isEmpty {
                self.tf_amount.text = "৳ " + self.tf_amount.text!.replacingOccurrences(of: "৳ ", with: "")
            }
        } else {
            self.tf_amount.text = "৳ "
        }
        return true
    }
}
