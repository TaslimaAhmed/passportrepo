//
//  AddMoneyVM.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 18/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

class AddMoneyVM {
    
    var existingAccounts : [AccountInfo] = []
    
    func addMoney(pin: String, serialNumber recipientAccount: String, amount: String, response: @escaping(_ success: Bool, _ message: String)->Void) {
        let params = [
            "recipientAccount" : recipientAccount,
            "pin" : pin,
            "amount" : amount
        ] as [String : AnyObject]
        
        AlamofireWrapper.requestPOSTURL(HttpURLs.addMoney, params: params, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
            let addMoneyResponse = AddMoneyResponse (JSONString: responseData)
            
            if addMoneyResponse!.message!.lowercased().contains("success") {
                response(true, DefaultMessages.SUCCESS_ADD_MONEY.rawValue.localized)
            } else {
                response(false, DefaultMessages.ERROR_MESSAGE.rawValue.localized)
            }
        }) { (error) in
            DLog(error)
            response(false, error)
        }
    }
    
    func getAccounts(response: @escaping(_ success: Bool, _ message: String)->Void){
         AlamofireWrapper.requestPOSTURL(HttpURLs.linkedAccounts, params: nil, headers: AlamofireWrapper().defaultHeader, success: { (responseData) in
             let connectedAccountResponse = ConnectedAccountsResopnse (JSONString: responseData)
             
             if connectedAccountResponse!.accountInfo!.count>0 {
                self.existingAccounts = connectedAccountResponse!.accountInfo!
                response(true,"Success")
             } else {
                 response(false, DefaultMessages.ERROR_MESSAGE.rawValue.localized)
             }
         }) { (error) in
             DLog(error)
            response(false, error)
         }
    }
}
