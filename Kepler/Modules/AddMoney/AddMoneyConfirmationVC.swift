//
//  AddMoneyConfirmationVC.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 18/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import SVProgressHUD

class AddMoneyConfirmationVC: UIViewController {
    
    var accountNumber: String = ""
    var accountId: String = ""
    var amount: String = ""
    var currentBalance: String = ""
    
    var addMoneyVM = AddMoneyVM()
    var viewController: AddMoneyAmountVC!
    
    @IBOutlet weak var label_amount_large: CustomLabel!
    
    @IBOutlet weak var label_bank_account: CustomLabel!
    @IBOutlet weak var label_current_balance: CustomLabel!
    @IBOutlet weak var lable_amount_requested: CustomLabel!
    
    @IBOutlet weak var label_new_balance: CustomLabel!
    @IBOutlet weak var btn_close: CustomUIButton!
    @IBOutlet weak var btn_confirm: CustomUIButton!
    
    @IBOutlet weak var view_pin_input: UIView!
    @IBOutlet weak var view_transaction_details: UIView!
    @IBOutlet weak var pinInputField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    func setupView() {
        self.label_amount_large.text = "৳ \(self.amount)"
        self.lable_amount_requested.text = "৳ \(self.amount)"
        self.label_current_balance.text = "৳ \(self.currentBalance)"
        
        self.btn_confirm.cornerRadius = self.btn_confirm.bounds.width / 2
        self.btn_close.cornerRadius = self.btn_close.bounds.width / 2
        self.pinInputField.delegate = self
        pinInputField.defaultTextAttributes.updateValue(24.0, forKey: NSAttributedString.Key.kern)
    }
    
    @IBAction func onConfirmClicked(_ sender: Any) {
        if !self.view_transaction_details.isHidden {
            self.view_transaction_details.isHidden = true
            self.view_pin_input.isHidden = false
            self.pinInputField.becomeFirstResponder()
            self.enableDisableNextButton()
        } else {
            if !pinInputField.text!.isEmpty {
                SVProgressHUD.show()
                self.addMoneyVM.addMoney(pin: pinInputField.text!.toBase64(), serialNumber: self.accountId, amount: self.amount) { (respose, message) in
                    SVProgressHUD.dismiss()
                    if respose {
                        NotificationHelper.showNotification(message: message)
                        self.dismiss(animated: true, completion: nil)
                        
                        let vc = UIStoryboard(storyboard: .addMoney).instantiateViewController(withIdentifier: AddMoneySuccessVC.identifier) as! AddMoneySuccessVC
                        vc.accountNumber = self.accountNumber
                        vc.totalAmount = self.amount
                        self.viewController.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        self.showToast(message: message)
                    }
                }
            } else {
                self.showToast(message: .ENTER_PIN)
            }
        }
    }
    
    @IBAction func onCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func enableDisableNextButton(_ shouldEnable: Bool = false) {
        if shouldEnable {
            self.btn_confirm.isEnabled = true
            self.btn_confirm.alpha = 1
        } else {
            self.btn_confirm.isEnabled = false
            self.btn_confirm.alpha = 0.5
        }
    }
}

extension AddMoneyConfirmationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var pinLength = self.pinInputField.text!.count
        if textField == self.pinInputField {
            if string != "" {
                pinLength = self.pinInputField.text!.count + 1
            } else {
                pinLength = self.pinInputField.text!.count - 1
            }
        }
        
        if !pinInputField.text!.isEmpty {
            if pinLength >= 4 {
                self.enableDisableNextButton(true)
            } else {
                self.enableDisableNextButton()
            }
        }
        
        return true
    }
}

