//
//  AddMoneyRootVC+extension.swift
//  Kepler
//
//  Created by Mufad Monwar on 5/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import UIKit

extension AddMoneyRootVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.addMoneyVM.existingAccounts.count != 0 {
            self.tv_no_accounts.text = "Select Your Account".localized
        }
        return addMoneyVM.existingAccounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BankAccountCell", for: indexPath) as! BankAccountCell
        cell.setupCell(account: addMoneyVM.existingAccounts[indexPath.row].maskedAccountNo!)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //add operation for opening amount vc and pass the account number
        if let viewController = UIStoryboard(storyboard: .addMoney).instantiateViewController(withIdentifier: AddMoneyAmountVC.identifier) as? AddMoneyAmountVC {
            
            viewController.account_id = String(addMoneyVM.existingAccounts[indexPath.row].serialNo!)
            viewController.account_number = addMoneyVM.existingAccounts[indexPath.row].maskedAccountNo!
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
}
