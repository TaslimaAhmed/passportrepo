//
//  TransactionTypeResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 25/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct TransactionTypeResponse : Mappable {
    var featuresConfigList : [FeaturesConfigList]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        featuresConfigList <- map["featuresConfigList"]
    }
}

struct FeaturesConfigList : Mappable {
    var featureName : String?
    var code : String?
    var minAmount : Int?
    var maxAmount : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        featureName <- map["featureName"]
        code <- map["code"]
        minAmount <- map["minAmount"]
        maxAmount <- map["maxAmount"]
    }
}
