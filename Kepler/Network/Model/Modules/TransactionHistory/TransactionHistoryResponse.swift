//
//  TransactionHistoryResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 1/7/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct TransactionHistoryResponse : Mappable {
    var statement : [Transaction]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        statement <- map["statement"]
    }
}

struct Transaction : Mappable {
    var amount : Double?
    var senderName : String?
    var receiverName : String?
    var actionType : String?
    var txCode : String?
    var transactionDate : String?
    var transactionType : String?
    var senderNumber : Double?
    var receiverNumber : Double?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        amount <- map["amount"]
        senderName <- map["senderName"]
        receiverName <- map["receiverName"]
        actionType <- map["actionType"]
        txCode <- map["txCode"]
        transactionDate <- map["transactionDate"]
        transactionType <- map["transactionType"]
        senderNumber <- map["senderNumber"]
        receiverNumber <- map["receiverNumber"]
    }
}
