//
//  SendMoneyResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 16/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

class SendMoneyResponse: Mappable {
    var message: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
    
}
