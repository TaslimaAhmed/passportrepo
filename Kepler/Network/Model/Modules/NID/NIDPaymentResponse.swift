//
//  NIDPaymentResponse.swift
//  Kepler
//
//  Created by Taslima Ahmed on 3/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct NIDPaymentResponse : Mappable {
    var code : String?
    var message : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        code <- map["code"]
        message <- map["message"]
    }
}
