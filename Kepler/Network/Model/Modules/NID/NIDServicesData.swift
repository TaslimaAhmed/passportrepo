//
//  NIDServicesData.swift
//  Kepler
//
//  Created by Taslima Ahmed on 3/3/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct NIDServicesData: Mappable {
    var description : String?
    var id : Int?
    var amount : String?
    
    

   

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        description <- map["description"]
        id <- map["id"]
        amount <- map["amount"]
    }
}

