//
//  MobileRechareResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 19/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

class MobileRechareResponse: Mappable {
    var message: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
    
}
