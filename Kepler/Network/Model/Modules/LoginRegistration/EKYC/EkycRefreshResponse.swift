//
//  EkycRefreshResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 25/11/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct EkycRefreshResponse : Mappable {
    var gigatechStatus : String?
    var tblStatus : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        gigatechStatus <- map["gigatechStatus"]
        tblStatus <- map["tblStatus"]
    }
}
