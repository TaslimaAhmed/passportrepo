//
//  EkycVerifyResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 25/11/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct EkycVerifyResponse : Mappable {
    var status : String?
    var status_code : Int?
    var message : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        status_code <- map["status_code"]
        message <- map["message"]
    }

}
