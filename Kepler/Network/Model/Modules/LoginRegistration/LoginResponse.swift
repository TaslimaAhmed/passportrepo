//
//  LoginResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 11/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct LoginResponse : Mappable {
    var jwt : String?
    var timeout : Int?
    var userLoginInfo : UserLoginInfo?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        jwt <- map["jwt"]
        timeout <- map["timeout"]
        userLoginInfo <- map["userLoginInfo"]
    }
}


struct UserLoginInfo : Mappable {
    var userName : String?
    var balance : String?
    var ekycComplete : Bool?
    var userStatus : String?
    var featureAccesibilityMatrix : FeatureAccesibilityMatrix?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        userName <- map["userName"]
        balance <- map["balance"]
        ekycComplete <- map["ekycComplete"]
        userStatus <- map["userStatus"]
        featureAccesibilityMatrix <- map["featureAccesibilityMatrix"]
    }
}

struct FeatureAccesibilityMatrix : Mappable {
    var featureList : [FeatureList]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        featureList <- map["featureList"]
    }
}

struct FeatureList : Mappable {
    var featureName : String?
    var featureAccess : Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        featureName <- map["featureName"]
        featureAccess <- map["featureAccess"]
    }
}
