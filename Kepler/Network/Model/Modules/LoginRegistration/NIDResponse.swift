//
//  NIDResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 10/8/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct NIDResponse : Mappable {
    var status : String?
    var message : String?
    var data : NIDData?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
    }
}

struct NIDData : Mappable {
    var nid_no : String?
    var dob : String?
    var applicant_name_ben : String?
    var applicant_name_eng : String?
    var father_name : String?
    var mother_name : String?
    var spouse_name : String?
    var address : String?
    var id_front_image : String?
    var id_back_image : String?
    var id_front_name : String?
    var id_back_name : String?
    
    var pin: String?
    var gender: String?
    var nomineeName: String?
    var nomineeeRelation: String?
    var profession: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        nid_no <- map["nid_no"]
        dob <- map["dob"]
        applicant_name_ben <- map["applicant_name_ben"]
        applicant_name_eng <- map["applicant_name_eng"]
        father_name <- map["father_name"]
        mother_name <- map["mother_name"]
        spouse_name <- map["spouse_name"]
        address <- map["address"]
        id_front_image <- map["id_front_image"]
        id_back_image <- map["id_back_image"]
        id_front_name <- map["id_front_name"]
        id_back_name <- map["id_back_name"]
    }
}
