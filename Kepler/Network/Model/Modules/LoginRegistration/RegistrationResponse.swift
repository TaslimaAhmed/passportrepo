//
//  RegistrationResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 10/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct RegistrationResponse : Mappable {
    var message : String?
    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        message <- map["message"]
    }
}
