//
//  CheckMerchantResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 27/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct CheckMerchantResponse : Mappable {
    var userType : String?
    var userName : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        userType <- map["userType"]
        userName <- map["userName"]
    }
}
