

import Foundation
import ObjectMapper

struct AccountInfo : Mappable {
	var serialNo : Int?
	var maskedAccountNo : String?
	var accountName : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		serialNo <- map["serialNo"]
		maskedAccountNo <- map["maskedAccountNo"]
		accountName <- map["accountName"]
	}

}
