//
//  DescoPrepaidBreakdownResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 11/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct DescoPrepaidBreakdownResponse : Mappable {
    var message : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        message <- map["message"]
    }
}
