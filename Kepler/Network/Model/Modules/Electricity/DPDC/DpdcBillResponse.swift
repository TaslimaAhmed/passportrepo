//
//  DpdcBillResponse.swift
//  Kepler
//
//  Created by Taslima Ahmed on 23/2/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct DpdcBillResponse : Mappable {
    var billAmount : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        billAmount <- map["billAmount"]
    }
}
