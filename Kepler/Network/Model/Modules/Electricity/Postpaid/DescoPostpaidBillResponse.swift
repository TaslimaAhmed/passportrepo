//
//  DescoPostpaidBillResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 23/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct DescoPostpaidBillResponse : Mappable {
    var billAmount : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        billAmount <- map["billAmount"]
    }
}
