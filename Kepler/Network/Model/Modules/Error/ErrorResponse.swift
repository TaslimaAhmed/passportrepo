//
//  ErrorResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 20/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

class ErrorResponse: Mappable {
    var code: String?
    var message: String?
    var timestamp: String?
    var status: String?
    var error: String?

    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        message <- map["message"]
        timestamp <- map["timestamp"]
        status <- map["status"]
        error <- map["error"]
    }
    
}
