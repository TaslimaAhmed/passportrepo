//
//  ChargeResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 27/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct ChargeResponse : Mappable {
    var code: String?
    var message: String?
    var chargeAmount : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        code <- map["code"]
        message <- map["message"]
        chargeAmount <- map["chargeAmount"]
    }
}
