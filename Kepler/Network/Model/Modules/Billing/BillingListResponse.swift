//
//  BillingListResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 16/9/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct BillingListResponse : Mappable {
    var categoryId : Int?
    var categoryTitle : String?
    var categoryIconSource: String?
    var itemList : [BillingItemList]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        categoryId <- map["categoryId"]
        categoryTitle <- map["categoryTitle"]
        categoryIconSource <- map["categoryIconSource"]
        itemList <- map["itemList"]
    }
}

struct BillingItemList : Mappable {
    var code : String?
    var description : String?
    var id : String?
    var targetType : String?
    var iconSource : String?
    var labelView : String?
    var defaultNoOfMonth : String?
    var termsAndConditions : String?
    var meterNumber : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        code <- map["code"]
        description <- map["description"]
        id <- map["id"]
        targetType <- map["targetType"]
        iconSource <- map["iconSource"]
        labelView <- map["labelView"]
        defaultNoOfMonth <- map["defaultNoOfMonth"]
        termsAndConditions <- map["termsAndConditions"]
        meterNumber <- map["meterNumber"]
    }
}
