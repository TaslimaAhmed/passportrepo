//
//  FundPaymentResponse.swift
//  Kepler
//
//  Created by Taslima Ahmed on 21/4/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct FundPaymentResponse: Mappable {
    var message : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        message <- map["message"]
    }
}
