//
//  ForceUpdateResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 12/1/21.
//  Copyright © 2021 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct ForceUpdateResponse : Mappable {
    var forceUpdateVersion : String?
    var blacklistVersion : String?
    var message : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        forceUpdateVersion <- map["forceUpdateVersion"]
        blacklistVersion <- map["blacklistVersion"]
        message <- map["message"]
    }
}
