//
//  CheckBlanceResponse.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 15/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

class CheckBlanceResponse: Mappable {
    var currentBalance: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        currentBalance <- map["currentBalance"]
    }
    
}
