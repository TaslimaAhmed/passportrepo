//
//  Merchants.swift
//  Kepler
//
//  Created by Mufad Monwar on 17/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import UIKit

class Merchants {
    var name : String
    var icon : UIImage
    
    init(name: String, icon: UIImage) {
        self.name = name
        self.icon = icon
    }
}
