//
//  HomeMenu.swift
//  Kepler
//
//  Created by Mufad Monwar on 7/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import UIKit

class HomeMenu {
    var name: String
    var icon: UIImage
    var featureName: String
    var isActive: Bool = true
    
    init(name: String, image: UIImage, featureName: String, isActive: Bool) {
        self.name = name.localized
        self.icon = image
        self.featureName = featureName
        self.isActive = isActive
    }
    
    init(name: MenuConstants, image: UIImage, featureName: String, isActive: Bool) {
        self.name = name.rawValue.localized
        self.icon = image
        self.featureName = featureName
        self.isActive = isActive
    }
    
    init(name: DoMoreMenuConstants, image: UIImage, featureName: String, isActive: Bool) {
        self.name = name.rawValue.localized
        self.icon = image
        self.featureName = featureName
        self.isActive = isActive
    }
}
