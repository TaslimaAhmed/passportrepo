//
//  MobileOperator.swift
//  Kepler
//
//  Created by Mufad Monwar on 10/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import UIKit

class MobileOperator {
    var icon: UIImage
    var name: String
    
    init(icon:UIImage, name:String) {
        self.icon = icon
        self.name = name
    }
}
