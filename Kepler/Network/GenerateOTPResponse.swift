//
//  GenerateOTPResponse.swift
//  Kepler
//
//  Created by Mufad Monwar on 9/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct GenerateOTPResponse : Mappable {
    var code : String?
    var message : String?
    var otpReferenceId : String?

    init?(map: Map) {

    }
    
    mutating func mapping(map: Map) {
        code <- map["code"]
        message <- map["message"]
        otpReferenceId <- map["otpReferenceId"]
    }
}
