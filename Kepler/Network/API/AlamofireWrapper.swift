//
//  AlamofireWrapper.swift
//  Kepler
//
//  Created by Mufad Monwar on 8/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import Alamofire
import NotificationBannerSwift
import SVProgressHUD

class AlamofireWrapper {
    
    var defaultHeader: HTTPHeaders = {
        return [
            HTTPHeader(name: "Authorization", value: SessionManager.getString(forKey: .ACCESS_TOKEN) ?? ""),
            HTTPHeader(name: "msisdn", value: SessionManager.getString(forKey: .USER_MSISDN) ?? ""),
            HTTPHeader(name: "requestTime", value: getCurrentMillis())
        ]
    } ()
    
    class func requestGETURL(_ strURL: String, params : [String : AnyObject]?, headers : HTTPHeaders? = nil, success:@escaping (String) -> Void, failure:@escaping (String) -> Void) {
        DLog(headers)
        NetworkManager.isReachable { reachable in
            if reachable {
                //if !VpnChecker.isVpnActive() {
                    AF.request(strURL, method: .get, parameters: params, encoding: JSONEncoding.default, headers: headers).responseString { (responseString) -> Void in
                        DLog("\(strURL) response data: \(responseString)")
                        if let response = responseString.response, response.statusCode == 400 {
                            DLog("\(strURL) response data: \(responseString)")
                            let errorResponse = ErrorResponse (JSONString: responseString.value!)
                            if let errorMessage = errorResponse?.message {
                                failure(errorMessage)
                            }
                        } else if let response = responseString.response, response.statusCode == 401 {
                            initOnboardingView()
                        } else {
                            switch responseString.result {
                            case .success(let responseString):
                                success(responseString)
                            case.failure(let error):
                                failure(error.localizedDescription)
                            }
                        }
                    }
                //} else {
                //    SVProgressHUD.dismiss()
                //    NotificationBanner(title: "VPN connected.", subtitle: "Please disconnect VPN and try again.", style: .danger).show()
                //    DLog("VPN connected.")
                //}
            } else {
                showNoInternet()
                DLog("No Internet.")
                failure("No Internet!.".localized)
            }
        }
    }
    
    class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : HTTPHeaders? = nil, success:@escaping (String) -> Void, failure:@escaping (String) -> Void){
        DLog(headers)
        DLog("request data: \(String(describing: params))")
        DLog(params)
        NetworkManager.isReachable { reachable in
            if reachable {
                //if !VpnChecker.isVpnActive() {
                    AF.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseString { (responseString) -> Void in
                        DLog("\(strURL) response data: \(responseString)")
                        if let response = responseString.response, response.statusCode == 400 {
                            DLog("\(strURL) response data: \(responseString)")
                            let errorResponse = ErrorResponse (JSONString: responseString.value!)
                            if let errorMessage = errorResponse?.message {
                                failure(errorMessage)
                            }
                        } else if let response = responseString.response, response.statusCode == 401 {
                            showSessionExpired()
                        } else {
                            switch responseString.result {
                            case .success(let responseString):
                                success(responseString)
                            case.failure(let error):
                                failure(DefaultMessages.ERROR_MESSAGE.rawValue)
                            }
                        }
                    }
                //} else {
                //    SVProgressHUD.dismiss()
                //    NotificationBanner(title: "VPN connected.", subtitle: "Please disconnect VPN and try again.", style: .danger).show()
                //    DLog("VPN connected.")
                //}
            } else {
                showNoInternet()
                DLog("No Internet.")
                failure("No Internet!.".localized)
            }
        }
    }
    
    class func requestDELTEURL(_ strURL : String, params : [String : AnyObject]?, headers : HTTPHeaders? = nil, success:@escaping (String) -> Void, failure:@escaping (Error) -> Void){
        AF.request(strURL, method: .delete, parameters: params, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).responseString { (responseString) -> Void in
            
            DLog("\(strURL) response data: \(responseString)")
            
            switch responseString.result {
            case .success(let responseString):
                success(responseString)
            case.failure(let error):
                failure(error)
            }
        }
    }
}
