//
//  NetworkParams.swift
//  Kepler
//
//  Created by Mufad Monwar on 8/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

public struct HttpURLs {
    private static let PRODUCTION_API   = "https://api.bdkepler.com/api_middleware-0.0.1-RELEASE/"
    private static let STAGING_API      = "http://54.169.94.55:8080/api_middleware-0.0.1-TESTING/"
    private static let MOCK_API         = "http://54.169.94.55:8080/api_middleware-0.0.1-TESTING/"
    private static let BASE_URL         = PRODUCTION_API
    
    public static let aboutLink             = "https://trustaxiatapay.com/aboutus.php"
    public static let visitWebsiteLink      = "https://trustaxiatapay.com"
    public static let termsConditionLinkEN  = "https://api.bdkepler.com/terms-and-condition-en.html"
    public static let termsConditionLinkBN  = "https://api.bdkepler.com/terms-and-condition-bn.html"
    public static let privacyPolicyLink     = "https://api.bdkepler.com/privacy-policy.html"
    public static let samplePrepaidCard     = "https://api.bdkepler.com/images/biller-icon/preview/DESCO-prepaid.jpeg"
    public static let bannerImage           = "https://api.bdkepler.com/images/banner/tab02_001.png"
    
    public static let checkUser                 = BASE_URL + "checkUser"
    public static let generateOTP               = BASE_URL + "generate-otp"
    public static let verifyOTP                 = BASE_URL + "validate-otp"
    public static let loginUser                 = BASE_URL + "login"
    public static let logout                    = BASE_URL + "signOut"
    public static let balanceCheck              = BASE_URL + "check_balance"
    public static let sendMoney                 = BASE_URL + "p2p"
    public static let cashOut                   = BASE_URL + "cash_out"
    public static let addMoney                  = BASE_URL + "flora_fund"
    public static let mobileRecharge            = BASE_URL + "top_up"
    public static let transactionHistory        = BASE_URL + "mini_statement"
    public static let transactionType           = BASE_URL + "config-data"
    public static let linkedAccounts            = BASE_URL + "core_banking_account_info"
    
    public static let billList                  = BASE_URL + "getList"
    public static let getInstitution            = BASE_URL + "getInstitution"
    public static let getInsurance              = BASE_URL + "getInsurance"
    public static let getUtility                = BASE_URL + "getUtility"
    public static let getOhers                  = BASE_URL + "getOthers"
    public static let descoPrepaidBreakdown     = BASE_URL + "desco_prepaid_bill"
    public static let descoPrepaid              = BASE_URL + "desco_prepaid"
    public static let descoPostpaid             = BASE_URL + "desco_postpaid"
    public static let descoPostpaidBillGenerate = BASE_URL + "desco_bill"
    public static let tuitionPayment            = BASE_URL + "tuitionPayment"
    public static let insurancePayment          = BASE_URL + "insurancePayment"
    public static let passpotPayment            = BASE_URL + "passpotPayment"
    public static let nidFeePayment             = BASE_URL + "nidFeePayment"
    public static let nidServices               = BASE_URL + "nid_services"
    public static let nidBillPayment            = BASE_URL + "nid_payment"
    public static let checkUserType             = BASE_URL + "user_type"
    public static let merchantPayment           = BASE_URL + "merchant_payment"
    public static let update_pin                = BASE_URL + "update_pin"
    public static let passportBillPayment       = BASE_URL + "passport_payment"
    public static let dpdcBill                  = BASE_URL + "dpdc_bill"
    public static let payDpdcBill               = BASE_URL + "dpdc_pay"
    public static let fundTransfer              = BASE_URL + "deposit_money"
    
   
    
    
    public static let checkCharge               = BASE_URL + "fee_charge"
    
    
    //MARK: EKYC API's
    public static let asyncRegistration         = BASE_URL + "async_registration"
    public static let nidUpload                 = BASE_URL + "nidUpload"
    public static let ekyc_verify               = BASE_URL + "ekyc_verify"
    public static let ekyc_refresh              = BASE_URL + "refresh_ekyc"
    
    public static let force_update_check        = BASE_URL + "check_forceupdate_version?platform=IOS"
}
