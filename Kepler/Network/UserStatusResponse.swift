//
//  UserStatusResponse.swift
//  Kepler
//
//  Created by Mufad Monwar on 9/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

class UserStatusResponse: Mappable {
    var userStatus: Bool?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        userStatus <- map["userStatus"]
    }
    
}
