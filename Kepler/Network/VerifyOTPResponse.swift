//
//  VerifyOTP.swift
//  Kepler
//
//  Created by Mufad Monwar on 9/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

import ObjectMapper

struct VerifyOTPResponse : Mappable {
    var code : String?
    var message : String?
    var error_code : String?
    var error_description : String?
    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        code <- map["code"]
        message <- map["message"]
        
        error_code <- map["error_code"]
        error_description <- map["error_description"]
    }

}

