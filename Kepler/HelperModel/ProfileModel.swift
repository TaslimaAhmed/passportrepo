//
//  ProfileModel.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 25/10/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

class ProfileModel {
    var profileImage: String?
    var profileName: String?
    
    init(profileImage: Images, profileName: String) {
        self.profileImage = profileImage.rawValue
        self.profileName = profileName
    }
    
    class func profileDataList() -> [ProfileModel] {
        let profileList = [
            //ProfileModel(profileImage: .PROFILE, profileName: "Profile".localized),
            ProfileModel(profileImage: .PIN, profileName: "Change PIN".localized),
            ProfileModel(profileImage: .LIMIT, profileName: "TAP Limits".localized),
            ProfileModel(profileImage: .BANK, profileName: "Bank A/C Information".localized),
            ProfileModel(profileImage: .ABOUT, profileName: "About TAP".localized),
            ProfileModel(profileImage: .SIGNOUT, profileName: "Sign out".localized)
        ]
        
        return profileList
    }
}
