//
//  ContactsItem.swift
//  Kepler
//
//  Created by Mufad Monwar on 14/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation
import ObjectMapper

struct ContactList: Mappable {
    var contacts: [ContactsItem]!
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        self.contacts <- map["contacts"]
    }
    
    init(contacts: [ContactsItem]) {
        self.contacts = contacts
    }
}

struct ContactsItem: Mappable {
    var name: String!
    var number: String!
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.name <- map["name"]
        self.number <- map["number"]
    }
    
    init(name: String, number:String) {
        self.name = name
        self.number = number
    }
}
