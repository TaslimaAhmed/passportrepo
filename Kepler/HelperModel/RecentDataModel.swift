//
//  RecentDataModel.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 14/12/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import Foundation

class RecentDataModel {
    var transactionType: RecentTransectionType!
    var receiverPhoneNumber: String!
    var receiverName: String!
    
    init(transactionType: RecentTransectionType, _ receiverPhoneNumber: String, _ receiverName: String) {
        self.transactionType = transactionType
        self.receiverPhoneNumber = receiverPhoneNumber
        self.receiverName = receiverName
    }
}
