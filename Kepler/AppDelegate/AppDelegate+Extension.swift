//
//  AppDelegate+Extension.swift
//  Kepler
//
//  Created by Mahbubur Rashid on 9/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SVProgressHUD
import UserNotifications
import Contacts

extension AppDelegate {
    func renderStartingPage() {
        if !SessionManager.hasValue(forKey: .USER_MSISDN) {
            if SessionManager.hasValue(forKey: .IS_LOGGED_IN) {
                let initialViewController = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: SigninVC.identifier)
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            } else {
                let initialViewController = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: LandingVC.identifier)
                let navVC = UINavigationController(rootViewController: initialViewController)
                self.window?.rootViewController = navVC
                self.window?.makeKeyAndVisible()
            }
        } else {
            if SessionManager.hasValue(forKey: .IS_LOGGED_IN) {
                let initialViewController = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: SigninVC.identifier)
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            } else {
                let initialViewController = UIStoryboard(storyboard: .main).instantiateViewController(withIdentifier: LandingVC.identifier)
                let navVC = UINavigationController(rootViewController: initialViewController)
                self.window?.rootViewController = navVC
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    func setupInitialEkycFlag() {
        if !SessionManager.hasValue(forKey: .EKYC_STATUS) {
            SessionManager.setValue(EkycUserStatus.INCOMPLETE.rawValue, forKey: .EKYC_STATUS)
        }
    }
    
    func configureKeyboardAndProgress() {
        //IQKeyboardManager Configuration
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.previousNextDisplayMode = .default
        
        // Progress configuration
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        SVProgressHUD.setMinimumDismissTimeInterval(0.2)
    }
    
    func navigationBarColor() {
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for:UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.keplerDarkGray
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.keplerDarkGray]
    }
    
    func setupNotification() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if !granted {
                
            }
        }
        center.delegate = self
    }
    
    
    
    func checkUpdate() {
        CommonVM.checkForceUpdate { (ifHasUpdate, message) in
            if ifHasUpdate {
                let vc = UIStoryboard(storyboard: .common).instantiateViewController(withIdentifier: UpdateAvailableVC.identifier) as! UpdateAvailableVC
                self.window?.rootViewController?.present(vc, animated: true, completion: nil)
            }
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    //Whene application in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    //Whene application in foreground
}
